
Razm Programming Manual
=======================

by Andrew Apted, 2021.


Invocation
==========

The `razm` program is used to compile razm source code.  At the very
least it requires one or more source files to compile, and produces a
single file of native assembly.  Source files usually have the `.rz`
file extension, and the output assembly usually has the `.s` extension.
The output filename can be explicitly given via the `-o` option, though
it may be omitted and then the output filename is just the first source
filename with the extension changed to `.s`.

Running razm with the `--help` option will show a list of available
options which can be used.

If any errors are detected in the source files, they will be printed to
standard error.  Nothing is usually printed to standard output.  Each
error will show the file and line number where it occurs.  Since the
compiler runs in multiple phases, not all errors may be detected at
once since an error in an early phase prevents the later phases from
being run.

In order to build working executables, additional tools and libraries
will be needed.  For the AMD64 architecture, the NASM assembler must
be used to generate `.o` object files, and a linker (such as GNU LD)
must be used to merge the object files together.  You will also need a
runtime of some kind to communicate with the operating system.  Note
that these details are beyond the scope of this manual.

The `--target` option specifies the target operating system.  Only two
values are currently supported: "windows" and "linux", though the latter
generates code which is compatible with most 64-bit Unix-like OSes,
since the ABI (function calling convention) is usually the same.
The default target is the same as the one used to build razm.

The architecture can be selected via the `--arch` option.  Currently
only the "amd64" architecture is supported.  Using the `--arch` and
`--os` options is generally only needed when cross-compiling code
to something different than the host system.  Some combinations of
target OS and architecture may not make sense.

All optimization, including the register allocator, can be disabled by
the `-N` or `--no-optim` option.  This can be useful when debugging,
since all local variables of a function will occupy a unique slot in
the function's stack frame, and none are removed.

________________________________________________________________________


Syntax
======

As is usual for assembly languages, the overall syntax is line based.
Each directive or statement occurs on a single line, although a line
can be extended over multiple real lines by using a `\` (backslash)
symbol at the end of each line to extend, except the last of course.

Razm source code is expected to be Unicode, using the UTF-8 encoding.
There is no support for other character sets or encodings.

Whitespace is only significant at the beginning of line.  Directives
like `var`, `fun` and `end` must occur at the start of a line, whereas
the code in function bodies and the data in variable bodies must have
at least one whitespace character before them.  An exception to this
is labels within function bodies, which may (but don't have to) begin
at the start of a line, consistent with true assembly languages.

Comments are introduced by a `;` (semicolon) character and continue to
the end of current line.  Comments are always completely ignored.
There is no facility for comments which cover multiple lines.

Tokens
------

Each logical line is broken into a sequence of tokens.  Generally tokens
must be separated by whitespace, except for a handful of symbols which
include parentheses, square brackets, curly brackets, the `:` (colon)
and `^` (caret) symbols.  Hence something like `a+b*c` will be parsed
as a single token rather than five tokens as you may expect.

Identifiers may contain letters and digits (as defined by Unicode) or
one of the punctuation characters in the list below.  An identifier
cannot begin with a digit or the `'` (single quote) character.
Identifiers beginning with `#` (hash) and `@` (at sign) are reserved
for special keywords, and ones beginning with a `.` (dot) can only be
used for the names of struct fields and methods.  It is recommended to
only use the `\` (backslash) character as an escape mechanism, e.g.
when compiling a high-level language to razm and certain identifiers
cannot be represented natively.

```
    _ . - + * / % < = > ! ? $ & | ~ ' @ # \
```

Grouping Constructs
-------------------

Parentheses and brackets can be used to group a sequence of tokens.
The opening bracket must always be matched by a closing bracket of the
same kind.  There are three forms: a group in `()` parentheses is called
an "expression", a group in `[]` square brackets is called an "access",
and a group in `{}` curly brackets is called "data".  The first two are
mainly used in code, and the latter is mainly used in global variables.

Definitions
-----------

There are four main kinds of definitions in razm code: constants, types,
variables and functions.  These have a corresponding section in this
manual.  Definitions may appear in any order, so there is no need for
"forward declarations" like in the C language.

Visibility
----------

Functions and global variables are either "public" or "private".  This
visibility is related to the generated assembly files: in order for a
definition in one file to be used by another (i.e. "exported") it must
be marked public.  For each input `.rz` file, the visibility defaults
to private, and directives must be used to change the current visibility.

The visibility directives are:
```
    #public    ; mark subsequent defs as public  (exported)
    #private   ; mark subsequent defs as private (unexported)
```

________________________________________________________________________


Constants
=========

Constants are literal values, or the name of a literal value.

There are several types of values:
-  integers, such as `-123` or `0xABCD`
-  floats, such as `4.56` or `-0.789e15`
-  characters, such as `'A'`
-  strings, such as `"hello world"`
-  booleans, such as TRUE

Except for booleans, constants are considered *untyped*, which means they
rely on their context in order to get a concrete type like `s32` or `f64`.
For example, passing an integer literal in a function call is always okay
since parameters always have known types.  An error will be produced if a
constant is used in a way where the type cannot be determined.

Character constants are mostly equivalent to integers, with a value that
represents a single Unicode code point.  The only difference is how they
appear in the output assembly, and the wording of certain error messages.

Numeric Literals
----------------

Integers optionally begin with a sign, `+` or `-`, then may come a prefix
which sets the numeric base, and then there is one or more digits.  The
default numeric base is 10 (decimal), the prefix `0b` specifies base 2
(binary), and the prefix `0x` specifies base 16 (hexadecimal).

Integer examples:
```
    0  23  +23  -23  0b10101  0x57
```

Floating point numbers optionally begin with a sign, `+` or `-`, followed
by at least one decimal digit, then an optional fractional part after a
`.` (dot), then an optional exponent following the letter `e` or `E`.
The exponent may have a sign, `+` or `-`.  Either the fractional part
or the exponent must be present.

There is also a hexadecimal representation of floats, which follows the
C99 standard.  It begins with an optional sign, followed by `0x`, then at
least one hexadecimal digit, then an optional fractional part after a `.`
(dot), then an exponent following the letter `p` or `P`.  The exponent
consists of *decimal* digits and may begin with a sign.  The exponent
part must be present.

Float examples:
```
    0.0  -2.3  +2.3  1e7  -4.567e-89  0x1p4  -0x0.FEDCBAp-23
```

Character and String Literals
-----------------------------

Character literals represent a single Unicode code point.  The usual
syntax is the character itself surrounded on each side by a `'` (single
quote).  To allow the full range of characters, the single quotes may
contain an escape sequence which begins with a `\` (backslash) character
and is followed other characters (sometimes forming a numeric sequence)
to specify the wanted character.

Character examples:
```
    'A'        ; the first letter of English alphabet
    '7'        ; the number seven digit
    '\''       ; a single quote
    '\\'       ; a backslash
    '\n'       ; a newline character
    '\u263A'   ; a smiley face
```

String literals are a sequence of zero or more characters surrounded by
`"` (double quotes).  They represent an encoding of Unicode code points.
Like character literals, strings may contain escape sequences.

Strings can only be used in a context where the type is known, including
as variable values, inside data structures, as parameters to a function
call, etc...  The type must either be a pointer to an integer type, or
an array of integer type, and the target type determines the encoding.
When the target type is 8-bit, the string will be stored as UTF-8, when
the type is 16-bit the string is stored as UTF-16, and when the type is
32-bit the string is stored as UTF-32.

Strings used in the context of a pointer type are stored like `rom-var`
variable, i.e. in the read-only section of an executable, and will be
followed by at least one zero (NUL) character.  Strings used in an array
never add a NUL, so multiple strings can appear in a sequence and that
will be the same as having one long string.

String examples:
```
    ""                 ;  the empty string
    "Hello World!"     ;  a common greeting
    "\u263A \u263A"    ;  two smiley faces
```

Escape Sequences
----------------

The following table shows the non-numeric escape sequences:
```
    \'   : single quote
    \"   : double quote
    \`   : back quote
    \\   : backslash
    \?   : question mark

    \a   : bell or alert   (BEL)
    \b   : backspace       (BS)
    \n   : new line        (LF)
    \r   : carriage return (CR)

    \t   : horizontal tab  (HT)
    \v   : vertical tab    (VT)
    \f   : form feed       (FF)
    \e   : escape          (ESC)
```

The numeric escape sequences are:
```
    \123        : \  followed by one to three octal digits
    \x5A        : \x followed by exactly two hex digits
    \u263A      : \u followed by exactly four hex digits
    \U0001F602  : \U followed by exactly eight hex digits
```

The last two produce Unicode code points.  The first two produce raw byte
values -- for strings used in an 8-bit context, those bytes are used
directly, but for strings in 16-bit or 32-bit contexts they must form
a valid UTF-8 sequence, otherwise an error is produced.

User-Defined Constants
----------------------

The `const` directive is used to create user-defined constants.  It must
be followed by the name of the constant, an `=` (equal sign), and then a
single token for its value.  The value may be a literal, or the name of
another constant, or an expression inside `()` parentheses.

Example:
```
const MAX_BUFFER = 1024
const VERSION    = "v1.23"
```

Named constants are a convenience for the programmer.  Unlike variables,
they have no real existence beyond the source code, using them is like
using a macro which textually substitutes their name for their value.

Built-in Constants
------------------

```
FALSE  : the boolean "no"  value
TRUE   : the boolean "yes" value

NULL   : a pointer whose bits are all zero

+INF   : a float value representing positive infinity
-INF   : a float value representing negative infinity
QNAN   : a float value representing a quiet NaN
SNAN   : a float value representing a signalling NaN

CPU_ENDIAN : zero for little endian, one for big endian
CPU_BITS   : bits in a pointer and usize/ssize types
PTR_SIZE   : equivalent to (CPU_BITS / 8)
```

These constants are also untyped, e.g. `NULL` can be used anywhere a
pointer type is expected, and `+INF` can be used for both `f32` and `f64`
float types.

Note that in the IEEE-754 specification for floating point numbers, there
are many bit patterns which can represent a "NaN" (Not a Number) value.
The `QNAN` and `SNAN` constants are just two common ones.

Size Constants
--------------

For each user-defined type, a constant is created which represents the
total size (in bytes) of that type.  The name of this constant is just
the name of the type with `.size` appended.

Because types are processed in a later phase to constants, it is not
possible to use these size constants in certain contexts where a normal
constant may be used.  That includes the value for a `const` definition
or the size in `[]` of an array type.

Constant Expressions
--------------------

A constant expression is used to calculate a value at compile time.
They take the form of a math-like expression inside `()` parentheses,
either two values separated by a mathematical operator, or a unary
operator followed by a value.  The values must be either literals, named
constants, or another constant expression, and for binary operators both
values must be the same general type (integer, float, string, bool).

Example:
```
const MASK = ((1 << 10) - 1)
```

Constant expressions may be used for the value of a `const` directive,
for values in a `var` directive, for the size of an array type in `[]`
brackets, and within the code of a function.

Like other constants, the final value of a constant expression is untyped.
Floating point values are computed using 64-bit floats, and integer values
are computed using 64-bit integers.  The results are undefined if any part
of the calculation overflows a 64-bit value.  Division by zero is detected
and produces an error.

For strings, only the `+` (addition) operator is supported.

The available binary operators:
```
    +   addition
    -   subtraction
    *   multiplication
    /   division
    %   remainder (via truncation)

    &   bit-wise and  (also boolean)
    |   bit-wise or   (also boolean)
    ~   bit-wise xor

    <<  left bit shift
    >>  right bit shift

    ==  equal
    !=  not equal
    <   less than
    <=  less than or equal
    >   greater than
    >=  greater than or equal
```

The available unary operators:
```
    -   integer negation
    ~   flip (toggle) all bits
    !   boolean not
```

________________________________________________________________________


Types
=====

The basic types are as follows.  The `u` prefix denotes an unsigned
integer, whereas the `s` prefix denotes a signed integer.

```
u8,  s8      :  8-bit integer, occupies 1 byte
u16, s16     : 16-bit integer, occupies 2 bytes
u32, s32     : 32-bit integer, occupies 4 bytes
u64, s64     : 64-bit integer, occupies 8 bytes

usize, ssize : integer with same size as a pointer

f32          : 32-bit floating point, occupies 4 bytes
f64          : 64-bit floating point, occupies 8 bytes

bool         : boolean true or false, occupies one bit in a byte

void         : specifies a function which returns no value
no-return    : specifies a function which never returns
```

An important property of the basic types, including pointers, is that
they usually fit into a single CPU register.  That is in constrast to
the "complex" types, like arrays and structs, which always occupy an
area of memory somewhere.

Integers
--------

There are eight fixed-size integer types, four which are signed and
four which are unsigned.  The signed types support negative numbers,
whereas the unsigned types are always >= 0.  The format of the signed
integers is guaranteed to use two's complement.  The following table
shows the range of values supported by each type:

```
    s8   : -128 to +127
    u8   :    0 to +255

    s16  : -32768 to +32767
    u16  :      0 to +65535

    s32  : -2147483648 to +2147483647
    u32  :           0 to +4294967295

    s64  : -9223372036854775808 to +9223372036854775807
    u64  :                    0 to +18446744073709551615
```

The `usize` and `ssize` types are guaranteed to be the same size as a
pointer.  They are not directly compatible with the fixed-size integer
types, an explicit conversion is required when mixing them.  They are
mainly useful for representing sizes of memory areas, and computing the
difference between two pointers.

In most contexts a signed integer type is compatible with an unsigned
integer of the same size.  For example, you can pass a `u32` variable
as a parameter to a function needing a `s32` type.  There are a couple
of exceptions though, especially comparisons like the `lt?` and `ge?`
intrinsics which require the same signed-ness of their arguments.

There are no implicit conversions between integer types of different
sizes.

Floating Point
--------------

There are two floating point types, `f32` and `f64`, which occupy 32 bits
and 64 bits respectively.  They are very likely (but not guaranteed) to
use the same format as the IEEE-754 standard for floating point numbers,
where `f32` is known as "binary32" and `f64` is known as "binary64".

According to the IEEE-754 standard, they are broken down as:
```
    f32 : 1 sign bit,  8 exponent bits, 23 mantissa bits
    f64 : 1 sign bit, 11 exponent bits, 52 mantissa bits
```

The largest possible value of a `f32` is approximately 3.4028e+38, and the
largest possible value of a `f64` is approximately 1.7977e+308.  Each type
also has two zeros, +0.0 and -0.0, though the sign usually has no effect
on a calculation.  There are also some special values: an "infinity" is
produced when dividing a non-zero number by zero, and a "NaN" represents
a calculation with no meaningful result (it stands for "Not a Number"),
for example when dividing zero by itself.

There are no implicit conversions between the floating point types, nor
between an integer type and a floating point type.

Booleans
--------

A `bool` represents a truth value, either FALSE or TRUE.  Booleans are
represented as a single byte, but actually only a single bit in that
byte is used (the least significant bit).  The upper bits of the byte
are always ignored.

Certain operations, such as comparing two numbers, produce boolean values.
Some operations, especially the `jump` statement, require a boolean value.

Converting a boolean to an integer produces `0` for FALSE, `1` for TRUE.
Converting an integer to a boolean is equivalent to `le? value 0`
(so negative values are mapped to FALSE).

There are no implicit conversions to or from a boolean.

Casting between a boolean and an 8-bit integer can be done via `raw-cast`,
however when casting from an integer to a boolean there is no guarantee
that the unused bits will be kept as-is.

Pointers
--------

A pointer represents the location of something in memory.  Pointer types
are written as a `^` (caret) symbol followed by the target type, which
could be another pointer type.

The special `raw-mem` target represents a "bare" pointer.  These pointers
relax the rules for type compatibility, e.g. when assigning to a variable
or passing something to a function.  They can never be dereferenced (i.e.
read or write something at their address).  These pointers are mainly
useful for functions which allocate/copy/free raw memory blocks.

Pointer values may take on the special value `NULL` which (by convention)
means that the pointer is not pointing at anything.  Trying to read or
write a null pointer is undefined behavior, though it will generally
cause your program to crash.

Reading and writing the memory which a pointer points to requires using
the access notation in `[]` square brackets.  The following example shows
a function which reads and writes a signed 32-bit integer via a pointer:
```
fun pointer-stuff (p ^s32)
   val = [p]  ; read value at p
   [p] = 123  ; write value at p
end
```

The size of a pointer depends on the CPU architecture, either 32 bits or
64 bits.  Pointer values can be manipulated with the `padd` and `psub`
intrinsics, which always adjust the pointer by a number of bytes
(regardless of its target type).  Pointers can be `raw-cast` from and
to the `usize` and `ssize` integer types, for situations where you need
to calculate an address and the `padd` or `psub` intrinsics are not
sufficient.

Arrays
------

An array represents a block of elements of a particular type stored in
memory.  Array types are written as a length value within `[]` square
brackets and followed by the type of the elements.  The length value
is always a constant, and is allowed to be zero.

Within functions, arrays are always stored in local vars as a pointer
to the array type, since arrays always occupy a piece of memory and
cannot be stored in local vars directly.

Example of arrays:
```
zero-var sizes [20]s32  ; this array has 20 elements

fun array-stuff ()
   [sizes 0] = 123      ; write the first element
   print-num [sizes 3]  ; read the fourth element
end
```

Elements of an array can be accessed using notation in `[]` square
brackets.  The first thing in `[]` must be a pointer to the array, and
the second thing must be the index value.  Index values begin at zero.
For multi-dimensional arrays, both indexes need to be present, for
example: `[matrix 1 3]`.

Note that no bounds checking is done for array access, attempting to
read or write values outside of the array is generally considered as
undefined behavior, though using an array of size "0" can be useful to
access a larger data structure where the size is stored separately or
can otherwise be calculated somehow.

Structs
-------

A struct represents a group of fields which are stored together in a piece
of memory.  Each field has a name which must begin with a `.` (dot), and
the type of each field must also be given.  Structs must have at least one
field.  The total size of a struct is simply the sum of the sizes of each
field -- no implicit padding is ever added.

Like arrays, a struct must be referred to in code via a pointer -- they
cannot occupy a local var or be passed directly as a parameter when
calling a function.

Here is an example of defining a struct type:
```
type Person struct
   .name  ^u8
   .born   s64
end
```

That example defines a new type `Person` with two fields.  The `.name`
field is a pointer to byte (which will actually refer to a string), and
the `.born` field is a signed 64-bit integer.

Care must be taken with the alignment of fields in a struct.  On some
architectures, a program will crash when (for example) reading or writing
a 4-byte value whose address is not aligned to 4-bytes.  On some other
architectures it is allowed, but can be much slower.  Padding fields
will often need to be explicitly added.

Unions
------

A union represents a group of fields where only a single field is being
used at any one time.  Which field is "active" is not stored in the
union itself, it is either stored somewhere else or computed somehow.
Due to this property, the fields of a union overlap in memory and will
all begin at the same location.  The size of a union is the size of the
largest field it contains.

Unions are defined with the same general syntax as structs:
```
type Value union
   .byte   s8
   .short  s16
   .int    s64
   .float  f32
end
```

In the above example, the largest field is an `s64` and hence the size
of the union will be 8 bytes.

Accessing a union is also done the same way as accessing a struct, the
only difference is that multiple fields cannot be used at the same time.
With unions you mainly read the same field as you last wrote, but they
are occasionally used to access raw bit patterns by writing to one field
and then reading another one (when `raw-cast` is not sufficient).

Function Types
--------------

A function type is used when you want to store a pointer to a function
in a variable or data structure, or pass it as a parameter.  Function
types can only be defined as a user defined type, and consist of the
keyword `fun` followed by `()` parentheses which contain the parameters
and return type, as shown in the example below.

To actually use a function type, e.g. for a global variable, requires
using a *pointer* to the defined type, since the function itself cannot
be manipulated in any way.  Exactly what these pointer values point to
depends on the implementation, usually it is the address of the machine
code which the CPU will execute, but that is not guaranteed.

```
type PersonChecker fun (p ^Person -> bool)

zero-var check-func ^PersonChecker
```

________________________________________________________________________


Variables
=========

Variables in the global scope are defined using the `var` directive,
or alternatively the `zero-var` directive.  The former requires an
explicit value, and stores the variable in the data section of an
executable.  The latter stores the variable in the bss section, and
takes no value since everything in the bss section is cleared to zero
when the program is loaded.

Variables require a type specifier immediately after their name.
The type describes how the piece of memory used by the variable is
layed out.  For the `var` directive, the type is followed by an `=`
(equal sign) which is either followed by the value (on the same line),
or the value is spread out over subsequent lines, and these lines are
terminated by an `end` directive.

A simple example:
```
var game_over bool = FALSE
```

When the name of a global variable appears elsewhere in a program,
it represents the *address* of the global variable, and therefore its
type in that context is a pointer to the actual type of the variable.
So reading and writing the value of a global variable within a function
requires using the access notation in `[]` brackets.

External Variables
------------------

The `extern-var` directive defines a variable which does not exist in the
code currently being compiled.  Instead it will exist in another piece
of code which is compiled separately.  The other code might be razm code,
or it could be native assembly code, or even some other language like C
or C++.  The *linker* takes care of finding such variables when the final
executable is created, and it will produce an error if it cannot find the
variable in any object file.  It is the programmer's responsibility to
ensure that the types used in each programming language are compatible.

```
extern-var cow_count s32
```

Read-only Variables
-------------------

Data which never changes can be placed into a read-only section of the
executable.  This can help catch bugs, since any attempt to write to it
will, under most operating systems, cause the program to crash with a
"Segmentation Fault" or "General Protection Fault" error.  In razm code
this is achieved using the `rom-var` directive, as in the following
somewhat contrived example:

```
rom-var PI f32 = 3.14159265
```

Data Structures
---------------

A variable of a complex type (like an array or struct) must use a data
structure as the initializer.  There are two ways of doing this, the
compact form or the verbose form.  Both forms are equivalent, so which
one to use is often just a matter of personal taste.

The compact form is introduced by a `{` (open curly bracket) symbol and
finished by a `}` (close curly bracket) symbol.  All the elements of the
array or struct appear between these, separated by whitespace, and the
whole thing must occur on a single logical line.

Examples of the compact form:
```
var primes [12]s32 = { 2 3 5 7  11 13 17 19  23 29 31 37 }

var president Person = { .name "Obama" .born 1961 }
```

The verbose form spreads the data amongst multiple lines, terminated by
the `end` directive.  When a data element is another complex type, it may
use either the compact form in `{}` curly brackets, or another verbose
form.  For the latter, keywords corresponding to the data type must be
used at the start and end of the data element, and it is recommended to
indent the inner data more than the outer data to make everything more
readable.

Examples of the verbose form:
```
var primes [12]s32 =
   2  3  5  7
   11 13 17 19
   23 29 31 37
end

var presidents [3]Person =
   struct
      .name "Obama"
      .born 1961
   endstruct

   struct
      .name "Trump"
      .born 1946
   endstruct

   struct
      .name "Biden"
      .born 1942
   endstruct
end
```

Union values always consist of a field name followed by the value for
that field.  Field names are optional for structs, but when present
they are required to match the current field, and fields must always
be given in the same order as defined by the type.

In the verbose form, a complex type can be introduced by the keywords
`array`, `struct` or `union`.  These must appear on their own line,
though a field name may appear before them when appropriate.  The
contents of the element is given on the following lines, terminated
by the keywords `endarray`, `endstruct` or `endunion`, which must
appear on a line by itself.

Zero Filling
------------

The `...` (triple dots) symbol may appear at the end of the data for an
array or struct, either in the compact form or the verbose form.  This
causes the remaining array elements or struct fields to be filled with
zero bytes, similar to what happens with the `zero-var` directive.

In the following example, the last four elements of the array will be
zero:
```
var numbers [8]s32 = { 1 2 3 4 ... }
```

________________________________________________________________________


Functions
=========

Functions are where the magic happens, since they contain the code which
actually does stuff.  Each function has zero or more parameters, these
are the inputs to the function, and each function may return a value,
which is the output of a function.

Functions are defined by the `fun` directive, which is followed by the
name of the new function.  The parameters and return type, if any, are
given in `()` parentheses after the name.  The return type is optional,
when present it appears after the `->` (arrow) symbol, otherwise the
return type is `void`.  The subsequent lines contain the body of the
function (its code), which is terminated by the `end` directive.  Apart
from labels, the code in a function body *must* be indented by at least
one character of whitespace, and it does not matter if you use tabs or
spaces.

Example of a simple function:
```
fun add-two (num s32 -> s32)
   ; num is a parameter of type 's32'
   ; the result is also of type 's32'
   num = iadd num 2
   return num
end
```

Local Variables
---------------

A local variable is a variable which only exists in the context of a
function body -- they can never be accessed by a different function.
Local variables exist either on the stack, or within a CPU register.
The latter is ideal since CPU registers are much faster than reading
or writing memory, but since there are only a handful of CPU registers
it is often not possible for the compiler to assign one to every local
variable.

Local variables always have a fixed type, and it must be a simple type,
they cannot be complex types like arrays, structs or unions.  There is
no scoping within the body of a function, once a local is declared it
exists for the rest of the function.  Hence it is not possible to have
two local variables with the same name but different types.

Locals are declared by an assignment statement, which uses the `=`
(equal sign) symbol.  To the left of the equal sign is the name of the
local var, which may be followed by a type name.  The type is often
omitted, since the compiler can usually figure out the type from the
computation following the equal sign.  When the type cannot be deduced
by the compiler, especially when the value is a literal, then the type
must be given explicitly.

```
fun local-stuff ()
   x s32 = 123  ; type for x is needed, as literals are untyped
   y     = x    ; type for y is not needed, it is deduced from x
end
```

Multiple assignments to a local may specify the type of the local, but
they must all be the *same* type.  An error is produced if you attempt
to declare the same local with different types.

Stack Variables
---------------

A variable can be created in the stack frame of a function by using
the `stack-var` keyword.  It must be followed by a type name, and the
type determines the amount of space used on the stack.  It produces a
*pointer* to this memory, hence reading or writing it requires using
the access notation in `[]` brackets (like with global variables).
The space is allocated once for the entire function, so inside a loop
you will always get the same pointer.

Note that these variables are not initialized or cleared in any way,
you need to do that yourself.  So in the following example, the last
two elements of the `totals` array are not set, hence reading them
would produce random garbage.  Real programs need to take care to
fully initialize such variables.

```
fun stack-stuff ()
   totals = stack-var [4]s32
   [totals 0] = 123
   [totals 1] = 456
   print-num [totals 0]
end
```

Labels
------

A label is an identifier followed by the `:` (colon) symbol.  Labels must
appear as the first thing in a line, and may be followed by a statement
or appear on a line by themselves.  They are often indented one level
less than the code they belong to, though this is not a requirement.
Labels have only one purpose, to mark a place in the code where a `jump`
statement will jump to.

```
fun looper ()
   x s32 = 0
repeat:
   x = iadd x 1
   print-num x
   jump repeat
end
```

Labels and local variables occupy a single namespace, hence you cannot
have a label and a local variable (including parameters) with the same
name.

Statements
----------

Each line of code in a function which is not a comment or a label is a
statement.  One of the most common statements is an assignment to a
local variable or a piece of memory.  Another common statement is a
function call.

The are also "pseudo statements" which are actually not fundamental
elements of the language, but more like macros which expand to the
fundamental forms.  For example, the `loop` and `endloop` statements
expand to a label and a `jump` statement, respectively.  These are
described in a section below.

The `return` statement is used to finish execution of a function and
return to the calling function.  If the function returns a value, then
the `return` keyword must be followed by a computation which computes
the value to return, otherwise it must be on a line by itself.  Using
this in a `no-return` function produces a compile error.

Functions which return a value *must* have a return statement at the
very end, unless the compiler can tell that the very end is never
reached.  Functions which do not return a value, sometimes called
"void functions", do not need a `return` at the very end, and for
readability's sake it is usually omitted there.

The `jump` statement is the fundamental form of flow control, causing
the execution of the code in a function to jump to a different line.
The `jump` keyword must be followed by the name of a label.  After the
label may be a condition, which must be satisfied for the jump to occur.
The condition consists of the keyword `if` or the keyword `unless`, and
is followed by a computation which must have boolean type.  The `if`
form requires the condition to be TRUE, whereas the `unless` form
requires the condition to be FALSE.

```
fun checker (num s32)
   jump negative if neg? num
   print-str "number is positive"
   return
negative:
   print-str "number is negative"
end
```

Assignment
----------

An assignment takes a computed value and stores it somewhere, either
in a local variable or a piece of memory.  The syntax of an assignment
is a line consisting of a destination, followed by an `=` (equal sign),
followed by a computation.  The destination is either the name of a
local variable, or a memory access in `[]` brackets.  Local variables
may be followed by a type.

```
zero-var gg s32

fun assignments ()
   x s32 = 123  ; assign 123 to local variable 'x'
   [gg]  = x    ; assign x to global variable 'gg'
end
```

The `mem-write` statement is similar to an assignment.  It takes two
operands, a memory access in `[]` brackets for the memory to write to,
and the value to store there.  The primary difference compared to an
ordinary assignment is that the compiler guarantees to never optimize
away the memory store, which can be important for memory-mapped I/O.

The `swap` statement takes two operands, both of which are something
which can be assigned to, and it swaps the values between them.

```
fun swapper ()
   x s32 = 123
   y s32 = 456
   swap x y
   print-num x  ; should show 456
   print-num y  ; should show 123
end
```

Function Calls
--------------

A function call causes execution to temporarily leave the currently
running function and enter a new function.  When that new function is
finished, execution normally returns to the calling function.  The
exception is a function which has a result type of `no-return`, such
a function is guaranteed to never return.

A function call can appear by itself on a line, or as a computation,
for example after the `=` (equal sign) in an assignment.  When the call
appears on a line by itself, any return value from the called function
is simply ignored (thrown away).  The first element of a function call
is the function name, which is followed by zero or more operands which
are the parameters of the function call.  The number of parameters in
the call must match the number in the function's definition, and the
type of each operand must be compatible with the type of the
corresponding parameter.

The `call` keyword is another way of calling functions.  It is followed
by an operand whose type which is a pointer to a function type, usually
a local variable or something read from a data structure, but the name
of a global function is also allowed here.  Following that are the
parameters, if any.

```
fun call-stuff ()
   ; normal function call
   print-num 123

   ; computed function call
   f = fun print-num
   call f 456
end

fun print-num (num s32)
   ; code to print a number...
end
```

The `tail-call` statement performs a special kind of function call
which saves stack space, allowing recursive algorithms to work without
overflowing the stack.  The tail-called function does not return to
the current function, instead its stack frame replaces the area of
the stack used by the current function, and when it finishes it will
return directly to the caller of the current function.

The tail-called function must have a return type which is compatible
with the current function, unless the current function has no return
value (is a "void function").  Due to ABI limitations, a tail-called
function cannot have more than four parameters.

Pseudo Statements
-----------------

The `if` statement simplifies the construction of conditional code,
using a syntax similar to that provided by higher level languages.
After the `if` keyword is a computation which must have boolean type,
and when it evaluates to TRUE (at run-time) then the code in subsequent
lines will be executed, otherwise that code is skipped.  The full `if`
statement is terminated by the `endif` keyword on a line by itself.

Within an `if` statement there may be one or more `elif` keywords.
These are followed by another computation and (assuming the original
condition was FALSE) will be tested and when TRUE the following code
is executed.  There may also be a single `else` keyword, which causes
the following lines to be executed only when all previous conditions
evaluated to FALSE.

```
fun num-to-word (num s32 -> ^u8)
   if eq? num 1
      return "one"
   elif eq? num 2
      return "two"
   else
      return "???"
   endif
end
```

The `loop` statement simplifies the construction of looping code,
and helps to make such code more readable.  The `loop` keyword appears
on a line by itself, and the following lines contain the code which will
be repeatedly executed.  The `endloop` keyword is used to mark the end
of the loop.

More complex loops contain a condition after the `loop` keyword, and
that condition is checked at the beginning of each loop and it must be
satisfied for the loop to continue executing.  The condition consists
of the keyword `while` or the keyword `until`, followed by a computation
which must have boolean type.  The `while` form requires the condition
to be TRUE, whereas the `until` form requires the condition to be FALSE.

```
fun countdown (n s32)
   loop while gt? n 0
      print-num n
      n = isub n 1
   endloop
end
```

The `break` statement can only be used within a `loop` block, and it
causes execution to jump out of the loop.  It can be used by itself, or
followed by a condition which must be satisfied for the jump to occur.
The condition has the same syntax as used in a `jump` statement.

Computations
------------

A "computation" is something which produces a value.  The simplest kind
of computation is just a literal value like `42`.  Function calls and
intrinsics are two very common kinds of computation.  Reading memory of
something in `[]` brackets is another one.  Other computations begin
with a distinct keyword.

Function calls were discussed previously.  When used as a computation,
the value returned from the call is the computed value.  Intrinsics and
memory access are discussed in their own sections further down.

The `mem-read` keyword is another way to read memory.  It takes a single
operand, a memory access in `[]`, and it produces the value read from
that memory location.  The primary difference compared to a plain memory
access is that the compiler guarantees to never optimize away the read,
which can be important when doing memory-mapped I/O.

The `sel-if` keyword selects between two values based on a condition.
The first operand is the condition, which must have boolean type, the
second operand is the value selected when the condition is TRUE, and
the third operand is the value selected when the condition is FALSE.

```
fun selection (lowercase bool)
   a u32 = sel-if lowercase 'a' 'A'
   z u32 = sel-if lowercase 'z' 'Z'
   print-char a
   print-char z
end
```

The `fun` keyword takes a single operand, the name of a global function,
and produces the address of that function.  This pointer can be used in
the `call` statement to call the function, or stored in a variable or
data structure, etc...

Memory Access
-------------

A memory access is denoted by something in `[]` square brackets.  This
can be used as the destination of an assignment, causing a value to be
written into a piece of memory, or it can be used as a value (such as a
parameter in a function call), causing a value to be read from a piece
of memory.

The elements inside the `[]` brackets are used to compute the address
of the memory to read or write.  If the first element is a pointer to
a simple type, then nothing else is needed (or allowed), as the pointer
itself is the address.  If the first element refers to an array, then
the next element must be integer value which is the index of the array
element to access.  If the first element refers to a struct or union,
then the next element must be the field name to access.

After an array index or struct field, there can be another array index
or struct field, e.g. to access a two dimensional array or a struct
within a struct, etc...  These are allowed since they are essentially
just additions to the base address.  In constrast, it is not possible
to dereference more than one pointer inside a single `[]` expression,
but memory accesses may be nested -- a base pointer or array index may
also be a memory access in `[]` brackets.  When so nested, the inner
access will actually be computed via a temporary local variable.

```
var presidents [3]Person =
   { "Obama" 1961 }
   { "Trump" 1946 }
   { "Biden" 1942 }
end

fun show-info (idx s32)
   print-str "name: "
   print-str [presidents idx .name]
   print-str "born in: "
   print-str [presidents idx .born]
end
```

The first element inside `[]` brackets may be the keyword `addr-of`,
and instead of accessing memory this merely computes the address of
the memory which would have been accessed.  The result is a pointer
whose target type matches the specified element or field.

Conversions
-----------

There are two conversion keywords, `conv` and `raw-cast`.  The former
is used to convert between numeric types (and also booleans), whilst
the latter merely re-interprets the bit pattern of a value into a
different type.

The `conv` keyword is followed by a type name specifying the target
type of the conversion, and that is followed by the value to convert.
The target type must be either an integer, floating point or boolean,
and similarly for the source type.  Conversion between floats and
integers requires a signed integer type, and the result is undefined
if the value cannot be represented in the target type.  Conversions
to and from booleans is discussed in the Types section.  It is not
possible to convert between floats and booleans.

```
fun converter ()
   x f32 = -53.4
   y s32 = conv s32 x  ; y will be -53
end
```

The `raw-cast` keyword is followed by an *optional* type name, and then
the value to be casted.  When the type is omitted, the value must have
a pointer type, and the target type will be a pointer to `raw-mem`.  The
source type must have exactly the same number of bits as the target type.
Note that pointers can only be casted between the `usize` and `ssize`
integer types, as these are guaranteed to have the same number of bits
as pointers on all architectures.

```
fun caster ()
   x ^A = NULL
   y ^B = raw-cast x
   z    = raw-cast usize y
end
```

Intrinsics
----------

An "intrinsic" is like a built-in function which directly computes a
value, such as adding two numbers or comparing two pointers.  They are
modelled on CPU instructions, and often map directly to a single CPU
instruction.

Since they compute something, and don't modify anything, they cannot
be used without a destination.

All of the current intrinsics require at least one or two arguments.
Intrinsics which require a specific type usually begin with a letter
which representing that type: `i` for integer, `f` for float, `p` for
pointer.  Boolean intrinsics are an exception to this rule.

Integer intrinsics:
```
    ineg  X    ; negate an integer
    iabs  X    ; absolute value of an integer

    iadd  X Y  ; integer addition
    isub  X Y  ; integer subtraction
    imul  X Y  ; integer multiplication
    idivt X Y  ; integer division (truncating toward zero)
    iremt X Y  ; integer remainder (via a truncating division)

    imin  X Y  ; minimum of two integers
    imax  X Y  ; maximum of two integers

    iand  X Y  ; bit-wise and
    ior   X Y  ; bit-wise or
    ixor  X Y  ; bit-wise exclusive-or
    iflip X    ; bit-wise flip (toggle) all bits

    ishl  X C  ; integer shift left
    ishr  X C  ; integer shift right
```

Floating point intrinsics:
```
    fneg  X    ; negate a float
    fabs  X    ; absolute value of a float
    finv  X    ; inverse of a float, same as (1.0 / X)
    fsqrt X    ; square-root of a float

    fadd  X Y  ; float addition
    fsub  X Y  ; float subtraction
    fmul  X Y  ; float multiplication
    fdiv  X Y  ; float division
    fremt X Y  ; float remainder (via a truncating division)

    fmin  X Y  ; minimum of two floats
    fmax  X Y  ; maximum of two floats

    fround  X  ; round float to nearest integer
    ftrunc  X  ; round float toward zero (truncate it)
    ffloor  X  ; round float down (toward -infinity)
    fceil   X  ; round float up   (toward +infinity)
```

Pointer intrinsics:
```
    padd  P I  ; add integer offset to pointer
    psub  P I  ; subtract integer offset from pointer
    pdiff P Q  ; subtract two pointers, produce an integer
```

Boolean intrinsics:
```
    not  X    ; boolean not
    and  X Y  ; boolean and (true if both X and Y are true)
    or   X Y  ; boolean or  (true if either X or Y are true)
```

Endianness intrinsics:
```
    little-endian  X  ; endian swap X if CPU is big endian
    big-endian     X  ; endian swap X if CPU is little endian
```

Comparisons and Classification:
```
    eq?  X Y  ; check if X is equal to Y
    ne?  X Y  ; check if X is not equal to Y

    lt?  X Y  ; check if X is less than Y
    le?  X Y  ; check if X is less than or equal to Y

    gt?  X Y  ; check if X is greater than Y
    ge?  X Y  ; check if X is greater than or equal to Y

    zero?  X  ; check if X is zero
    some?  X  ; check if X is non-zero
    pos?   X  ; check if X is positive (and non-zero)
    neg?   X  ; check if X is negative

    inf?   F  ; check for a floating point infinity
    nan?   F  ; check for a floating point NaN

    null?  P  ; check for a NULL pointer
    ref?   P  ; check for a usable (non-null) pointer
```

Expressions
-----------

An "expression" is a computation which is appears in `()` parentheses.
The basic way these are handled is to create a temporary local variable
which computes the thing inside `()`, and replace that expression with
the name of the temporary variable.

For example:
```
    SomeFunc a (iadd x y) b
```

gets replaced by something like this:
```
    __expr1 = iadd x y
    SomeFunc a __expr1 b
```

One exception to this rule is that when the expression is a constant
expression, it is simply replaced by the value calculated at compile
time.  Also an expression in `()` which follows the `=` (equal sign) in
a line is expanded in-place instead of creating a redundant temporary
variable.

Expressions are recursive, i.e. one expression may contain another one.
Expressions in a line are expanded in left-to-right order,  and inner
ones are always expanded before their containing expression.

Chained Intrinsics
------------------

Most of the two-argument intrinsics support "chaining", which means they
accept more than two arguments, and each additional argument performs
the same operation using the previous result and the current argument.
For example, the computation `isub A B C` will first subtract `B` from
`A`, and then subtract `C` from that value.

The `eq?` and `ne?` comparisons also support multiple arguments, but
with a different meaning.  For `eq?` the result is TRUE if the first
argument is equal to *any* other argument.  For `ne?` the result is
TRUE only if the first argument is not equal to any other arguments.

External Functions
------------------

The `extern-fun` directive is analogous to the `extern-var` directive,
since it defines a function which does not exist in the code currently
being compiled, rather it exists in a piece of code compiled separately.

Inline Functions
----------------

The `inline-fun` directive is used to create a function which is not
compiled like normal functions.  Instead the body of the function gets
inserted into the calling function, using a process similar to macro
expansion.  This means that two inline functions can never call each
other.  It also means that the function does not exist as a real thing
in the output assembly, and hence it is not possible to take the address
of an inline function.

The main benefit of inline functions is for small utility functions
where you really want to avoid the overhead of doing a normal function
call.  For example: a set of fixed-point math routines.  Since modern
CPUs usually pass parameters via registers, the benefit of inlining a
function may not be as great as it seems, in fact the duplication of
that code can actually be detrimental to performace.
