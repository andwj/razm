// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "math"
import "strconv"

func (fu *FuncDef) Expand() error {
	// we will build a new body like Arnie
	old_body := fu.body
	fu.body = NewNode(NG_Body, "code", old_body.pos)

	for _, line := range old_body.children {
		if fu.ExpandLine(line, false) != OK {
			// allow a limited number of errors per function
			fu.error_num += 1
			if fu.error_num > 5 && !Options.all_errors {
				return FAILED
			}
		}
	}

	if fu.error_num > 0 {
		return FAILED
	}

	if !fu.blocks.Empty() {
		ErrorsSetPos(fu.blocks.top.start)
		PostError("unfinished %s block", fu.blocks.top.kind)
		return FAILED
	}

	return OK
}

func (fu *FuncDef) ExpandLine(line *Node, is_expr bool) error {
	ErrorsSetPos(line.pos)

	// split labels from rest of line
	if len(line.children) > 0 && line.children[0].kind == NH_Label {
		fu.body.Add(line.children[0])
		line.children = line.children[1:]
	}

	// remove empty lines (mostly from the above split)
	if line.Len() == 0 {
		return OK
	}

	// first element of a line can never be an expression
	if line.children[0].kind == NG_Expr {
		PostError("cannot use () expression in that context")
		return FAILED
	}

	// handle constant expressions in `()`.
	// need to do this before handling `if` forms.
	// normal expressions in `()` get done a bit later.

	if fu.TryExpandConstExprs(line, true) != OK {
		return FAILED
	}

	// convert the `if` and `loop` forms into labels and jumps.
	if !is_expr {
		head := line.children[0]
		switch {
		case head.Match("if"), head.Match("elif"), head.Match("else"):
			return fu.ExpandIfElse(line)

		case head.Match("endif"):
			return fu.ExpandEndif(line)

		case head.Match("loop"):
			return fu.ExpandLoop(line)

		case head.Match("break"):
			return fu.ExpandBreak(line)

		case head.Match("endloop"):
			return fu.ExpandEndloop(line)
		}
	}

	// expand expressions in `()`, create a temporary for it and
	// treat it like a line occurring before the current one.
	// this is recursive, exprs in exprs will get expanded too.

	if fu.TryExpandExprs(line) != OK {
		return FAILED
	}

	// keep track of names of locals.
	// this is vital for the detection of constant expressions.
	// do it *after* expansion to handle locals which shadow a global const.
	if !is_expr {
		head := line.children[0]
		if line.Find("=") >= 0 && head.kind == NL_Name {
			fu.seen_locals[head.str] = true
		}
	}

	// pass everything else through as-is
	fu.body.Add(line)
	return OK
}

func (fu *FuncDef) TryExpandConstExprs(line *Node, is_line bool) error {
	for k, child := range line.children {
		if child.kind == NG_Expr {
			if fu.IsConstExpression(child) {
				res, t_value := EvalExpression(child, fu)
				if res != EVAL_OK {
					return FAILED
				}
				line.children[k] = t_value
				continue
			}
		}

		switch child.kind {
		case NG_Access, NG_Data, NG_Expr:
			// expand const expressions inside bracketed groups
			if fu.TryExpandConstExprs(child, false) != OK {
				return FAILED
			}
		}
	}

	return OK
}

func (fu *FuncDef) TryExpandExprs(line *Node) error {
	// for a line of the form `x = (...)`, avoid creating a temporary
	// by just expanding the NG_Expr in the current line.
	equal_pos := line.Find("=")

	if equal_pos > 0 && equal_pos == line.Len()-2 {
		expr := line.children[equal_pos + 1]
		if expr.kind == NG_Expr {
			// check a few things...
			if expr.Len() == 0 {
				PostError("empty expression in ()")
				return FAILED
			}
			if expr.Find("=") >= 0 {
				PostError("cannot use '=' in an expression")
				return FAILED
			}
			line.children = line.children[0:equal_pos+1]

			for _, child := range expr.children {
				line.Add(child)
			}
		}
	}

	for k, child := range line.children {
		switch child.kind {
		case NG_Expr:
			temp := fu.NewTemporary("__expr")

			// replace that expression with the temporary
			t_name := NewNode(NL_Name, temp, child.pos)
			line.children[k] = t_name

			// check a few things...
			if child.Len() == 0 {
				PostError("empty expression in ()")
				return FAILED
			}
			if child.Find("=") >= 0 {
				PostError("cannot use '=' in an expression")
				return FAILED
			}

			// build a new line and recursively handle it
			if fu.ExpandExpression(child, temp) != OK {
				return FAILED
			}

		case NG_Access, NG_Data:
			// expand expressions used inside the `[]` brackets
			if fu.TryExpandExprs(child) != OK {
				return FAILED
			}
		}
	}

	return OK
}

func (fu *FuncDef) ExpandExpression(expr *Node, temp string) error {
	t_name  := NewNode(NL_Name, temp, expr.pos)
	t_equal := NewNode(NL_Name, "=", expr.pos)

	new_line := NewNode(NG_Line, "", expr.pos)
	new_line.Add(t_name)
	new_line.Add(t_equal)

	for _, child := range expr.children {
		new_line.Add(child)
	}
	return fu.ExpandLine(new_line, true)
}

func (fu *FuncDef) ExpandIfElse(line *Node) error {
	children := line.children
	head := children[0]

	is_if   := head.Match("if")
	is_elif := head.Match("elif")
	is_else := head.Match("else")

	var blk *BlockInfo

	if is_if {
		blk = fu.blocks.Push("if", line.pos)
		fu.if_labels += 1
		blk.label = "__endif" + strconv.Itoa(fu.if_labels)

	} else {
		if !fu.blocks.Match("if") {
			PostError("%s used without a matching if", head.str)
			return FAILED
		}

		blk = fu.blocks.top

		if blk.else_ {
			PostError("expected endif, got: %s", head.str)
			return FAILED
		}

		if is_else {
			blk.else_ = true
		}

		// for `elif` and `else`, make previous code jump to end, and
		// emit the skipping label of the previous `if` or `elif`.
		fu.ExpandJump(blk.label, head.pos)
		fu.EmitLabel(blk.skip, head.pos)

		blk.skip = ""
	}

	// fpr `if` and `elif`, grab the condition and create a jump
	if is_if || is_elif {
		// we NEED to expand expressions in `()` here to support `elif`
		// properly, any new lines need to occur AFTER the jump and label
		// which we just added.

		if fu.TryExpandExprs(line) != OK {
			return FAILED
		}

		if line.Len() < 2 {
			PostError("missing condition after %s", head.str)
			return FAILED
		}

		fu.if_labels += 1
		blk.skip = "__over" + strconv.Itoa(fu.if_labels)

		// create an ordinary line with a jump statement
		// [ allow later code to validate the condition part ]

		t_jump   := NewNode(NL_Name, "jump",   head.pos)
		t_label  := NewNode(NL_Name, blk.skip, head.pos)
		t_unless := NewNode(NL_Name, "unless", head.pos)

		t_line := NewNode(NG_Line, "", head.pos)
		t_line.Add(t_jump)
		t_line.Add(t_label)
		t_line.Add(t_unless)

		for i := 1; i < line.Len(); i++ {
			t_line.Add(line.children[i])
		}

		fu.body.Add(t_line)

	} else {
		if line.Len() > 1 {
			PostError("extra rubbish after %s statement", head.str)
			return FAILED
		}
	}

	return OK
}

func (fu *FuncDef) ExpandEndif(line *Node) error {
	if !fu.blocks.Match("if") {
		if fu.blocks.top == nil {
			PostError("endif used without a matching if")
		} else {
			PostError("expected end%s, got: endif", fu.blocks.top.kind)
		}
		return FAILED
	}

	blk := fu.blocks.top

	if blk.skip != "" {
		fu.EmitLabel(blk.skip, line.pos)
	}
	fu.EmitLabel(blk.label, line.pos)
	fu.blocks.Pop()

	if line.Len() > 1 {
		PostError("extra rubbish after endif statement")
		return FAILED
	}
	return OK
}

func (fu *FuncDef) ExpandLoop(line *Node) error {
	// create a new label
	fu.loop_labels += 1
	lab_name := "__loop" + strconv.Itoa(fu.loop_labels)

	fu.EmitLabel(lab_name, line.pos)

	blk := fu.blocks.Push("loop", line.pos)
	blk.label = lab_name

	if fu.TryExpandExprs(line) != OK {
		return FAILED
	}

	// support a condition after the `loop` keyword
	if line.Len() > 1 {
		keyword   := line.children[1]
		jump_cond := "unless"

		if keyword.Match("while") {
			// ok
		} else if keyword.Match("until") {
			// ok
			jump_cond = "if"
		} else {
			PostError("expected 'while' or 'until', got: %s", keyword.String())
			return FAILED
		}

		// create the exit label
		fu.loop_labels += 1
		blk.skip = "__break" + strconv.Itoa(fu.loop_labels)

		new_line := fu.ExpandJump(blk.skip, line.pos)

		t_cond := NewNode(NL_Name, jump_cond, line.pos)
		new_line.Add(t_cond)

		for i := 2; i < line.Len(); i++ {
			new_line.Add(line.children[i])
		}
	}
	return OK
}

func (fu *FuncDef) ExpandBreak(line *Node) error {
	if fu.TryExpandExprs(line) != OK {
		return FAILED
	}

	// find the most recent loop block
	blk := fu.blocks.top

	for blk != nil {
		if blk.kind == "loop" {
			break // ok
		}
		blk = blk.parent
	}

	if blk == nil {
		PostError("break used outside of a loop")
		return FAILED
	}

	// create the exit label
	if blk.skip == "" {
		fu.loop_labels += 1
		blk.skip = "__break" + strconv.Itoa(fu.loop_labels)
	}

	new_line := fu.ExpandJump(blk.skip, line.pos)

	// support a condition after the `break` keyword
	if line.Len() > 1 {
		keyword := line.children[1]

		if keyword.Match("if") || keyword.Match("unless") {
			// ok
		} else {
			PostError("expected 'if' or 'unless', got: %s", keyword.String())
			return FAILED
		}

		for i := 1; i < line.Len(); i++ {
			new_line.Add(line.children[i])
		}
	}
	return OK
}

func (fu *FuncDef) ExpandEndloop(line *Node) error {
	if !fu.blocks.Match("loop") {
		if fu.blocks.top == nil {
			PostError("endloop used without a matching loop")
		} else {
			PostError("expected end%s, got: endloop", fu.blocks.top.kind)
		}
		return FAILED
	}

	blk := fu.blocks.top

	fu.ExpandJump(blk.label, line.pos)

	if blk.skip != "" {
		fu.EmitLabel(blk.skip, line.pos)
	}

	fu.blocks.Pop()

	if line.Len() > 1 {
		PostError("extra rubbish after endloop statement")
		return FAILED
	}
	return OK
}

func (fu *FuncDef) ExpandJump(label string, pos Position) *Node {
	t_jump  := NewNode(NL_Name, "jump", pos)
	t_label := NewNode(NL_Name, label,  pos)

	new_line := NewNode(NG_Line, "", pos)
	new_line.Add(t_jump)
	new_line.Add(t_label)

	fu.body.Add(new_line)

	return new_line
}

func (fu *FuncDef) IsConstExpression(t *Node) bool {
	if t.IsLiteral() {
		return true
	}

	switch t.kind {
	case NL_Name:
		// assume the name is a constant.
		// the evaluation logic will catch any problem.
		return true

	case NG_Expr:
		switch t.Len() {
		case 2:
			if !IsOperator(t.children[0]) {
				return false
			}
			return fu.IsConstExpression(t.children[1])

		case 3:
			if !IsOperator(t.children[1]) {
				return false
			}

			L := fu.IsConstExpression(t.children[0])
			R := fu.IsConstExpression(t.children[2])

			return L && R
		}
	}

	return false
}

//----------------------------------------------------------------------

// ActiveBlocks keeps track of active `if` and `loop` blocks in code,
// as well as `array`, `struct` and `union` blocks in data.
type ActiveBlocks struct {
	top *BlockInfo
}

type BlockInfo struct {
	kind   string
	label  string
	skip   string
	else_  bool
	start  Position
	group  *Node

	parent *BlockInfo
}

func (actb *ActiveBlocks) Empty() bool {
	return actb.top == nil
}

func (actb *ActiveBlocks) Match(kind string) bool {
	if actb.top != nil {
		return actb.top.kind == kind
	}
	return false
}

func (actb *ActiveBlocks) Push(kind string, start Position) *BlockInfo {
	blk := new(BlockInfo)
	blk.kind = kind
	blk.start = start
	blk.parent = actb.top

	actb.top = blk

	return blk
}

func (actb *ActiveBlocks) Pop() *BlockInfo {
	old_top := actb.top
	actb.top = actb.top.parent
	return old_top
}

//----------------------------------------------------------------------

type EvalResult int
const (
	EVAL_OK  EvalResult = iota
	EVAL_Error     // some error occurred
	EVAL_Unknown   // at least one constant is not eval'd yet
)

func EvalExpression(t *Node, ctx *FuncDef) (EvalResult, *Node) {
	if t.Len() < 2 {
		PostError("const expression in () is too short")
		return EVAL_Error, nil
	}
	if t.Len() > 3 {
		PostError("const expression in () is too long")
		return EVAL_Error, nil
	}

	if t.Len() == 2 {
		return EvalUnaryExpr(t, ctx)
	}

	op := t.children[1]
	if !IsOperator(op) {
		PostError("expected operator in const expr, got: %s", op.String())
		return EVAL_Error, nil
	}

	L_res, L := EvalNode(t.children[0], ctx)
	R_res, R := EvalNode(t.children[2], ctx)

	if L_res != EVAL_OK { return L_res, nil }
	if R_res != EVAL_OK { return R_res, nil }

	// allow mixing integer are character literals
	L_kind := L.kind
	R_kind := R.kind

	if L_kind == NL_Char { L_kind = NL_Integer }
	if R_kind == NL_Char { R_kind = NL_Integer }

	if L_kind != R_kind {
		PostError("type mismatch in const expression")
		return EVAL_Error, nil
	}

	switch L.kind {
	case NL_Float:
		return EvalFloat(op.str, L, R)

	case NL_String:
		return EvalString(op.str, L, R)

	case NL_Bool:
		return EvalBool(op.str, L, R)

	default:
		// if one of the arguments is negative, use signed ints.
		// otherwise use unsigned ints to compute the result.
		if L.str[0] == '-' || R.str[0] == '-' {
			return EvalSignedInt(op.str, L, R)
		} else {
			return EvalUnsignedInt(op.str, L, R)
		}
	}
}

func EvalUnaryExpr(t *Node, ctx *FuncDef) (EvalResult, *Node) {
	op := t.children[0]
	if !IsOperator(op) {
		PostError("expected operator in const expr, got: %s", op.String())
		return EVAL_Error, nil
	}

	L_res, L := EvalNode(t.children[1], ctx)
	if L_res != EVAL_OK {
		return L_res, nil
	}

	switch op.str {
	case "!":
		// boolean not
		if L.kind != NL_Bool {
			PostError("operator '%s' requires a boolean", op.str)
			return EVAL_Error, nil
		}

		V := (L.str == "1")
		return EvalMakeBool(! V, t.pos)

	case "-", "~":
		// integer negate and bit-flip
		if L.kind != NL_Integer {
			PostError("operator '%s' requires an integer", op.str)
			return EVAL_Error, nil
		}

		V, V_err := strconv.ParseInt(L.str, 0, 64)
		if V_err != nil {
			PostError("bad integer in const expression: %s", L.str)
			return EVAL_Error, nil
		}

		if op.str == "-" {
			V = 0 - V
		} else {
			V = 0 - (V + 1)
		}
		int_str := strconv.FormatInt(V, 10)
		return EVAL_OK, NewNode(NL_Integer, int_str, L.pos)

	default:
		PostError("operator '%s' cannot be used in that context", op.str)
		return EVAL_Error, nil
	}
}

func EvalNode(t *Node, ctx *FuncDef) (EvalResult, *Node) {
	// this returns a literal token (like NL_Integer), or NIL when
	// there was a parsing problem OR a constant which has not been
	// resolved yet.

	switch t.kind {
	case NL_Integer, NL_Char, NL_Float, NL_String, NL_Bool:
		return EVAL_OK, t

	case NL_Name:
		// IsConstExpression() is lax with identifiers, so we must
		// check here that the name is not a local variable.
		if ctx != nil {
			if ctx.seen_locals[t.str] {
				PostError("local '%s' cannot be used in const expression", t.str)
				return EVAL_Error, nil
			}
		}

		def := all_defs[t.str]
		if def == nil || def.kind != DEF_Const {
			PostError("no such constant: %s", t.str)
			return EVAL_Error, nil
		}
		other := def.d_const
		if other.expr != nil {
			return EVAL_Unknown, nil
		}
		return EVAL_OK, other.MakeLiteral(Position{})

	case NG_Expr:
		return EvalExpression(t, ctx)

	default:
		PostError("bad value in const expression, got: %s", t.String())
		return EVAL_Error, nil
	}
}

func EvalString(op string, L, R *Node) (EvalResult, *Node) {
	if op != "+" {
		PostError("cannot use operator '%s' on strings", op)
		return EVAL_Error, nil
	}

	return EVAL_OK, NewNode(NL_String, L.str + R.str, L.pos)
}

func EvalBool(op string, L, R *Node) (EvalResult, *Node) {
	LV := (L.str == "1")
	RV := (R.str == "1")

	var result bool

	switch op {
	case "&":  result = LV && RV
	case "|":  result = LV || RV
	case "==": result = LV == RV
	case "!=": result = LV != RV
	default:
		PostError("cannot use operator '%s' on boolean", op)
		return EVAL_Error, nil
	}

	return EvalMakeBool(result, L.pos)
}

func EvalFloat(op string, L, R *Node) (EvalResult, *Node) {
	// WISH: format result as hexadecimal if LHS is hex.

	LV, L_err := strconv.ParseFloat(L.str, 64)
	RV, R_err := strconv.ParseFloat(R.str, 64)

	if L_err != nil {
		PostError("bad float value in const expression: %s", L.str)
		return EVAL_Error, nil
	}
	if R_err != nil {
		PostError("bad float value in const expression: %s", R.str)
		return EVAL_Error, nil
	}

	// check for division by zero
	if op == "/" || op == "%" {
		if RV == 0.0 {
			PostError("division by zero in const expression")
			return EVAL_Error, nil
		}
	}

	var result float64

	switch op {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV - math.Trunc(LV / RV) * RV

	case "==": return EvalMakeBool(LV == RV, L.pos)
	case "!=": return EvalMakeBool(LV != RV, L.pos)
	case "<":  return EvalMakeBool(LV <  RV, L.pos)
	case "<=": return EvalMakeBool(LV <= RV, L.pos)
	case ">":  return EvalMakeBool(LV >  RV, L.pos)
	case ">=": return EvalMakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' on floats", op)
		return EVAL_Error, nil
	}

	float_str := strconv.FormatFloat(result, 'g', -1, 64)
	return EVAL_OK, NewNode(NL_Float, float_str, L.pos)
}

func EvalSignedInt(op string, L, R *Node) (EvalResult, *Node) {
	LV, L_err := strconv.ParseInt(L.str, 0, 64)
	RV, R_err := strconv.ParseInt(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return EVAL_Error, nil
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return EVAL_Error, nil
	}

	// check for bad shift count
	if op == "<<" || op == ">>" {
		if RV < 0 {
			PostError("bad shift count in const expression")
			return EVAL_Error, nil
		}
	}

	// check for division by zero
	if op == "/" || op == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return EVAL_Error, nil
		}
	}

	var result int64

	switch op {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << uint64(RV)
	case ">>": result = LV >> uint64(RV)

	case "==": return EvalMakeBool(LV == RV, L.pos)
	case "!=": return EvalMakeBool(LV != RV, L.pos)
	case "<":  return EvalMakeBool(LV <  RV, L.pos)
	case "<=": return EvalMakeBool(LV <= RV, L.pos)
	case ">":  return EvalMakeBool(LV >  RV, L.pos)
	case ">=": return EvalMakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' on integers", op)
		return EVAL_Error, nil
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatInt(result, 10)
	return EVAL_OK, NewNode(NL_Integer, int_str, L.pos)
}

func EvalUnsignedInt(op string, L, R *Node) (EvalResult, *Node) {
	LV, L_err := strconv.ParseUint(L.str, 0, 64)
	RV, R_err := strconv.ParseUint(R.str, 0, 64)

	if L_err != nil {
		PostError("bad integer in const expression: %s", L.str)
		return EVAL_Error, nil
	}
	if R_err != nil {
		PostError("bad integer in const expression: %s", R.str)
		return EVAL_Error, nil
	}

	// check for subtraction producing a negative value
	if op == "-" && RV > LV {
		result  := int64(LV) - int64(RV)
		int_str := strconv.FormatInt(result, 10)
		return EVAL_OK, NewNode(NL_Integer, int_str, L.pos)
	}

	// check for division by zero
	if op == "/" || op == "%" {
		if RV == 0 {
			PostError("division by zero in const expression")
			return EVAL_Error, nil
		}
	}

	var result uint64

	switch op {
	case "+":  result = LV + RV
	case "-":  result = LV - RV
	case "*":  result = LV * RV
	case "/":  result = LV / RV
	case "%":  result = LV % RV

	case "&":  result = LV & RV
	case "|":  result = LV | RV
	case "~":  result = LV ^ RV
	case "<<": result = LV << RV
	case ">>": result = LV >> RV

	case "==": return EvalMakeBool(LV == RV, L.pos)
	case "!=": return EvalMakeBool(LV != RV, L.pos)
	case "<":  return EvalMakeBool(LV <  RV, L.pos)
	case "<=": return EvalMakeBool(LV <= RV, L.pos)
	case ">":  return EvalMakeBool(LV >  RV, L.pos)
	case ">=": return EvalMakeBool(LV >= RV, L.pos)

	default:
		PostError("cannot use operator '%s' on integers", op)
		return EVAL_Error, nil
	}

	// WISH: format result as hexadecimal if LHS is hex.

	int_str := strconv.FormatUint(result, 10)
	return EVAL_OK, NewNode(NL_Integer, int_str, L.pos)
}

func EvalMakeBool(b bool, pos Position) (EvalResult, *Node) {
	if b {
		return EVAL_OK, NewNode(NL_Bool, "1", pos)
	} else {
		return EVAL_OK, NewNode(NL_Bool, "0", pos)
	}
}

func IsOperator(t *Node) bool {
	if t.kind == NL_Name {
		return IsOperatorName(t.str)
	}
	return false
}

func IsOperatorName(s string) bool {
	switch s {
	case "+", "-", "*", "/", "%",
		"&", "|", "~", "<<", ">>", "!",
		"==", "!=", "<", "<=", ">", ">=":
		return true

	default:
		return false
	}
}
