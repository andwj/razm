// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"
import "fmt"

func DeduceAllFunctions() {
	for _, def := range ordered_defs {
		if def.kind == DEF_Func {
			def.d_func.Deduce()
		}
	}
	for _, fu := range all_methods {
		fu.Deduce()
	}

	ErrorsSetFunction("")
}

func ParseAllFunctions() {
	// visit all inline functions first, since a problem within one
	// means that every normal function using it would also fail.
	DoParseAllFunctions(true)

	if !have_errors {
		DoParseAllFunctions(false)
	}
}

func DoParseAllFunctions(inline bool) {
	for _, def := range ordered_defs {
		if def.kind == DEF_Func && !def.d_func.extern && def.d_func.inline == inline {
			def.d_func.Parse()
		}
	}
	for _, fu := range all_methods {
		if !fu.extern && fu.inline == inline {
			fu.Parse()
		}
	}
	ErrorsSetFunction("")
}

func CompileAllFunctions() {
	for _, def := range ordered_defs {
		if def.kind == DEF_Func {
			arch.CompileFunction(def.d_func)
		}
	}
	for _, fu := range all_methods {
		arch.CompileFunction(fu)
	}
	ErrorsSetFunction("")
}

//----------------------------------------------------------------------

// Deduce determines the overall type of the function.
// It does not examine the body of the function.
func (fu *FuncDef) Deduce() error {
	ErrorsSetFunction(fu.name)
	ErrorsSetPos(fu.raw_pars.pos)

	fu.ty = ParseParameters(fu.raw_pars)
	if fu.ty == nil {
		return FAILED
	}
	if ValidateType(fu.ty) != OK {
		return FAILED
	}

	// store methods into the Type which they belong, and check
	// for duplicate names.
	if fu.method {
		if len(fu.ty.param) == 0 {
			PostError("method definition with no receiver")
			return FAILED
		}
		self_type := fu.ty.param[0].ty
		if self_type.kind != TYP_Pointer {
			PostError("method receiver must be a pointer, got a %s",
				self_type.SimpleName())
			return FAILED
		}
		belong_type := self_type.sub
		if belong_type.def == nil {
			PostError("method receiver must refer to a custom type")
			return FAILED
		}
		if belong_type.FindMethod(fu.name) != nil {
			PostError("method '%s' already defined in type %s",
				fu.name, belong_type.def.name)
			return FAILED
		}
		belong_type.AddMethod(fu)
	}

	fu.ptr = NewPointerType(fu.ty)
	return OK
}

//----------------------------------------------------------------------

// Parse analyses the code in the function and produces higher-level
// nodes for the Compile method to use.  it will decompose `loop` and
// `if` forms into simpler forms, and create temporaries for usage of
// expressions in `()` parentheses.
func (fu *FuncDef) Parse() error {
	ErrorsSetFunction(fu.name)

	// setup some stuff
	fu.locals = make(map[string]*LocalInfo)
	fu.labels = make(map[string]bool)
	fu.seen_locals = make(map[string]bool)
	fu.used_labels = make(map[string]bool)
	fu.params = make([]*LocalInfo, 0)

	// create a local for each parameter
	for idx, par := range fu.ty.param {
		local := fu.NewLocal(par.name, par.ty)
		local.param_idx = idx
		fu.params = append(fu.params, local)
	}

	// expand usages of expressions in (), handle `if` and `loop`
	if fu.Expand() != OK {
		return FAILED
	}

	// expansion phase used this, reset it now
	fu.seen_locals = make(map[string]bool)

	// convert each line into a higher-level node
	if fu.ParseBody() != OK {
		return FAILED
	}

	if fu.MissingLabels() != OK {
		return FAILED
	}
	if fu.FlowAnalysis() != OK {
		return FAILED
	}

// Dumpty("\nfunction " + fu.name, fu.body, 4)

	return OK
}

func (fu *FuncDef) ParseBody() error {
	old_body := fu.body
	fu.body = NewNode(NG_Body, "code", old_body.pos)

	for _, line := range old_body.children {
		if fu.ParseLine(line) != OK {
			// allow a limited number of errors per function
			fu.error_num += 1
			if fu.error_num > 5 && !Options.all_errors {
				return FAILED
			}
		}
	}

	if fu.error_num > 0 {
		return FAILED
	}

	// ensure the body is never empty (for live ranges)
	if fu.body.Len() == 0 {
		cmt := NewNode(NH_Comment, "empty function", fu.body.pos)
		fu.body.Add(cmt)
	}

	return OK
}

func (fu *FuncDef) ParseLine(line *Node) error {
	// not that lines are never empty...

	ErrorsSetPos(line.pos)

	// check that labels are unique
	if line.kind == NH_Label {
		label_str := line.str
		if fu.labels[label_str] {
			PostError("duplicate label: %s", label_str)
			return FAILED
		}
		if fu.locals[label_str] != nil {
			PostError("label name clashes with a local: %s", label_str)
			return nil
		}
		fu.labels[label_str] = true
		fu.body.Add(line)
		return OK
	}

	if line.kind != NG_Line {
		panic("weird node for ParseLine")
	}

	fu.CommentLine(line)

	// handle special statements....
	head := line.children[0]
	switch {
	case head.Match("return"):
		return fu.ParseReturn(line)

	case head.Match("tail-call"):
		return fu.ParseTailCall(line)

	case head.Match("jump"):
		return fu.ParseJump(line)

	case head.Match("mem-write"):
		return fu.ParseMemWrite(line)

	case head.Match("swap"):
		return fu.ParseSwap(line)
	}

	// split line around the `=` equals sign, if present
	equal_pos := line.Find("=")
	children  := line.children

	if equal_pos < 0 {
		return fu.ParseDrop(children)
	}

	// we have `=` so this line becomes a NH_Move node...

	// parse the destination.
	// it is either a local var (with or without a type spec),
	// or an access expression in `[]` brackets.
	var dest *Node

	var_part := children[0:equal_pos]
	children = children[equal_pos+1:]

	if len(var_part) == 0 {
		PostError("missing destination before '='")
		return FAILED
	}

	t_var := var_part[0]

	switch t_var.kind {
	case NG_Access:
		if len(var_part) > 1 {
			PostError("extra rubbish after access in []")
			return FAILED
		}
		dest = fu.OperandAccess(t_var, false)
		if dest == nil {
			return FAILED
		}

	case NL_Name:
		if ValidateName(t_var, "local", ALLOW_SHADOW) != OK {
			return FAILED
		}

		// inhibit future error messages about the local if the rest
		// of this line fails to parse.
		fu.seen_locals[t_var.str] = true

		dest = NewNode(NP_Local, t_var.str, t_var.pos)

		// handle a type following the name, validate it
		if len(var_part) > 1 {
			ty, count := ParseTypeSpec(var_part[1:], "a local")
			if ty == nil {
				return FAILED
			}
			dest.ty = ty

			switch ty.kind {
			case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
				// ok
			default:
				PostError("local var '%s' cannot be %s", t_var.str, ty.SimpleName())
				return FAILED
			}

			if 1+count != len(var_part) {
				PostError("extra rubbish after type spec")
				return FAILED
			}
		} else {
			// if local is not new, grab its type
			local := fu.locals[t_var.str]
			if local != nil {
				dest.ty = local.ty
			}
		}

	default:
		PostError("invalid destination, got: %s", t_var.String())
		return FAILED
	}

	// the stuff after the `=` is the computation, often a function
	// call or intrinsic, but could just be a literal value.

	source := fu.ParseComputation(children, dest.ty)
	if source == nil {
		return FAILED
	}
	if source.ty == nil {
		// source is a literal with no type, and dest is a new local
		// without a explicit type.
		PostError("local var '%s' needs an explicit type (literals are untyped)", dest.str)
		return nil
	}

	// if the destination is a new local, create it now, and if no type
	// was specified then deduce it from the computation.

	if dest.kind == NP_Local {
		local := fu.locals[dest.str]

		if local == nil {
			if fu.labels[dest.str] {
				PostError("local name clashes with a label: %s", dest.str)
				return nil
			}

			local = fu.NewLocal(dest.str, dest.ty)

		} else {
			if dest.ty != nil {
				// if a type spec was used for an existing local, check it
				// matches the first one *exactly*.
				if TypeCompare(local.ty, dest.ty) &&
					local.ty.unsigned == dest.ty.unsigned {
					// ok
				} else {
					PostError("local '%s' given two different types: %s, %s",
						local.name, local.ty.String(), dest.ty.String())
					return FAILED
				}
			}
		}

		if local.ty == nil {
			local.ty = source.ty
		}
		dest.ty = local.ty

		switch local.ty.kind {
		case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
			// ok
		default:
			PostError("local var '%s' inferred as %s",
				local.name, local.ty.SimpleName())
			return FAILED
		}
	} else {
		// WISH: detect read-only global in `[]` in destination

		switch dest.ty.kind {
		case TYP_Array, TYP_Struct, TYP_Union, TYP_Function:
			PostError("cannot assign to a whole %s", dest.ty.SimpleName())
			return FAILED
		}
	}

	if dest.ty == nil {
		panic("dest without type")
	}

	// check type compatibility of assignment
	if !TypeCompare(dest.ty, source.ty) {
		PostError("type mismatch in assignment: wanted %s, got %s",
			dest.ty.String(), source.ty.String())
		return FAILED
	}

	t_move := NewNode(NH_Move, "", dest.pos)
	t_move.Add(dest)
	t_move.Add(source)

	fu.body.Add(t_move)
	return OK
}

func (fu *FuncDef) ParseDrop(children []*Node) error {
	comp := fu.ParseComputation(children, nil)
	if comp == nil {
		return FAILED
	}
	if comp.ty == nil || comp.kind == NL_Bool {
		PostError("found a value wandering around by itself")
		return FAILED
	}

	switch comp.kind {
	case NC_Call, NC_MemRead:
		// always keep these.

	case NC_Builtin:
		bu := builtins[comp.str]
		if (bu.flags & BF_SIDE_FX) == 0 {
			PostError("no destination for %s intrinsic", comp.str)
			return FAILED
		}

	default:
		PostError("computation with no destination")
		return FAILED
	}

	t_drop := NewNode(NH_Drop, "", children[0].pos)
	t_drop.Add(comp)

	fu.body.Add(t_drop)
	return OK
}

func (fu *FuncDef) ParseReturn(line *Node) error {
	ret_type := fu.ty.sub

	t_ret := NewNode(NH_Return, "", line.pos)

	if ret_type.kind == TYP_NoReturn {
		PostError("return used in a no-return function")
		return FAILED

	} else if ret_type.kind == TYP_Void {
		if line.Len() > 1 {
			PostError("return with value in void function")
			return FAILED
		}

	} else {
		if line.Len() < 2 {
			PostError("missing value after return")
			return FAILED
		}

		comp := fu.ParseComputation(line.children[1:], ret_type)
		if comp == nil {
			return FAILED
		}
		if comp.ty == nil {
			panic("return value without a type")
		}

		if !TypeCompare(comp.ty, ret_type) {
			PostError("type mismatch on return value: wanted %s, got %s",
				ret_type.String(), comp.ty.String())
			return FAILED
		}

		t_ret.Add(comp)
	}

	fu.body.Add(t_ret)
	return OK
}

func (fu *FuncDef) ParseJump(line *Node) error {
	if line.Len() < 2 {
		PostError("missing label for jump statement")
		return FAILED
	}

	t_label := line.children[1]

	if t_label.kind != NL_Name {
		PostError("expected label name in jump, got: %s", t_label.String())
		return FAILED
	}

	// mark label as used
	fu.used_labels[t_label.str] = true

	t_jump := NewNode(NH_Jump, t_label.str, line.pos)

	if line.Len() > 2 {
		t_cond, negated := fu.ParseJumpCondition(line.children[2:])
		if t_cond == nil {
			return FAILED
		}

		// handle TRUE and FALSE literals
		if t_cond.kind == NL_Bool {
			val := (t_cond.str == "1")
			if negated {
				val = !val
			}

			if val {
				fu.body.Add(t_jump)
			} else {
				// omit the jump completely
			}
			return OK
		}

		if negated {
			t_jump.kind = NH_JumpNot
		}
		t_jump.Add(t_cond)
	}

	fu.body.Add(t_jump)
	return OK
}

func (fu *FuncDef) ParseJumpCondition(children []*Node) (*Node, bool) {
	negated := false

	t_word  := children[0]
	children = children[1:]

	if t_word.Match("if") {
		// ok
	} else if t_word.Match("unless") {
		negated = true
	} else {
		PostError("expected 'if' or 'unless', got: %s", t_word.String())
		return nil, false
	}

	// handle "not ..." as a negation too
	if len(children) > 0 && children[0].Match("not") {
		negated = !negated
		children = children[1:]
	}

	if len(children) == 0 {
		PostError("missing condition in jump statement")
		return nil, false
	}

	comp := fu.ParseComputation(children, nil)
	if comp == nil {
		return nil, false
	}
	if comp.kind == NL_Bool {
		comp.ty = bool_type
	}
	if comp.ty == nil || comp.ty.kind != TYP_Bool {
		PostError("condition for if/jump must have boolean type")
		return nil, false
	}

	return comp, negated
}

func (fu *FuncDef) ParseMemWrite(line *Node) error {
	if line.Len() < 2 || line.children[1].kind != NG_Access {
		PostError("missing access in [] for mem-write statement")
		return FAILED
	}
	if line.Len() < 3 {
		PostError("missing value for mem-write statement")
		return FAILED
	}
	if line.Len() > 3 {
		PostError("extra rubbish at end of mem-write statement")
		return FAILED
	}

	dest := fu.OperandAccess(line.children[1], false)
	if dest == nil {
		return FAILED
	}

	op_val := fu.ParseOperand(line.children[2], false)
	if op_val == nil {
		return FAILED
	}

	// check whether a literal is suitable for the dest type, and
	// if so then give it a type (for the code generator backend).
	if op_val.IsLiteral() {
		op_val.ty = dest.ty
		if fu.CheckLiteral(op_val) != OK {
			return FAILED
		}
	} else {
		// check type compatibility of assignment
		if !TypeCompare(dest.ty, op_val.ty) {
			PostError("type mismatch in mem-write: wanted %s, got %s",
				dest.ty.String(), op_val.ty.String())
			return FAILED
		}
	}

	t_write := NewNode(NH_MemWrite, "", line.pos)
	t_write.Add(dest)
	t_write.Add(op_val)

	fu.body.Add(t_write)
	return OK
}

func (fu *FuncDef) ParseSwap(line *Node) error {
	if line.Len() < 3 {
		PostError("missing operands for swap statement")
		return FAILED
	}
	if line.Len() > 3 {
		PostError("extra rubbish at end of swap statement")
		return FAILED
	}

	left  := fu.ParseOperand(line.children[1], false)
	right := fu.ParseOperand(line.children[2], false)

	if left == nil || right == nil {
		return FAILED
	}

	if left.IsLiteral() || right.IsLiteral() {
		PostError("cannot use a literal value in a swap statement")
		return FAILED
	}

	left_ok  := ( left.kind == NP_Local ||  left.kind == NP_Memory)
	right_ok := (right.kind == NP_Local || right.kind == NP_Memory)

	if !left_ok || !right_ok {
		PostError("operands in a swap statement must be assignable")
		return FAILED
	}

	if left.kind == NP_Local && right.kind == NP_Local {
		if left.str == right.str {
			PostError("cannot swap a local with itself")
			return FAILED
		}
	}

	if !TypeCompare(left.ty, right.ty) {
		PostError("type mismatch in swap: got %s and %s",
			left.ty.String(), right.ty.String())
		return FAILED
	}

	t_swap := NewNode(NH_Swap, "", line.pos)
	t_swap.Add(left)
	t_swap.Add(right)

	fu.body.Add(t_swap)
	return OK
}

func (fu *FuncDef) ParseTailCall(line *Node) error {
	if line.Len() < 2 {
		PostError("missing function for tail-call statement")
		return FAILED
	}

	t_call := fu.ComputeExplicitCall(line.children)
	if t_call == nil {
		return FAILED
	}

	// t_call should always be an NC_Call here

	called_func := fu.GetCalledFunc(t_call)
	if called_func != nil && called_func.inline {
		PostError("cannot tail call an inline function")
		return FAILED
	}

	fun_type := t_call.children[0].ty

	// this limitation stems from the Win64 ABI, since tail-calls can
	// only pass parameters via registers (NEVER via the stack).
	if len(fun_type.param) > 4 {
		PostError("too many parameters for a tail call (limit is 4)")
		return FAILED
	}

	// if this function (the caller) returns something or is `no-return`,
	// require the called function to return the SAME type.
	if fu.ty.sub.kind != TYP_Void {
		if !TypeCompare(fun_type.sub, fu.ty.sub) {
			PostError("type mismatch on tail-call return: wanted %s, got %s",
				fu.ty.sub.String(), fun_type.sub.String())
			return FAILED
		}
	}

	t_call.kind = NH_TailCall

	fu.body.Add(t_call)
	return OK
}

func (fu *FuncDef) GrabReceiver(t_self *Node) *Node {
	op_val := fu.ParseOperand(t_self, false)
	if op_val == nil {
		return nil
	}

	if op_val.IsLiteral() {
		PostError("method receiver cannot be a literal")
		return nil
	}

	self_type := op_val.ty
	if self_type == nil {
		panic("receiver without a type")
	}
	if self_type.kind != TYP_Pointer {
		PostError("method receiver must be a pointer, got a %s",
			self_type.SimpleName())
		return nil
	}
	if self_type.sub.def == nil {
		PostError("method receiver must refer to a custom type")
		return nil
	}

	return op_val
}

func (fu *FuncDef) GrabParameters(t_call, recv *Node, params []*Node, fun_type *Type) error {
	if len(params) != len(fun_type.param) {
		PostError("wrong number of parameters: wanted %d, got %d",
			len(fun_type.param), len(params))
		return nil
	}

	for i, par := range fun_type.param {
		// the receiver of methods is already parsed
		if i == 0 && recv != nil {
			t_call.Add(recv)
			continue
		}

		t_par := params[i]

		// back-end wants parameters to be simple, so shift an access
		// into a temporary var.
		op_val := fu.ParseOperand(t_par, false)
		if op_val == nil {
			return FAILED
		}

		if op_val.IsLiteral() {
			// check if the literal is suitable
			op_val.ty = par.ty
			if fu.CheckLiteral(op_val) != OK {
				return FAILED
			}

		} else if op_val.ty == nil {
			panic("operand without a type")

		} else if !TypeCompare(op_val.ty, par.ty) {
			PostError("type mismatch on parameter %s: wanted %s, got %s",
				par.name, par.ty.String(), op_val.ty.String())
			return FAILED
		}

		t_call.Add(op_val)
	}

	return OK
}

func (fu *FuncDef) ShiftAccess(op *Node) *Node {
	if op.kind == NP_Memory {
		if op.ty == nil {
			panic("NP_Memory with no type")
		}

		// create the temporary local
		temp  := fu.NewTemporary("__access")
		local := fu.NewLocal(temp, op.ty)

		var_dest := NewNode(NP_Local, temp, op.pos)
		var_dest.ty = local.ty

		// insert new assignment before the current line
		t_move := NewNode(NH_Move, "", op.pos)
		t_move.Add(var_dest)
		t_move.Add(op)

		fu.body.Add(t_move)

		op = NewNode(NP_Local, temp, op.pos)
		op.ty = local.ty
	}

	return op
}

func (fu *FuncDef) CommentLine(line *Node) {
	text := fu.LineAsText(line)
	comment := NewNode(NH_Comment, text, line.pos)

	fu.body.Add(comment)
}

func (fu *FuncDef) LineAsText(line *Node) string {
	s := ""

	for i, t := range line.children {
		token := t.str

		switch t.kind {
		case NL_String: token = "\"...\""
		case NL_Null:   token = "NULL"

		case NL_Bool:
			if t.str == "0" {
				token = "FALSE"
			} else if t.str == "1" {
				token = "TRUE"
			} else {
				token = t.str
			}

		case NL_Char:
			ch, _ := strconv.Atoi(t.str)
			if 32 <= ch && ch <= 126 {
				token = fmt.Sprintf("'%c'", rune(ch))
			} else if ch <= 0xFFFF {
				token = fmt.Sprintf("'\\u%04X'", ch)
			} else {
				token = fmt.Sprintf("'\\U%08X'", ch)
			}

		case NG_Expr:   token = "(" + fu.LineAsText(t) + ")"
		case NG_Access: token = "[" + fu.LineAsText(t) + "]"
		case NG_Data:   token = "{" + fu.LineAsText(t) + "}"
		}

		if token == "" {
			token = "????"
		}
		if i > 0 {
			s += " "
		}
		s += token
	}

	return s
}

//----------------------------------------------------------------------

func (fu *FuncDef) ParseComputation(children []*Node, dest_type *Type) *Node {
	if len(children) == 0 {
		PostError("missing value/computation after '='")
		return nil
	}

	head := children[0]

	switch {
	case head.Match("call"):
		return fu.ComputeExplicitCall(children)

	case head.Match("conv"):
		return fu.ComputeConv(children)

	case head.Match("raw-cast"):
		return fu.ComputeRawCast(children)

	case head.Match("fun"):
		return fu.ComputeFunAddr(children)

	case head.Match("mem-read"):
		return fu.ComputeMemRead(children)

	case head.Match("stack-var"):
		return fu.ComputeStackVar(children)

	case head.Match("sel-if"):
		return fu.ComputeSelect(children, dest_type)
	}

	if head.IsField() {
		return fu.ComputeMethodCall(children)
	}

	if head.kind == NL_Name {
		bu := builtins[head.str]
		if bu != nil {
			return fu.ComputeBuiltin(children, bu)
		}
	}

	source := fu.ParseOperand(head, true)
	if source == nil {
		return nil
	}
	if source.kind == NP_FuncRef {
		return fu.ComputeCall(children, source)
	}

	/* we have a pure assignment */

	if len(children) > 1 {
		PostError("extra rubbish after assignment value")
		return nil
	}

	// assign a type if possible
	// [ caller must check as well! ]

	if source.ty == nil {
		if !source.IsLiteral() {
			panic("non-literal operand with no type")
		}

		if dest_type != nil {
			// check whether a literal is suitable for the dest type, and
			// if so then give it a type (for the code generator backend).
			source.ty = dest_type
			if fu.CheckLiteral(source) != OK {
				return nil
			}
		}
	}

	return source
}

func (fu *FuncDef) ComputeExplicitCall(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing function for call statement")
		return nil
	}

	// method call?
	if children[1].IsField() {
		return fu.ComputeMethodCall(children[1:])
	}

	t_func := fu.ParseOperand(children[1], true)
	if t_func == nil {
		return nil
	}

	fun_type := t_func.ty
	if fun_type != nil && fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type == nil || fun_type.kind != TYP_Function {
		PostError("call requires a function argument")
		return nil
	}

	return fu.ComputeCall(children[1:], t_func)
}

func (fu *FuncDef) ComputeCall(children []*Node, t_func *Node) *Node {
	fun_type := t_func.ty
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}

	t_call := NewNode(NC_Call, "", t_func.pos)
	t_call.ty = fun_type.sub
	t_call.Add(t_func)

	params := children[1:]
	if fu.GrabParameters(t_call, nil, params, fun_type) != OK {
		return nil
	}
	return t_call
}

func (fu *FuncDef) ComputeMethodCall(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing receiver for method call")
		return nil
	}

	t_method := children[0]

	recv := fu.GrabReceiver(children[1])
	if recv == nil {
		return nil
	}

	belong_type := recv.ty.sub

	method := belong_type.FindMethod(t_method.str)
	if method == nil {
		PostError("unknown method '%s' in type %s",
			t_method.str, belong_type.def.name)
		return nil
	}

	t_func := NewNode(NP_Method, method.name, t_method.pos)
	t_func.ty = method.ty

	t_call := NewNode(NC_Call, "", t_method.pos)
	t_call.ty = method.ty.sub
	t_call.Add(t_func)

	// this includes the receiver, but it is only there tp simplify the
	// code in GrabParameters(), it is not used.
	params := children[1:]

	if fu.GrabParameters(t_call, recv, params, method.ty) != OK {
		return nil
	}
	return t_call
}

func (fu *FuncDef) ComputeBuiltin(children []*Node, bu *Builtin) *Node {
	if bu.args < 1 || bu.args > 2 {
		panic("intrinsic with strange # of args")
	}

	args := children[1:]

	multi := ((bu.flags & BF_MULTI) != 0)

	if len(args) < bu.args || (len(args) > bu.args && !multi) {
		PostError("wrong number of args for %s: wanted %d, got %d",
			bu.name, bu.args, len(args))
		return nil
	}

	/* firstly. parse all the arguments */

	for i := range args {
		args[i] = fu.ParseOperand(args[i], false)
		if args[i] == nil {
			return nil
		}
	}

	// for type stuff we mainly look at the LHS and RHS
	var L *Node
	var R *Node

	if len(args) >= 1 { L = args[0] }
	if len(args) >= 2 { R = args[1] }

	/* determine types, check L and R are compatible */

	var L_type *Type
	var R_type *Type

	if len(args) == 1 {
		L_type = L.ty

		if L_type == nil {
			PostError("unknown type for %s intrinsic (untyped literal)", bu.name)
			return nil
		}
	} else {
		L_type = L.ty
		R_type = R.ty

		if L_type == nil && R_type == nil {
			PostError("unknown type for %s intrinsic (untyped literal)", bu.name)
			return nil
		}

		if L_type == nil {
			if bu.rhs == 0 {
				L.ty = R_type
				L_type = L.ty
				if fu.CheckLiteral(L) != OK {
					return nil
				}
			} else {
				PostError("unknown type for %s intrinsic (untyped literal)", bu.name)
				return nil
			}
		}

		if R_type == nil {
			if bu.name == "ishl" || bu.name == "ishr" {
				R.ty = u8_type
			} else if bu.name == "padd" || bu.name == "psub" {
				R.ty = ssize_type
			} else if bu.rhs == 0 {
				R.ty = L_type
			} else {
				PostError("unknown type for %s intrinsic (untyped literal)", bu.name)
				return nil
			}
			R_type = R.ty

			if fu.CheckLiteral(R) != OK {
				return nil
			}
		}
	}

	// handle literals in any additional arguments
	for i := 2; i < len(args); i++ {
		A := args[i]

		if A.ty == nil {
			if bu.name == "padd" || bu.name == "psub" {
				A.ty = ssize_type
			} else {
				A.ty = R_type
			}

			if fu.CheckLiteral(A) != OK {
				return nil
			}
		}
	}

	// check that a constant shift count is valid
	if bu.name == "ishl" || bu.name == "ishr" {
		if R.kind == NL_Integer {
			val, err := strconv.ParseInt(R.str, 0, 64)

			if err != nil || val < 0 {
				PostError("invalid shift count: %s", R.str)
				return nil
			}
			if val >= int64(L_type.bits) {
				PostError("shift count too high (>= %d)", L_type.bits)
				return nil
			}
		}
	}

	/* perform type-checking between the arguments */

	for i := 1; i < len(args); i++ {
		A := args[i]

		if i == 1 && bu.rhs != 0 {
			// ok -- RHS type is allowed to be different from LHS type
			continue
		}
		if L_type.kind == TYP_Pointer && A.ty.kind == TYP_Pointer {
			// ok -- the `pdiff` intrinsic is lax with pointer types
			continue
		}

		// these just need an integer, size does not matter
		if bu.name == "padd" || bu.name == "psub" {
			if A.ty.kind != TYP_Int {
				PostError("type not supported by %s intrinsic: %s",
					bu.name, A.ty.String())
				return nil
			}
			continue
		}

		want_type := L_type
		if i >= 2 {
			want_type = R_type
		}

		if !TypeCompare(want_type, A.ty) {
			PostError("type mismatch between %s args: %s and %s",
				bu.name, L_type.String(), A.ty.String())
			return nil
		}

		// some intrinsics like `imul` require same signed-ness
		if L_type.kind == TYP_Int && (bu.flags & BF_MIX) == 0 {
			if L_type.unsigned != A.ty.unsigned {
				PostError("sign mismatch between %s args: %s and %s",
					bu.name, L_type.String(), A.ty.String())
				return nil
			}
		}
	}

	/* check intrinsic supports the type of arguments */

	if (bu.flags & BF_PTR) != 0 && (L_type.kind == TYP_Pointer) {
		// ok
	} else if (bu.flags & BF_SIGNED) != 0 && (L_type.kind == TYP_Int) && !L_type.unsigned {
		// ok
	} else if (bu.flags & BF_UNSIGNED) != 0 && (L_type.kind == TYP_Int) && L_type.unsigned {
		// ok
	} else if (bu.flags & BF_FLOAT) != 0 && (L_type.kind == TYP_Float) {
		// ok
	} else if (bu.flags & BF_BOOL) != 0 && (L_type.kind == TYP_Bool) {
		// ok
	} else {
		PostError("type not supported by %s intrinsic: %s",
			bu.name, L_type.String())
		return nil
	}

	if bu.rhs != 0 {
		if (bu.rhs & BF_PTR) != 0 && (R_type.kind == TYP_Pointer) {
			// ok
		} else if (bu.rhs & BF_SIGNED) != 0 && (R_type.kind == TYP_Int) && !R_type.unsigned {
			// ok
		} else if (bu.rhs & BF_UNSIGNED) != 0 && (R_type.kind == TYP_Int) && R_type.unsigned {
			// ok
		} else if (bu.rhs & BF_FLOAT) != 0 && (R_type.kind == TYP_Float) {
			// ok
		} else {
			PostError("type not supported by %s intrinsic: %s",
				bu.name, R_type.String())
			return nil
		}
	}

	t_builtin := NewNode(NC_Builtin, bu.name, children[0].pos)

	for i := range args {
		A := args[i]

		// RHS of `padd` and `psub` is an integer of any size, but back-end
		// needs the same width as a pointer, so convert it.
		if bu.name == "padd" || bu.name == "psub" {
			if i > 0 && A.ty.bits != L_type.bits {
				A = fu.ConvertOffset(A, ssize_type)
			}
		}

		t_builtin.Add(A)
	}

	/* result type of intrinsic */

	if (bu.flags & BF_COMPARISON) != 0 {
		t_builtin.ty = bool_type
	} else if bu.name == "pdiff" {
		t_builtin.ty = ssize_type
	} else {
		t_builtin.ty = L_type
	}

	return t_builtin
}

func (fu *FuncDef) ComputeConv(children []*Node) *Node {
	// source and target types must either be integer or float.
	// conversions between int and float require signed integers.

	children = children[1:]

	if len(children) == 0 {
		PostError("missing type in conv statement")
		return nil
	}

	conv_type, count := ParseTypeSpec(children, "conversion target")
	if conv_type == nil {
		return nil
	}
	children = children[count:]

	if len(children) == 0 {
		PostError("missing value in conv statement")
		return nil
	}
	if len(children) > 1 {
		PostError("extra rubbish at end of conv statement")
		return nil
	}

	// check target type makes sense
	switch conv_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("target type of conv cannot be a %s", conv_type.SimpleName())
		return nil
	}

	op_val := fu.ParseOperand(children[0], false)
	if op_val == nil {
		return nil
	}

	if op_val.IsLiteral() {
		return fu.ComputeConvLiteral(op_val, conv_type)
	}

	src_type := op_val.ty
	if src_type == nil {
		panic("operand to conv has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Bool:
		// ok
	default:
		PostError("source type of conv cannot be a %s", src_type.SimpleName())
		return nil
	}

	// check combination of source + dest types
	if (conv_type.kind == TYP_Float && src_type.kind == TYP_Int && src_type.unsigned) ||
		(src_type.kind == TYP_Float && conv_type.kind == TYP_Int && conv_type.unsigned) {
		PostError("cannot convert between float and unsigned int")
		return nil
	}
	if (conv_type.kind == TYP_Float && src_type.kind == TYP_Bool) ||
		(src_type.kind == TYP_Float && conv_type.kind == TYP_Bool) {
		PostError("cannot convert between float and boolean")
		return nil
	}

	t_conv := NewNode(NC_Convert, "", op_val.pos)
	t_conv.ty = conv_type
	t_conv.Add(op_val)

	return t_conv
}

func (fu *FuncDef) ComputeConvLiteral(lit *Node, conv_type *Type) *Node {
	switch lit.kind {
	case NL_Integer, NL_Char:
		switch conv_type.kind {
		case TYP_Int:
			if IntegerForceSize(lit, conv_type.bits) != OK {
				return nil
			}
			lit.ty = conv_type
			return lit

		case TYP_Float:
			if IntegerToFloat(lit) != OK {
				return nil
			}
			lit.ty = conv_type
			return lit

		case TYP_Bool:
			if IntegerToBool(lit) != OK {
				return nil
			}
			lit.ty = conv_type
			return lit
		}

	case NL_Bool:
		switch conv_type.kind {
		case TYP_Int:
			lit.kind = NL_Integer
			lit.ty = conv_type
			return lit
		}

	case NL_Float:
		switch conv_type.kind {
		case TYP_Float:
			lit.ty = conv_type
			if fu.CheckLiteral(lit) != OK {
				return nil
			}
			return lit

		case TYP_Int:
			if conv_type.unsigned {
				PostError("cannot convert between float and unsigned int")
				return nil
			}
			if FloatToInteger(lit) != OK {
				return nil
			}
			if IntegerForceSize(lit, conv_type.bits) != OK {
				return nil
			}
			lit.ty = conv_type
			return lit
		}

	case NL_FltSpec:
		PostError("cannot convert a special float literal")
		return nil

	case NL_String:
		PostError("cannot convert a string literal")
		return nil

	case NL_Null:
		PostError("cannot convert a pointer literal")
		return nil
	}

	panic("odd literal for conv")
}

func (fu *FuncDef) ComputeRawCast(children []*Node) *Node {
	// source and target type must be one of: int, float, ptr.
	// source and target must be same size, no exceptions.

	children = children[1:]

	if len(children) == 0 {
		PostError("missing value in raw-cast statement")
		return nil
	}

	var conv_type *Type

	// a type is optional, when absent the operand must be a pointer
	// and we will cast it into a ^raw-mem.

	if len(children) == 1 {
		head := children[0]
		if head.kind == NL_Name && ParseKnownType(head.str) != nil {
			PostError("missing value in raw-cast statement")
			return nil
		}

	} else {
		var count int
		conv_type, count = ParseTypeSpec(children, "a raw-cast")
		if conv_type == nil {
			return nil
		}
		children = children[count:]

		if len(children) == 0 {
			PostError("missing value in raw-cast statement")
			return nil
		}
		if len(children) > 1 {
			PostError("extra rubbish at end of raw-cast statement")
			return nil
		}
	}

	op_val := fu.ParseOperand(children[0], false)
	if op_val == nil {
		return nil
	}
	if op_val.IsLiteral() {
		PostError("raw-cast cannot be used with a literal value")
		return nil
	}

	if conv_type == nil {
		if op_val.ty != nil && op_val.ty.kind == TYP_Pointer {
			conv_type = NewPointerType(mem_type)
		}
	}
	if conv_type == nil {
		PostError("missing type in raw-cast statement")
		return nil
	}

	// check target type makes sense
	switch conv_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("target type of raw-cast cannot be a %s", conv_type.SimpleName())
		return nil
	}

	src_type := op_val.ty
	if src_type == nil {
		panic("operand to raw-cast has no type")
	}

	// check source type makes sense
	switch src_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok
	default:
		PostError("source type of raw-cast cannot be a %s", src_type.SimpleName())
		return nil
	}

	// check combination of source + dest types.
	C_archy := (conv_type.kind == TYP_Pointer) ||
			   (conv_type == usize_type) || (conv_type == ssize_type);

	S_archy := (src_type.kind == TYP_Pointer) ||
			   (src_type == usize_type) || (src_type == ssize_type);

	if C_archy != S_archy {
		PostError("cannot raw-cast a fixed size type and arch-dependent one")
		return nil
	}
	if conv_type.bits != src_type.bits {
		PostError("raw-cast requires types with same size")
		return nil
	}

	t_cast := NewNode(NC_RawCast, "", op_val.pos)
	t_cast.ty = conv_type
	t_cast.Add(op_val)

	return t_cast
}

func (fu *FuncDef) ComputeFunAddr(children []*Node) *Node {
	if len(children) < 2 || children[1].kind != NL_Name {
		PostError("missing function name")
		return nil
	}
	if len(children) > 2 {
		PostError("extra rubbish after function name")
		return nil
	}

	t := children[1]

	name := t.str
	def  := all_defs[name]

	if def == nil {
		PostError("no such function: %s", name)
		return nil
	}
	if def.kind != DEF_Func {
		PostError("expected a function name, got: %s", name)
		return nil
	}
	if def.d_func.inline {
		PostError("cannot take the address of an inline function")
		return nil
	}

	t_glob := NewNode(NP_GlobPtr, name, t.pos)
	t_glob.ty = def.d_func.ptr

	return t_glob
}

func (fu *FuncDef) ComputeMemRead(children []*Node) *Node {
	// note that NP_Drop is allowed for the destination

	if len(children) < 2 || children[1].kind != NG_Access {
		PostError("missing access in [] for mem-read statement")
		return nil
	}
	if len(children) > 2 {
		PostError("extra rubbish at end of mem-read statement")
		return nil
	}

	op := fu.OperandAccess(children[1], false)
	if op == nil {
		return nil
	}

	t_read := NewNode(NC_MemRead, "", children[0].pos)
	t_read.ty = op.ty
	t_read.Add(op)

	return t_read
}

func (fu *FuncDef) ComputeStackVar(children []*Node) *Node {
	if len(children) < 2 {
		PostError("missing type in stack-var statement")
		return nil
	}

	ty, count := ParseTypeSpec(children[1:], "a stack var")
	if ty == nil {
		return nil
	}

	if len(children) > count+1 {
		PostError("extra rubbish at end of stack-var statement")
		return nil
	}

	t_stackvar := NewNode(NC_StackVar, "", children[0].pos)
	t_stackvar.ty = NewPointerType(ty)

	return t_stackvar
}

func (fu *FuncDef) ComputeSelect(children []*Node, dest_type *Type) *Node {
	if len(children) < 2 {
		PostError("missing condition in sel-if statement")
		return nil
	}
	if len(children) < 4 {
		PostError("missing values in sel-if statement")
		return nil
	}
	if len(children) > 4 {
		PostError("extra rubbish at end of sel-if statement")
		return nil
	}

	t_cond := children[1]
	t_val1 := children[2]
	t_val2 := children[3]

	cond := fu.ParseSimpleOperand(t_cond, false)
	if cond == nil {
		return nil
	}
	if cond.kind != NP_Local {
		PostError("condition for sel-if must be a local, got: %s", t_cond.String())
		return nil
	}
	if cond.ty.kind != TYP_Bool {
		PostError("condition for sel-if must have boolean type")
		return nil
	}

	v_true  := fu.ParseOperand(t_val1, false)
	v_false := fu.ParseOperand(t_val2, false)
	if v_true == nil || v_false == nil {
		return nil
	}

	// determine and check types
	ty := dest_type
	if ty == nil {
		ty = v_true.ty
	}
	if ty == nil {
		ty = v_false.ty
	}
	if ty == nil {
		PostError("unknown type for sel-if (untyped literal)")
		return nil
	}

	if v_true.ty == nil {
		v_true.ty = ty
		if fu.CheckLiteral(v_true) != OK {
			return nil
		}
	}
	if v_false.ty == nil {
		v_false.ty = ty
		if fu.CheckLiteral(v_false) != OK {
			return nil
		}
	}

	if !TypeCompare(ty, v_true.ty) {
		PostError("type mismatch in sel-if: wanted %s, got %s",
			ty.String(), v_true.ty.String())
		return nil
	}
	if !TypeCompare(ty, v_false.ty) {
		PostError("type mismatch in sel-if: wanted %s, got %s",
			ty.String(), v_false.ty.String())
		return nil
	}

	t_select := NewNode(NC_Select, "", t_cond.pos)
	t_select.ty = ty
	t_select.Add(cond)
	t_select.Add(v_true)
	t_select.Add(v_false)

	return t_select
}

func (fu *FuncDef) CheckLiteral(t *Node) error {
	ty := t.ty

	if !t.IsLiteral() {
		panic("CheckLiteral with non literal")
	}
	if ty == nil {
		panic("CheckLiteral with no type")
	}

	if ty.kind == TYP_Int && t.kind == NL_Integer {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Int && t.kind == NL_Char {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && t.kind == NL_Float {
		return CheckNumericValue(t, ty)
	}
	if ty.kind == TYP_Float && t.kind == NL_FltSpec {
		return OK
	}
	if ty.kind == TYP_Bool && t.kind == NL_Bool {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_Null {
		return OK
	}
	if ty.kind == TYP_Pointer && t.kind == NL_String {
		if ty.sub.kind == TYP_Int && ty.sub.unsigned && ty.sub.bits < 64 {
			return OK
		}
		PostError("cannot use string in that context (type mismatch)")
		return FAILED
	}

	PostError("literal has wrong type, wanted a %s", ty.SimpleName())
	return FAILED
}

//----------------------------------------------------------------------

func (fu *FuncDef) ParseSimpleOperand(t *Node, allow_func bool) *Node {
	res := fu.ParseOperand(t, allow_func)
	if res != nil {
		res = fu.ShiftAccess(res)
	}
	return res
}

func (fu *FuncDef) ParseOperand(t *Node, allow_func bool) *Node {
	if t.IsLiteral() {
		// note that this return node has no type (ty = NIL).
		// everything else will get a type.
		return t
	}
	if t.kind == NG_Access {
		return fu.OperandAccess(t, true)
	}
	if t.kind == NL_Name {
		return fu.OperandName(t, allow_func)
	}

	PostError("expected operand, got: %s", t.String())
	return nil
}

func (fu *FuncDef) OperandName(t *Node, allow_func bool) *Node {
	name := t.str

	local := fu.locals[name]
	if local != nil {
		op := NewNode(NP_Local, name, t.pos)
		op.ty = local.ty
		return op
	}

	def := all_defs[name]
	if def != nil {
		switch def.kind {
		case DEF_Const:
			// note that the result node will have no type
			return def.d_const.MakeLiteral(t.pos)

		case DEF_Var:
			return fu.OperandVar(t, def.d_var)

		case DEF_Func:
			if !allow_func {
				PostError("invalid use of function name: %s", name)
				return nil
			}
			return fu.OperandFunc(t, def.d_func)

		case DEF_Type:
			PostError("cannot use type '%s' in that context", t.str)
			return nil
		}
	}

	if builtins[t.str] != nil {
		PostError("cannot use '%s' intrinsic in that context", t.str)
		return nil
	}

	if fu.error_num > 0 && fu.seen_locals[t.str] {
		// don't show errors for a local that was known to be created
	} else {
		PostError("unknown identifier: %s", t.str)
	}
	return nil
}

func (fu *FuncDef) OperandVar(t *Node, vdef *VarDef) *Node {
	t_glob := NewNode(NP_GlobPtr, vdef.name, t.pos)
	t_glob.ty = vdef.ptr
	return t_glob
}

func (fu *FuncDef) OperandFunc(t *Node, fdef *FuncDef) *Node {
	t_func := NewNode(NP_FuncRef, fdef.name, t.pos)
	t_func.ty = fdef.ty
	return t_func
}

func (fu *FuncDef) OperandAccess(t *Node, allow_addr bool) *Node {
	// this takes an access in `[]` and produces a NP_Memory node,
	// with extra child nodes to compute intermediate addresses
	// (and one or more temporary vars).
	//
	// for the `[addr-of ...]` form it produces a NP_Local with a
	// temporary var computing the final address.

	children := t.children

	addr_of := false
	if len(children) > 0 && children[0].Match("addr-of") {
		if !allow_addr {
			PostError("addr-of cannot be used in that context")
			return nil
		}
		addr_of = true
		children = children[1:]
	}

	if len(children) == 0 {
		PostError("missing elements in []")
		return nil
	}

	base := fu.ParseSimpleOperand(children[0], false)
	children = children[1:]

	if base == nil {
		return nil
	}
	if base.IsLiteral() {
		PostError("cannot access a literal value")
		return nil
	}
	if base.ty == nil {
		panic("operand in [] without a type")
	}
	if base.ty.kind != TYP_Pointer {
		PostError("expected pointer in [], got a %s", base.ty.SimpleName())
		return nil
	}
	if base.ty.sub == mem_type {
		PostError("cannot access a raw-mem pointer")
		return nil
	}
	if base.ty.sub.kind == TYP_Extern {
		PostError("cannot access an external type in []")
		return nil
	}

	// simple case 1/2: a single element

	if len(children) == 0 {
		switch base.ty.sub.kind {
		case TYP_Array:
			PostError("missing array index in []")
			return nil

		case TYP_Struct, TYP_Union:
			PostError("missing field name in []")
			return nil
		}

		if addr_of {
			PostError("addr-of requires an indexed array or struct")
			return nil
		}

		t_mem := NewNode(NP_Memory, "", t.pos)
		t_mem.ty = base.ty.sub
		t_mem.offset = 0
		t_mem.Add(base)
		return t_mem
	}

	// simple case 2/2: a struct pointer + a field name

	if len(children) == 1 && !addr_of {
		switch base.ty.sub.kind {
		case TYP_Struct, TYP_Union:
			t_elem := children[0]

			if !t_elem.IsField() {
				PostError("expected struct/union field in [], got: %s", t_elem.String())
				return nil
			}

			struct_type := base.ty.sub
			field_idx   := struct_type.FindParam(t_elem.str)

			if field_idx < 0 {
				PostError("no such field %s in type %s", t_elem.str, struct_type.String())
				return nil
			}

			field := struct_type.param[field_idx]

			return fu.MemoryAccess(base, field.ty, field.offset)
		}
	}

	// complex cases...

	// after each element, `base` will be a temporary var (NP_Local)
	// whose type is always a pointer to the element indexed.
	elem_type := base.ty.sub

	for _, t_elem := range children {
		if base.ty.kind != TYP_Pointer {
			PostError("cannot deref a %s in []", base.ty.SimpleName())
			return nil
		}

		switch elem_type.kind {
		case TYP_Array:
			// essentially: base += index * type_size

			arr_type := elem_type
			elem_type = arr_type.sub

			if t_elem.IsField() {
				PostError("expected array index in [], got: %s", t_elem.str)
				return nil
			}

			op_index := fu.ParseSimpleOperand(t_elem, false)
			if op_index == nil {
				return nil
			}

			switch op_index.kind {
			case NL_Integer, NL_Char:
				// ok, give it a type
				op_index.ty = usize_type

				if op_index.str[0] == '-' {
					op_index.ty = ssize_type
				}

				if fu.CheckLiteral(op_index) != OK {
					return nil
				}

			case NP_Local:
				if op_index.ty.kind != TYP_Int {
					PostError("array index must be integer, got a %s",
						op_index.ty.SimpleName())
					return nil
				}

			default:
				PostError("expected array index in [], got: %s", t_elem.String())
				return nil
			}

			base = fu.IndexArray(base, op_index, elem_type)
			if base == nil {
				return nil
			}

		case TYP_Struct, TYP_Union:
			// essentially: base += offset

			if !t_elem.IsField() {
				PostError("expected struct/union field in [], got: %s", t_elem.String())
				return nil
			}

			struct_type := elem_type
			field_idx   := struct_type.FindParam(t_elem.str)

			if field_idx < 0 {
				PostError("no such field %s in type %s", t_elem.str, struct_type.String())
				return nil
			}

			field := struct_type.param[field_idx]

			if field.offset > 0x7FFFFFFF {
				PostError("offset for field %s is too large", t_elem.str)
				return nil
			}

			// use BaseOffset even for zero offsets (to get correct type)
			int_str  := strconv.Itoa(field.offset)

			t_offset := NewNode(NL_Integer, int_str, t.pos)
			t_offset.ty = usize_type

			base = fu.BaseOffset(base, t_offset, field.ty)

			elem_type = field.ty

		case TYP_Pointer:
			PostError("cannot deref a new pointer in []")
			return nil

		default:
			if t_elem.IsField() {
				PostError("expected struct type in [], got: %s", base.ty.String())
			} else {
				PostError("expected array type in [], got: %s", base.ty.String())
			}
			return nil
		}
	}

	if addr_of {
		return base
	}

	return fu.MemoryAccess(base, elem_type, 0)
}

func (fu *FuncDef) MemoryAccess(base *Node, mem_type *Type, offset int) *Node {
	switch mem_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		// ok

	case TYP_Array:
		PostError("missing array index in []")
		return nil

	case TYP_Struct, TYP_Union:
		PostError("missing field name in []")
		return nil

	default:
		PostError("cannot use [] to access a %s", mem_type.SimpleName())
		return nil
	}

	t_mem := NewNode(NP_Memory, "", base.pos)
	t_mem.ty = mem_type
	t_mem.offset = offset
	t_mem.Add(base)

	return t_mem
}

func (fu *FuncDef) IndexArray(base, t_index *Node, elem_type *Type) *Node {
	if elem_type.size < 0 {
		panic("unset type size")
	}

	// for a literal value, precompute the multiply

	switch t_index.kind {
	case NL_Integer, NL_Char:
		offset, err := strconv.ParseInt(t_index.str, 0, 64)
		if err != nil || offset < 0 || offset > 0x7FFFFFFF {
			PostError("illegal index value: %s", t_index.str)
			return nil
		}

		if offset == 0 {
			ptr_type := NewPointerType(elem_type)
			base.ty = ptr_type
			return base
		}

		offset = offset * int64(elem_type.size)

		int_str  := strconv.FormatInt(offset, 10)
		t_offset := NewNode(NL_Integer, int_str, t_index.pos)
		t_offset.ty = usize_type

		return fu.BaseOffset(base, t_offset, elem_type)
	}

	ofs_type := ssize_type
	if t_index.ty.unsigned {
		ofs_type = usize_type
	}

	// check type of index is suitable, if not convert it
	if t_index.ty.bits != ofs_type.bits {
		t_index = fu.ConvertOffset(t_index, ofs_type)
	}

	// create temporary local to hold (scale * index)
	temp  := fu.NewTemporary("__offset")
	local := fu.NewLocal(temp, ofs_type)

	var_dest := NewNode(NP_Local, temp, base.pos)
	var_src  := NewNode(NP_Local, temp, base.pos)
	var_dest.ty = local.ty
	var_src.ty  = local.ty

	scale_str := strconv.Itoa(elem_type.size)
	t_scale := NewNode(NL_Integer, scale_str, base.pos)
	t_scale.ty = ofs_type

	t_mul := NewNode(NC_Builtin, "imul", base.pos)
	t_mul.ty = ofs_type
	t_mul.Add(t_index)
	t_mul.Add(t_scale)

	t_move := NewNode(NH_Move, "", base.pos)
	t_move.Add(var_dest)
	t_move.Add(t_mul)

	fu.body.Add(t_move)

	return fu.BaseOffset(base, var_src, elem_type)
}

func (fu *FuncDef) ConvertOffset(t_offset *Node, ofs_type *Type) *Node {
	temp  := fu.NewTemporary("__conv")
	local := fu.NewLocal(temp, ofs_type)

	conv_dest := NewNode(NP_Local, temp, t_offset.pos)
	conv_src  := NewNode(NP_Local, temp, t_offset.pos)
	conv_dest.ty = local.ty
	conv_src.ty  = local.ty

	t_conv := NewNode(NC_Convert, "", t_offset.pos)
	t_conv.ty = ofs_type
	t_conv.Add(t_offset)

	t_move := NewNode(NH_Move, "", t_offset.pos)
	t_move.Add(conv_dest)
	t_move.Add(t_conv)

	fu.body.Add(t_move)

	return conv_src
}

func (fu *FuncDef) BaseOffset(base, t_offset *Node, target_type *Type) *Node {
	// produce code to compute BASE + OFFSET.
	// Note that type of t_offset must be usize or ssize.

	ptr_type := NewPointerType(target_type)

	t_add := NewNode(NC_Builtin, "padd", base.pos)
	t_add.ty = ptr_type

	temp  := fu.NewTemporary("__ptr")
	local := fu.NewLocal(temp, ptr_type)
	_ = local

	var_dest := NewNode(NP_Local, temp, base.pos)
	var_src  := NewNode(NP_Local, temp, base.pos)
	var_dest.ty = ptr_type
	var_src.ty  = ptr_type

	t_add.Add(base)
	t_add.Add(t_offset)

	t_move := NewNode(NH_Move, "", base.pos)
	t_move.Add(var_dest)
	t_move.Add(t_add)

	fu.body.Add(t_move)

	return var_src
}

//----------------------------------------------------------------------

func (fu *FuncDef) NewTemporary(prefix string) string {
	fu.num_temps += 1
	return prefix + strconv.Itoa(fu.num_temps)
}

func (fu *FuncDef) NewLocal(name string, ty *Type) *LocalInfo {
	local := new(LocalInfo)
	local.name = name
	local.ty   = ty
	local.param_idx = -1

	fu.locals[local.name] = local
	return local
}

func (fu *FuncDef) EmitLabel(name string, pos Position) {
	t_label := NewNode(NH_Label, name, pos)
	fu.body.Add(t_label)
}

func (fu *FuncDef) GetCalledFunc(t *Node) *FuncDef {
	// input node must be a NH_TailCall or NC_Call, or a high level
	// node which has a NC_Call as its computation.  result is NIL
	// when the function is not a global (or a method).

	comp := t.GetComputation()
	if comp != nil {
		t = comp
	}

	switch t.kind {
	case NC_Call, NH_TailCall:
		// ok
	default:
		return nil
	}

	t_func := t.children[0]

	switch t_func.kind {
	case NP_FuncRef:
		def := all_defs[t_func.str]
		return def.d_func

	case NP_Method:
		belong_type := t_func.ty.param[0].ty.sub
		return belong_type.FindMethod(t_func.str)
	}

	return nil
}
