// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strings"
import "os"
import "os/exec"

func RunRazmCompiler() error {
	var err error

	tool := "./razm"
	cmd  := exec.Command(tool)

	for _, srcfile := range Options.sources {
		cmd.Args = append(cmd.Args, srcfile)
	}

	if Options.no_optim {
		cmd.Args = append(cmd.Args, "-N")
	}

	cmd.Args = append(cmd.Args, "-t")
	cmd.Args = append(cmd.Args, Options.target)
	cmd.Args = append(cmd.Args, "-o")
	cmd.Args = append(cmd.Args, Work.s_file)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the razm compiler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box
	cmd.Stdout = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	err_string := TidyCapturedOutput(err_box.String())

	if err != nil {
		// fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_string)

		return fmt.Errorf("the razm code failed to compile")
	}

	// show any warnings or debugging messages
	if len(err_string) > 0 {
		fmt.Fprintf(os.Stderr, "%s\n", err_string)
	}

	return Ok
}

//----------------------------------------------------------------------

func RunAssembler() error {
	var err error

	format := "xxx"
	switch Options.target {
	case "linux":   format = "elf64"
	case "windows": format = "win64"
	}

	tool := "nasm"
	cmd  := exec.Command(tool, "-f", format, Work.s_file, "-o", Work.o_file)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the assembler
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	err_string := TidyCapturedOutput(err_box.String())

	if err != nil {
		// fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_string)

		return fmt.Errorf("the generated ASM code failed to assemble")
	}

	// show any warnings
	if len(err_string) > 0 {
		fmt.Fprintf(os.Stderr, "%s\n", err_string)
	}

	return Ok
}

//----------------------------------------------------------------------

func RunLinker() error {
	var err error

	tool := "ld"
	cmd  := exec.Command(tool, Work.o_file, "-o", Options.exe_name)

	// drop normal output on stdout, prevent clobbering our messages
	cmd.Stdout = nil

	// collect any error messages from the linker
	err_box := new(strings.Builder)
	cmd.Stderr = err_box

	err = cmd.Start()
	if err != nil {
		return fmt.Errorf("could not run %s: %s", tool, err.Error())
	}

	err = cmd.Wait()
	err_string := TidyCapturedOutput(err_box.String())

	if err != nil {
		// fmt.Fprintf(os.Stderr, "ERROR FROM %s: %s\n", tool, err.Error())
		fmt.Fprintf(os.Stderr, "%s\n", err_string)

		return fmt.Errorf("the compiled .o files failed to link")
	}

	// show any warnings
	if len(err_string) > 0 {
		fmt.Fprintf(os.Stderr, "%s\n", err_string)
	}

	return Ok
}
