// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

const tiny_runtime_unix =
`
;;
;; basic start code
;;

%define sys_read	0
%define sys_write	1
%define sys_exit	60

section .text

global _start
global tinyrt?_exit

_start:
	xor	rbp,rbp
	call	main
	mov	edi,0

tinyrt?_exit:
	; this will not return
	mov	eax,sys_exit
	syscall
	hlt

;;
;; basic input/output functions
;;

%define STDIN		0
%define STDOUT		1
%define STDERR		2

global tinyrt?_in?_byte
global tinyrt?_out?_byte
global tinyrt?_out?_hex
global tinyrt?_out?_hex32
global tinyrt?_out?_hex16
global tinyrt?_out?_hex8
global tinyrt?_out?_str

tinyrt?_in?_byte:
	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16

	mov	edi,STDIN     ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_read
	syscall

	test	rax,rax
	jz	.eof
	js	.error

	xor	rax,rax
	mov	al,[rbp-4]

	leave
	ret
.eof:
.error:
	mov	rax,dword -1

	leave
	ret


tinyrt?_out?_byte:
	push	rbp
	mov	rbp,rsp

	; use the stack as our buffer
	sub	rsp,16
	mov	[rbp-4],dil

	mov	edi,STDOUT    ; fd
	lea	rsi,[rbp-4]   ; buf
	mov	edx,1         ; count

	mov	eax,sys_write
	syscall

	leave
	ret


tinyrt?_out?_str:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],r15
	mov	r15,rdi
.loop:
	mov	dil,[r15]
	or	dil,dil
	jz	.done

	call	tinyrt?_out?_byte

	inc	r15
	jmp	.loop
.done:
	mov	r15,[rbp-8]

	leave
	ret


tinyrt?_out?_hex:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,32
	call	tinyrt?_out?_hex32

	mov	rdi,[rbp-8]
	call	tinyrt?_out?_hex32

	leave
	ret

tinyrt?_out?_hex32:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,16
	call	tinyrt?_out?_hex16

	mov	rdi,[rbp-8]
	call	tinyrt?_out?_hex16

	leave
	ret

tinyrt?_out?_hex16:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,8
	call	tinyrt?_out?_hex8

	mov	rdi,[rbp-8]
	call	tinyrt?_out?_hex8

	leave
	ret

tinyrt?_out?_hex8:
	push	rbp
	mov	rbp,rsp
	sub	rsp,2*8

	mov	[rbp-8],rdi
	shr	rdi,4
	call	tinyrt?_out?_hex4

	mov	rdi,[rbp-8]
	call	tinyrt?_out?_hex4

	leave
	ret

tinyrt?_out?_hex4:
	push	rbp
	mov	rbp,rsp

	and	dil,15
	add	dil,48  ; '0'
	cmp	dil,58
	jb	.over

	add	dil,65-58  ; 'A'
.over:
	call	tinyrt?_out?_byte

	leave
	ret

;;
;; rest of program
;;

`;
