// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "io"
import "fmt"
import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	sources   []string
	target    string
	build_dir string
	exe_name  string
	no_optim  bool
	help      bool
}

var Work struct {
	s_file string
	o_file string
}

func main() {
	var err error

	Options.sources = make([]string, 0)
	Options.build_dir = "_temp"

	// default target matches the host OS
	if runtime.GOOS == "windows" {
		Options.target = "windows"
	} else {
		Options.target = "linux"
	}

	argv.Generic("o", "output", &Options.exe_name, "file", "name of output executable")
	argv.Generic("d", "dir", &Options.build_dir, "dir", "name of build dir (_temp)")
	argv.Generic("t", "target", &Options.target, "OS", "target operating system (windows|linux)")
	argv.Enabler("N", "no-optim", &Options.no_optim, "disable all optimizations")
	argv.Enabler("h", "help", &Options.help, "display this help text")

	err = argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	names := argv.Unparsed()

	if Options.help || len(names) == 0 {
		ShowUsage()
		os.Exit(0)
	}

	switch Options.target {
	case "windows", "linux":
		// ok
	default:
		FatalError("unknown target: %s", Options.target)
	}

	Options.sources = names

	// default executable name if unset
	if Options.exe_name == "" {
		Options.exe_name = FileRemoveExtension(Options.sources[0])
		if Options.target == "windows" {
			Options.exe_name += ".exe"
		}
	}

	err = BuildStuff()
	if err != nil {
		FatalError("%s", err.Error())
	}

	os.Exit(0)
}

func BuildStuff() error {
	var err error

	bare_exe := FileRemoveExtension(Options.exe_name)
	bare_exe = filepath.Base(bare_exe)

	Work.s_file = filepath.Join(Options.build_dir, bare_exe + ".s")
	Work.o_file = filepath.Join(Options.build_dir, bare_exe + ".o")

	err = DirCreate(Options.build_dir)
	if err != nil {
		return err
	}

	// step 1: run the razm compiler
	err = RunRazmCompiler()
	if err != nil {
		return err
	}

	// step 2: prepend our minimal runtime code
	err = PrependRuntime()
	if err != nil {
		return err
	}

	// step 3: run the assembler (NASM)
	err = RunAssembler()
	if err != nil {
		return err
	}

	// step 4: link the final program
	err = RunLinker()
	if err != nil {
		return err
	}

	fmt.Printf("successfully compiled %s\n", Options.exe_name)
	return Ok
}

func ShowUsage() {
	fmt.Printf("Usage: rz-build FILE... [OPTIONS...]\n")
	fmt.Printf("\n")
	fmt.Printf("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	format = "rz-build: " + format + "\n"
	fmt.Fprintf(os.Stderr, format, a...)
	os.Exit(1)
}

func PrependRuntime() error {
	src_file := Work.s_file

	Work.s_file = filepath.Join(Options.build_dir, "_combined.s")

	in_f, err := os.Open(src_file)
	if err != nil {
		return err
	}

	out_f, err := os.Create(Work.s_file)
	if err != nil {
		in_f.Close()
		return err
	}

	if Options.target == "windows" {
		_, err = io.WriteString(out_f, tiny_runtime_win)
	} else {
		_, err = io.WriteString(out_f, tiny_runtime_unix)
	}
	if err != nil {
		in_f.Close()
		return err
	}

	_, err = io.Copy(out_f, in_f)

	in_f.Close()
	out_f.Close()

	if err != nil {
		return err
	}
	return Ok
}
