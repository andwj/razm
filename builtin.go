// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// import "fmt"

type BuiltinFlags int

const (
	BF_PTR   BuiltinFlags = (1 << iota)

	BF_SIGNED    // signed integers
	BF_UNSIGNED  // unsigned integers
	BF_FLOAT     // floating point
	BF_BOOL      // booleans

	BF_MULTI     // allow additional arguments (at least two)
	BF_MIX       // allow arguments to have different sign
	BF_SIDE_FX   // builtin has a side-effect, allow no destination

	BF_COMPARISON  // result type is bool
)

type Builtin struct {
	name  string
	args  int
	flags BuiltinFlags
	rhs   BuiltinFlags  // 0 means all args are same type
}

var builtins map[string]*Builtin

func InitBuiltins() {
	builtins = make(map[string]*Builtin)

	// integer arithmetic

	RegisterBuiltin("ineg", 1, BF_SIGNED, 0)
	RegisterBuiltin("iabs", 1, BF_SIGNED, 0)

	RegisterBuiltin("iadd", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX, 0)
	RegisterBuiltin("isub", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX, 0)
	RegisterBuiltin("imul", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI, 0)

	RegisterBuiltin("idivt", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI, 0)
	RegisterBuiltin("iremt", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI, 0)

	RegisterBuiltin("imax", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI, 0)
	RegisterBuiltin("imin", 2, BF_SIGNED|BF_UNSIGNED|BF_MULTI, 0)

	// bit-wise operations

	RegisterBuiltin("iflip", 1, BF_SIGNED|BF_UNSIGNED, 0)
	RegisterBuiltin("iand",  2, BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX, 0)
	RegisterBuiltin("ior",   2, BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX, 0)
	RegisterBuiltin("ixor",  2, BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX, 0)

	RegisterBuiltin("ishl",  2, BF_SIGNED|BF_UNSIGNED, BF_SIGNED|BF_UNSIGNED)
	RegisterBuiltin("ishr",  2, BF_SIGNED|BF_UNSIGNED, BF_SIGNED|BF_UNSIGNED)

	// boolean

	RegisterBuiltin("not",   1, BF_BOOL, 0)
	RegisterBuiltin("and",   2, BF_BOOL|BF_MULTI, 0)
	RegisterBuiltin("or",    2, BF_BOOL|BF_MULTI, 0)

	// pointer arithmetic

	RegisterBuiltin("padd",  2, BF_PTR|BF_MULTI|BF_MIX, BF_SIGNED|BF_UNSIGNED)
	RegisterBuiltin("psub",  2, BF_PTR|BF_MULTI|BF_MIX, BF_SIGNED|BF_UNSIGNED)
	RegisterBuiltin("pdiff", 2, BF_PTR, 0)

	// floating point arithmetic

	RegisterBuiltin("fneg",  1, BF_FLOAT, 0)
	RegisterBuiltin("fabs",  1, BF_FLOAT, 0)
	RegisterBuiltin("finv",  1, BF_FLOAT, 0)
	RegisterBuiltin("fsqrt", 1, BF_FLOAT, 0)

	RegisterBuiltin("fadd",  2, BF_FLOAT|BF_MULTI, 0)
	RegisterBuiltin("fsub",  2, BF_FLOAT|BF_MULTI, 0)
	RegisterBuiltin("fmul",  2, BF_FLOAT|BF_MULTI, 0)
	RegisterBuiltin("fdiv",  2, BF_FLOAT|BF_MULTI, 0)
	RegisterBuiltin("fremt", 2, BF_FLOAT|BF_MULTI, 0)

	RegisterBuiltin("fmax",  2, BF_FLOAT|BF_MULTI, 0)
	RegisterBuiltin("fmin",  2, BF_FLOAT|BF_MULTI, 0)

	RegisterBuiltin("fround", 1, BF_FLOAT, 0)
	RegisterBuiltin("ftrunc", 1, BF_FLOAT, 0)
	RegisterBuiltin("ffloor", 1, BF_FLOAT, 0)
	RegisterBuiltin("fceil",  1, BF_FLOAT, 0)

	// comparisons

	RegisterBuiltin("eq?", 2, BF_BOOL|BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX|BF_COMPARISON, 0)
	RegisterBuiltin("ne?", 2, BF_BOOL|BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_MULTI|BF_MIX|BF_COMPARISON, 0)

	RegisterBuiltin("lt?", 2, BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("le?", 2, BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("gt?", 2, BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("ge?", 2, BF_PTR|BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)

	RegisterBuiltin("zero?", 1, BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("some?", 1, BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("pos?",  1, BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)
	RegisterBuiltin("neg?",  1, BF_FLOAT|BF_SIGNED|BF_UNSIGNED|BF_COMPARISON, 0)

	RegisterBuiltin("inf?",  1, BF_FLOAT|BF_COMPARISON, 0)
	RegisterBuiltin("nan?",  1, BF_FLOAT|BF_COMPARISON, 0)

	RegisterBuiltin("null?", 1, BF_PTR|BF_COMPARISON, 0)
	RegisterBuiltin("ref?",  1, BF_PTR|BF_COMPARISON, 0)

	// endianness

	RegisterBuiltin("little-endian", 1, BF_SIGNED|BF_UNSIGNED, 0)
	RegisterBuiltin("big-endian",    1, BF_SIGNED|BF_UNSIGNED, 0)
}

func RegisterBuiltin(name string, args int, flags, rhs BuiltinFlags) {
	bu := new(Builtin)
	bu.name  = name
	bu.args  = args
	bu.flags = flags
	bu.rhs   = rhs

	builtins[name] = bu
}
