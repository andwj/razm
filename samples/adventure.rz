;;
;; A simple adventure game
;;
;; by Andrew Apted, 2018-2021.
;;
;; this code is licensed as CC0 (i.e. public domain)
;;

#public

fun main ()
	welcome-message
	describe-room [player-loc]
	println ""
	loop until [game-over]
		read-and-process-command
	endloop
	quit-message
end

;;----------------------------------------------------------------------

#private

type uchar u8

; locations
type Loc u32

const LOC_Mountain = 1
const LOC_Forest   = 2
const LOC_Lake     = 3
const LOC_Outside  = 4
const LOC_Castle   = 5
const LOC_Treasury = 6

; directions
type Dir u32

const DIR_N = 1
const DIR_S = 2
const DIR_E = 3
const DIR_W = 4

type Room struct
	.name    ^uchar
	.objects [20]Object
	.exits   [20]Exit
	.desc    [20]^uchar
end

type Exit struct
	.dir  Dir
	.dest Loc
	.obstacle Object
end

type Object u32

const OBJ_Sword    = 1
const OBJ_Key      = 2
const OBJ_Steak    = 3
const OBJ_Carrot   = 4
const OBJ_Treasure = 5

const OBJ_Crocodile = 6
const OBJ_Parrot    = 7
const OBJ_Guard     = 8
const OBJ_Door      = 9

type ValueWord struct
	.value s32   ; -1 terminates a list
	.name  ^uchar
end

type CmdFunc fun ()

type Command struct
	.func ^CmdFunc
	.name ^uchar
end

;;----------------------------------------------------------------------

const PASSWORD = "pinecone"

var game-over bool = FALSE

; --- player information ---

var player-loc Loc = LOC_Mountain

var player-inventory [20]Object = { OBJ_Sword ... }

var player-found-key bool = FALSE

; --- room definitions ---

var ROOMS [7]Room =
	; the [0] element is just a place-holder
	{ NULL {...} {...} {...} }

	; LOC_Mountain
	struct
		.name "Mountain"
		.objects {...}
		.exits { {DIR_N LOC_Forest 0} ... }
		.desc array
			"You are standing on a large grassy mountain."
			"To the north you see a thick forest."
			"Other directions are blocked by steep cliffs."
			...
		endarray
	endstruct

	; LOC_Forest
	struct
		.name "Forest"
		.objects { OBJ_Crocodile OBJ_Parrot ... }
		.exits array
			{ DIR_S LOC_Mountain 0 }
			{ DIR_W LOC_Lake 0 }
			{ DIR_E LOC_Outside OBJ_Crocodile }
			...
		endarray
		.desc array
			"You are in a forest, surrounded by dense trees and shrubs."
			"A wide path slopes gently upwards to the south, and"
			"narrow paths lead east and west."
			...
		endarray
	endstruct

	; LOC_Lake
	struct
		.name "Lake"
		.objects { OBJ_Steak ... }
		.exits { {DIR_E LOC_Forest 0} ... }
		.desc array
			"You stand on the shore of a beautiful lake, soft sand under"
			"your feet.  The clear water looks warm and inviting."
			...
		endarray
	endstruct

	; LOC_Outside
	struct
		.name "Outside"
		.objects {...}
		.exits array
			{ DIR_W LOC_Forest 0 }
			{ DIR_E LOC_Castle OBJ_Door }
			...
		endarray
		.desc array
			"The forest is thinning off here.  To the east you can see a"
			"large castle made of dark brown stone.  A narrow path leads"
			"back into the forest to the west."
			...
		endarray
	endstruct

	; LOC_Castle
	struct
		.name "Castle"
		.objects { OBJ_Guard OBJ_Carrot ... }
		.exits array
			{ DIR_W LOC_Outside  0 }
			{ DIR_S LOC_Treasury OBJ_Guard }
			...
		endarray
		.desc array
			"You are standing inside a magnificant, opulent castle."
			"A staircase leads to the upper levels, but unfortunately"
			"it is currently blocked off by delivery crates.  A large"
			"wooden door leads outside to the west, and a small door"
			"leads south."
			...
		endarray
	endstruct

	; LOC_Treasury
	struct
		.name "Treasury"
		.objects { OBJ_Treasure ... }
		.exits { {DIR_N LOC_Castle 0} ... }
		.desc array
			"Wow!  This room is full of valuable treasures.  Gold, jewels,"
			"valuable antiques sit on sturdy shelves against the walls."
			"However...... perhaps money isn't everything??"
			...
		endarray
	endstruct
end

; --- various tables ---

rom-var OBJECTS [11]ValueWord =
	{ 0 NULL }  ; unused

	{ OBJ_Sword     "sword"     }
	{ OBJ_Key       "key"       }
	{ OBJ_Steak     "steak"     }
	{ OBJ_Carrot    "carrot"    }
	{ OBJ_Treasure  "treasure"  }

	{ OBJ_Crocodile "crocodile" }
	{ OBJ_Parrot    "parrot"    }
	{ OBJ_Guard     "guard"     }
	{ OBJ_Door      "door"      }

	{ -1 NULL }  ; terminator
end

rom-var DIRS [10]ValueWord =
	{ 0 NULL }  ; unused

	{ DIR_N "north" }
	{ DIR_S "south" }
	{ DIR_E "east"  }
	{ DIR_W "west"  }

	{ DIR_N "n" }
	{ DIR_S "s" }
	{ DIR_E "e" }
	{ DIR_W "w" }

	{ -1 NULL }  ; terminator
end

rom-var COMMANDS [31]Command =
	{ cmd-quit   "quit"    }
	{ cmd-quit   "q"       }
	{ cmd-quit   "exit"    }
	{ cmd-invent "invent"  }
	{ cmd-invent "inventory" }
	{ cmd-invent "inv"     }
	{ cmd-invent "i"       }
	{ cmd-help   "help"    }
	{ cmd-look   "look"    }
	{ cmd-look   "l"       }
	{ cmd-go     "go"      }
	{ cmd-swim   "swim"    }
	{ cmd-swim   "dive"    }
	{ cmd-drop   "drop"    }
	{ cmd-get    "get"     }
	{ cmd-get    "take"    }
	{ cmd-give   "give"    }
	{ cmd-give   "offer"   }
	{ cmd-feed   "feed"    }
	{ cmd-open   "open"    }
	{ cmd-open   "unlock"  }
	{ cmd-attack "attack"  }
	{ cmd-attack "fight"   }
	{ cmd-attack "kill"    }
	{ cmd-attack "hit"     }
	{ cmd-use    "use"     }
	{ cmd-use    "apply"   }
	{ cmd-say    "say"     }
	{ cmd-say    "speak"   }
	{ cmd-say    "tell"    }

	{ NULL NULL }  ; terminator
end

rom-var DUD_WORDS [6]ValueWord =
	{  1 "a"    }
	{  1 "an"   }
	{  1 "the"  }
	{  1 "to"   }
	{  1 "with" }

	{ -1  NULL  }  ; terminator
end

;;----------------------------------------------------------------------

fun welcome-message ()
	println ""
	println "Welcome to a simple adventure game!"
	println ""
end

fun quit-message ()
	println "Goodbye!"
end

fun solved-message ()
	println "You help yourself to the treasure."
	println "With your good health and new-found wealth,"
	println "you live happily ever after...."
	println ""
	println "Congratulations, you solved the game!"
end

fun read-and-process-command ()
	len = input-line
	; just quit on error or EOF [ nothing else we can do! ]
	if neg? len
		[game-over] = TRUE
	else
		parse-user-command
	endif
end

fun parse-user-command ()
	lowercase-words
	split-words

	if zero? [word-count]
		if some? [dud-words]
			println "Huh?"
			println ""
		endif

		return
	endif

	; convert a plain direction into a go command
	verb = [word-buf 0]
	dir  = lookup-word DIRS verb
	if pos? dir
		[word-buf 1] = verb
		[word-count] = 2
		verb = "go"
	endif

	[word-count] = isub [word-count] 1

	cmd = lookup-command verb
	if ref? cmd
		call [cmd .func]
	else
		print "I don't understand \""
		print verb
		println "\""
	endif

	println ""
end

;;----------------------------------------------------------------------

fun cmd-quit ()
	[game-over] = TRUE
end

fun cmd-help ()
	println "Use text commands to walk around and do things."
	println ""
	println "Some examples:"
	println "   go north"
	println "   get the rope"
	println "   drop the lantern"
	println "   inventory"
	println "   unlock door"
	println "   kill the serpent"
	println "   quit"
end

fun cmd-invent ()
	println "You are carrying:"

	i u32 = 0
	count u32 = 0

	loop while lt? i 20
		obj = [player-inventory i]
		if pos? obj
			print "    a "
			println [OBJECTS obj .name]
			count = iadd count 1
		endif
		i = iadd i 1
	endloop

	if zero? count
		println "    nothing"
	endif
end

fun cmd-look ()
	describe-room [player-loc]
end

fun cmd-go ()
	if zero? [word-count]
		println "Go where?"
		return
	endif

	dir = lookup-word DIRS [word-buf 1]
	if neg? dir
		println "I don't understand that direction."
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	i u32 = 0
	loop
		exit = [addr-of room .exits i]
		jump no_dir if eq? 0   [exit .dir]
		jump found  if eq? dir [exit .dir]
		i = iadd i 1
	endloop

no_dir:
	println "You cannot go that way."
	return

found:
	obstacle = [exit .obstacle]

	if eq? obstacle OBJ_Door
		println "The castle door is locked!"

	elif eq? obstacle OBJ_Crocodile
		println "A huge, scary crocodile blocks your path!"

	elif eq? obstacle OBJ_Guard
		println "The guard stops you and says \"Hey, you cannot go in there"
		println "unless you tell me the password!\"."

	else
		[player-loc] = [exit .dest]
		println ""
		describe-room [player-loc]
	endif
end

fun cmd-swim ()
	loc = [player-loc]

	if eq? loc LOC_Outside
		println "But the moat is full of crocodiles!"

	elif ne? loc LOC_Lake
		println "There is nowhere to swim here."

	elif [player-found-key]
		println "You enjoy a nice swim in the lake."

	else
		player-add-obj OBJ_Key
		[player-found-key] = TRUE

		println "You dive into the lake, enjoy paddling around for a while."
		println "Diving a bit deeper, you discover a rusty old key!"
	endif
end

fun cmd-drop ()
	if zero? [word-count]
		println "Drop what?"
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj
		; use a dummy obj number
		obj = 127
	endif

	if player-has-obj obj
		player-remove-obj obj
		room-add-obj room obj

		print "You drop the "
		print [word-buf 1]
		println "."
	else
		print "You are not carrying a "
		print [word-buf 1]
		println "."
	endif
end

fun cmd-get ()
	if zero? [word-count]
		println "Get what?"
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj
		; use a dummy obj number
		obj = 127
	endif

	if not (room-has-obj room obj)
		print "You do not see any "
		print [word-buf 1]
		println " here."

	elif eq? obj OBJ_Crocodile
		println "Are you serious?"
		println "The only thing you would get is eaten!"

	elif eq? obj OBJ_Parrot
		println "The parrot nimbly evades your grasp."

	elif eq? obj OBJ_Guard
		println "A momentary blush suggests the guard was flattered."

	elif eq? obj OBJ_Treasure
		[game-over] = TRUE
		solved-message

	else
		room-remove-obj room obj
		player-add-obj obj

		print "You pick up the "
		print [word-buf 1]
		println "."
	endif
end

fun cmd-give ()
	; need two extra words (object and recipient)
	if lt? [word-count] 2
		println "Give what to whom?"
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj
		; use a dummy obj number
		obj = 127
	endif

	whom = lookup-word OBJECTS [word-buf 2]
	if neg? whom
		; use a dummy obj number
		whom = 127
	endif

	if not (player-has-obj obj)
		print "You can't give a "
		print [word-buf 1]
		println ", since you don't have one!"

	elif not (room-has-obj room whom)
		print "There is no "
		print [word-buf 2]
		println " here."

	elif and (eq? obj OBJ_Carrot) (eq? whom OBJ_Parrot)
		player-remove-obj obj

		println "The parrot happily starts munching on the carrot.  Every now"
		print   "and then you hear it say \""
		print   PASSWORD
		println "\" as it nibbles away on that"
		println "orange stick.  I wonder who this parrot belonged to?"

	elif and (eq? obj OBJ_Steak) (eq? whom OBJ_Crocodile)
		player-remove-obj obj
		room-remove-obj room whom
		free-exit room DIR_E

		println "You hurl the steak towards the crocodile, which suddenly"
		println "snaps into action, grabbing the steak in its steely jaws"
		println "and slithering off to devour its meal in private."

	elif ge? whom OBJ_Crocodile
		print "The "
		print [word-buf 2]
		println " is not interested."

	else
		println "Don't be ridiculous!"
	endif
end

fun cmd-feed ()
	if lt? [word-count] 2
		println "Feed what to whom?"
		return
	endif

	cmd-give
end

fun cmd-open ()
	if zero? [word-count]
		println "Open what?"
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	obj = lookup-word OBJECTS [word-buf 1]

	if ne? obj OBJ_Door
		println "You cannot open that."

	elif ne? [player-loc] LOC_Outside
		println "There is no door here."

	elif not (player-has-obj OBJ_Key)
		println "The door is locked!"

	else
		player-remove-obj OBJ_Key
		free-exit room DIR_E

		println "Carefully you insert the rusty old key in the lock, and turn it."
		println "Yes!!  The door unlocks!  However the key breaks into several"
		println "pieces and is useless now."
	endif
end

fun cmd-attack ()
	if zero? [word-count]
		println "Attack what?"
		return
	endif

	target = lookup-word OBJECTS [word-buf 1]

	if eq? target OBJ_Crocodile
		println "The mere thought of wrestling with that savage beast"
		println "paralyses you with fear!"

	elif eq? target OBJ_Guard
		if player-has-obj OBJ_Sword
			println "You and the guard begin a dangerous sword fight!"
			println "But after ten minutes or so, you are both exhausted and"
			println "decide to call it a draw."
		else
			println "You raise your hands to fight, then notice that the guard"
			println "is carrying a sword, so you shadow box for a while instead."
		endif

	elif player-has-obj OBJ_Sword
		println "You swing your sword, but miss!"

	else
		println "You bruise your hand in the attempt."
	endif
end

fun cmd-use ()
	if zero? [word-count]
		println "Use what?"
		return
	endif

	obj = lookup-word OBJECTS [word-buf 1]
	if neg? obj
		; use a dummy obj number
		obj = 127
	endif

	if not (player-has-obj obj)
		println "You cannot use something you don't have."

	elif eq? obj OBJ_Key
		[word-buf 1] = "door"
		cmd-open

	elif eq? obj OBJ_Sword
		println "You practise your parry skills."

	else
		println "Its lack of utility leads to futility."
	endif
end

fun cmd-say ()
	if zero? [word-count]
		println "Say what?"
		return
	endif

	room = [addr-of ROOMS [player-loc]]

	if eq? [player-loc] LOC_Castle
		if streq? [word-buf 1] PASSWORD
			free-exit room DIR_S

			println "The guard says \"Welcome Sire!\" and beckons you to enter"
			println "the treasury."
			return
		endif
	endif

	println "Nothing happens."
end

;;----------------------------------------------------------------------

; --- player utilities ---

fun player-has-obj (obj Object -> bool)
	i u32 = 0
	loop
		if ge? i 20
			return FALSE
		endif
		if eq? [player-inventory i] obj
			return TRUE
		endif
		i = iadd i 1
	endloop
end

fun player-add-obj (obj Object)
	i u32 = 0
	loop
		if zero? [player-inventory i]
			[player-inventory i] = obj
			return
		endif
		i = iadd i 1
	endloop
end

fun player-remove-obj (obj Object)
	i u32 = 0
	loop while lt? i 20
		if eq? [player-inventory i] obj
			[player-inventory i] = 0
		endif
		i = iadd i 1
	endloop
end

; --- room utilities ---

fun free-exit (room ^Room dir Dir)
	i u32 = 0
	loop while lt? i 20
		exit = [addr-of room .exits i]
		if eq? [exit .dir] dir
			[exit .obstacle] = 0
		endif
		i = iadd i 1
	endloop
end

fun room-has-obj (room ^Room obj Object -> bool)
	i u32 = 0
	loop
		if ge? i 20
			return FALSE
		endif
		if eq? [room .objects i] obj
			return TRUE
		endif
		i = iadd i 1
	endloop
end

fun room-add-obj (room ^Room obj Object)
	i u32 = 0
	loop
		if zero? [room .objects i]
			[room .objects i] = obj
			return
		endif
		i = iadd i 1
	endloop
end

fun room-remove-obj (room ^Room obj Object)
	i u32 = 0
	loop while lt? i 20
		if eq? [room .objects i] obj
			[room .objects i] = 0
		endif
		i = iadd i 1
	endloop
end

fun describe-room (loc Loc)
	room = [addr-of ROOMS loc]

	; show description lines
	i u32 = 0
	loop
		desc = [room .desc i]
		break if null? desc
		println desc
		i = iadd i 1
	endloop

	; show objects which are here
	i u32 = 0
	loop while lt? i 20
		obj = [room .objects i]
		if pos? obj
			print "There is a "
			print [OBJECTS obj .name]
			println " here."
		endif
		i = iadd i 1
	endloop
end

; --- parsing utilities ---

const MAX_LINE = 256

var line-buf [MAX_LINE]uchar = {...}

; each pointer here will point into line-buf
var word-buf [20]^uchar = {...}
var word-count u32 = 0
var dud-words  u32 = 0

fun lowercase-words ()
	i u32 = 0
	loop while lt? i MAX_LINE
		ch = [line-buf i]
		upper = (and (ge? ch 'A') (le? ch 'Z'))
		if upper
			ch = iadd ch 32
			[line-buf i] = ch
		endif
		i = iadd i 1
	endloop
end

fun split-words ()
	[word-count] = 0
	[dud-words]  = 0

	i     u32 = 0
	start s32 = 0
	len   u32 = 0

	loop
		ch = [line-buf i]
		break if zero? ch

		if is-space? ch
			; ensure previous word is NUL-terminated
			[line-buf i] = 0

			if pos? len
				add-word [addr-of line-buf start]
				len = 0
			endif
		else
			if zero? len
				start = i
				len   = 1
			else
				len = iadd len 1
			endif
		endif

		i = iadd i 1
	endloop

	; ensure we have the last word
	if pos? len
		add-word [addr-of line-buf start]
	endif
end

fun is-space? (ch uchar -> bool)
	if le? ch 32
		return TRUE
	endif

	return FALSE
end

fun add-word (word ^uchar)
	; reached limit?
	if ge? [word-count] 20
		return
	endif

	; skip dud words like "a" and "the"
	dud = lookup-word DUD_WORDS word
	if pos? dud
		[dud-words] = iadd [dud-words] 1
		return
	endif

	; handle some aliases
	if streq? word "croc"
		word = "crocodile"
	endif

	[word-buf [word-count]] = word
	[word-count] = iadd [word-count] 1
end

fun lookup-command (s ^uchar -> ^Command)
	i u32 = 0
	loop
		cmd = [addr-of COMMANDS i]
		break if null? [cmd .func]

		if streq? s [cmd .name]
			return cmd
		endif

		i = iadd i 1
	endloop

	; not found
	return NULL
end

fun lookup-word (table ^[100]ValueWord s ^uchar -> s32)
	i u32 = 0
	loop
		entry = [addr-of table i]
		break if neg? [entry .value]

		name = [entry .name]
		if null? name
			; ignore a placeholder (name is "")
		elif streq? s name
			return [entry .value]
		endif

		i = iadd i 1
	endloop

	; not found
	return -1
end

fun streq? (s1 ^uchar s2 ^uchar -> bool)
	loop
		a = [s1]
		b = [s2]
		if ne? a b
			return FALSE
		endif
		if zero? a
			return TRUE
		endif
		s1 = padd s1 1
		s2 = padd s2 1
	endloop
end

; --- i/o utilities ---

fun println (s ^uchar)
	tinyrt-out-str s
	tinyrt-out-byte '\r'
	tinyrt-out-byte '\n'
end

fun print (s ^uchar)
	tinyrt-out-str s
end

fun input-line (-> s32)
	; returns length of line, or -1 on error/eof

	tinyrt-out-str "> "

	len s32 = 0
	[line-buf len] = 0

next_char:
	ch = tinyrt-in-byte
	if neg? ch
		return -1
	endif

	; end of the line?
	if eq? ch 10
		return len
	endif

	; ignore the CR character
	jump next_char if eq? ch 13

	; ignore chars when buffer is full
	len2 = iadd len 4
	jump next_char if ge? len2 MAX_LINE

	[line-buf len] = (conv uchar ch)
	len = iadd len 1

	; keep buffer NUL-terminated at all times
	[line-buf len] = 0

	jump next_char
end

;;----------------------------------------------------------------------

extern-fun tinyrt-out-byte (c u8)
extern-fun tinyrt-out-str  (s ^u8)
extern-fun tinyrt-in-byte  (-> s16)

;;--- editor settings ---
;; vi:ts=4:sw=4:noexpandtab
