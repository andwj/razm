// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

type BaseType int
const (
	TYP_Void BaseType = iota
	TYP_NoReturn
	TYP_Extern

	TYP_Int
	TYP_Float
	TYP_Pointer
	TYP_Bool

	TYP_Array
	TYP_Struct
	TYP_Union
	TYP_Function
)

type Type struct {
	kind BaseType

	// for TYP_Int and TYP_Float, this is number of bits (8,16,32,64).
	// for TYP_Pointer this depends on architecture.
	bits int

	// for TYP_Array, number of elements given in `[]`.  can be zero.
	elems int

	// the total size of this type (in bytes).
	// -1 means it has not (or could not) be determined yet.
	// for unions, it is the size of the largest field.
	// for pointers, this is size of a register.
	// for arrays, this is elems * sub.size
	size int

	// for TYP_Int, distinguishes u8..u64 from s8..s64.
	// for TYP_Pointer it is always set.
	unsigned bool

	// this is pointed-to type for pointers and references, the
	// element type for arrays, and return type for functions.
	sub *Type

	// parameters of a function, fields of a struct or union.
	param []ParamInfo

	// for a user-defined type, this is the TypeDef, otherwise nil.
	def *TypeDef

	// all methods defined for this user-defined type.
	methods []*FuncDef

	// private field for TypeCompare()
	_sequence int

	// private field for NewPointerType()
	_ptr *Type
}

type ParamInfo struct {
	name   string
	ty     *Type
	offset int
}

//----------------------------------------------------------------------

var (
	u8_type  *Type
	u16_type *Type
	u32_type *Type
	u64_type *Type

	s8_type  *Type
	s16_type *Type
	s32_type *Type
	s64_type *Type

	f32_type *Type
	f64_type *Type

	usize_type *Type
	ssize_type *Type

	bool_type  *Type
	mem_type   *Type

	void_type  *Type
	noret_type *Type
)

var all_types []*Type

func InitTypes() {
	all_types = make([]*Type, 0)

	reg_bits := arch.RegisterBits()

	u8_type  = NewType(TYP_Int,  8, true)
	u16_type = NewType(TYP_Int, 16, true)
	u32_type = NewType(TYP_Int, 32, true)
	u64_type = NewType(TYP_Int, 64, true)

	s8_type  = NewType(TYP_Int,  8, false)
	s16_type = NewType(TYP_Int, 16, false)
	s32_type = NewType(TYP_Int, 32, false)
	s64_type = NewType(TYP_Int, 64, false)

	f32_type = NewType(TYP_Float, 32, false)
	f64_type = NewType(TYP_Float, 64, false)

	usize_type = NewType(TYP_Int, reg_bits, true)
	ssize_type = NewType(TYP_Int, reg_bits, false)

	bool_type  = NewType(TYP_Bool, 8, false)

	mem_type   = NewType(TYP_Extern, 0, false)
	mem_type.size = 0

	void_type  = NewType(TYP_Void, 0, false)
	void_type.size = 0

	noret_type = NewType(TYP_NoReturn, 0, false)
	noret_type.size = 0
}

func NewType(kind BaseType, bits int, unsigned bool) *Type {
	ty := new(Type)
	ty.kind = kind
	ty.bits = bits
	ty.unsigned = unsigned
	if bits > 0 {
		ty.size = bits / 8
	} else {
		ty.size = -1
	}
	ty.methods = make([]*FuncDef, 0)

	// add it to global list
	ty._sequence = 1 + len(all_types)
	all_types = append(all_types, ty)

	return ty
}

func NewPointerType(sub *Type) *Type {
	if sub._ptr == nil {
		ty := NewType(TYP_Pointer, 0, false)
		ty.unsigned = true
		ty.bits = arch.RegisterBits()
		ty.size = ty.bits / 8
		ty.sub  = sub

		sub._ptr = ty
	}
	return sub._ptr
}

func GetIntegerType(bits int, unsigned bool) *Type {
	if unsigned {
		switch bits {
		case 8:  return u8_type
		case 16: return u16_type
		case 32: return u32_type
		case 64: return u64_type
		}
	} else {
		switch bits {
		case 8:  return s8_type
		case 16: return s16_type
		case 32: return s32_type
		case 64: return s64_type
		}
	}

	panic("weird bits for GetIntegerType")
}

func CompileAllTypes() {
	// an "alias" is a definition like this: type A B.
	// for these, the alias shares the underlying Type with the original.
	// naturally we must check for infinitely cyclic aliases...

	aliases := make(map[string]*TypeDef, 0)

	for _, def := range ordered_defs {
		if def.kind == DEF_Type {
			tdef := def.d_type

			if tdef.IsAlias() {
				tdef.alias = true
				aliases[tdef.name] = tdef
			} else {
				// create a dummy Type now, it is filled out later
				tdef.CreateBareType()
			}
		}
	}

	if !ResolveTypeAliases(aliases) {
		return
	}

	for _, def := range ordered_defs {
		if def.kind == DEF_Type && !def.d_type.alias && !def.d_type.extern {
			def.d_type.Compile()
		}
	}
}

func (tdef *TypeDef) CreateBareType() {
	tdef.ty = NewType(TYP_Void, 0, false)

	if tdef.extern {
		tdef.ty.kind = TYP_Extern
		tdef.ty.size = 0

	} else if tdef.body != nil && tdef.body.children[0].Match("fun") {
		tdef.ty.kind = TYP_Function
		tdef.ty.size = 0
	}

	// link the Type back to its definition
	tdef.ty.def = tdef
}

func (tdef *TypeDef) IsAlias() bool {
	if tdef.body == nil {
		return false
	}
	if tdef.body.kind != NG_Line {
		return false
	}
	if tdef.body.Len() != 1 {
		return false
	}
	child := tdef.body.children[0]
	return child.kind == NL_Name
}

func ResolveTypeAliases(aliases map[string]*TypeDef) bool {
	// need to loop until either the number of unresolved aliases
	// drops to zero or no changes were possible.  the latter case
	// means we have cyclic aliases.

	for len(aliases) > 0 {
		count   := len(aliases)
		example := ""

		// note that we modify the map as we iterate over it.
		// in Go this is legal, may not be legal in other languages.

		for _, tdef := range aliases {
			if !tdef.ResolveAlias(aliases) {
				return false
			}
			example = tdef.name
		}

		if len(aliases) == count {
			PostError("cyclic dependencies with types (such as %s)", example)
			return false
		}
	}

	return true
}

func (tdef *TypeDef) ResolveAlias(aliases map[string]*TypeDef) bool {
	other := tdef.body.children[0].str

	new_type := ParseKnownType(other)
	if new_type == nil {
		// try again later...
		return true
	}

	if new_type == void_type || new_type == noret_type || new_type == mem_type {
		PostError("custom types cannot be %s", other)
		return false
	}

	// okay, so remove from the list
	delete(aliases, tdef.name)

	tdef.ty = new_type
	return true
}

func (tdef *TypeDef) Compile() error {
	if tdef.body.kind == NG_Body {
		return tdef.CompileStruct()
	}

	line := tdef.body

	ErrorsSetPos(line.pos)

	if line.children[0].Match("fun") {
		return tdef.CompileFuncType()
	}

	new_type, count := ParseTypeSpec(line.children, "custom types")
	if new_type == nil {
		return FAILED
	}

	if count < len(line.children) {
		PostError("extra rubbish after type definition")
		return FAILED
	}

	tdef.ty.kind  = new_type.kind
	tdef.ty.bits  = new_type.bits
	tdef.ty.elems = new_type.elems
	tdef.ty.unsigned = new_type.unsigned

	if new_type.size >= 0 {
		tdef.ty.size = new_type.size
	}

	for _, par := range new_type.param {
		tdef.ty.AddParam(par.name, par.ty)
	}
	tdef.ty.sub = new_type.sub

	return OK
}

func (tdef *TypeDef) CompileFuncType() error {
	tdef.ty.kind = TYP_Function
	tdef.ty.size = 0

	children := tdef.body.children

	if len(children) < 2 {
		PostError("missing parameters after 'fun'")
		return FAILED
	}
	if len(children) > 2 {
		PostError("extra rubbish after type definition")
		return FAILED
	}

	param := children[1]

	if param.kind != NG_Expr {
		PostError("expected parameters in (), got: %s", param.String())
		return FAILED
	}

	fun_type := ParseParameters(param)
	if fun_type == nil {
		return FAILED
	}

	// copy parameters and ret-type into existing Type
	for _, par := range fun_type.param {
		tdef.ty.AddParam(par.name, par.ty)
	}
	tdef.ty.sub = fun_type.sub

	return OK
}

func (tdef *TypeDef) CompileStruct() error {
	if tdef.body.str == "union" {
		tdef.ty.kind = TYP_Union
	} else {
		tdef.ty.kind = TYP_Struct
	}

	for _, line := range tdef.body.children {
		ErrorsSetPos(line.pos)

		if line.Len() < 2 {
			PostError("missing type spec for struct/union field")
			return FAILED
		}

		t_field := line.children[0]
		if !t_field.IsField() {
			PostError("field names must begin with a dot, got: %s", t_field.str)
			return FAILED
		}

		ctx := "field " + t_field.str
		field_type, count := ParseTypeSpec(line.children[1:], ctx)
		if field_type == nil {
			return FAILED
		}

		if count < len(line.children[1:]) {
			PostError("extra rubbish after field definition")
			return FAILED
		}

		tdef.ty.AddParam(t_field.str, field_type)
	}

	if len(tdef.ty.param) == 0 {
		PostError("struct/union type cannot be empty")
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func FinalizeTypes() {
	DetermineTypeSizes()

	if !have_errors {
		DetermineTypeOffsets()
		CreateSizeConstants()
	}
}

func DetermineTypeSizes() {
	// collect all the type definitions that are lacking a size.
	// we will process the list multiple times until it shrinks to
	// zero.  if it fails to shrink, it means some types are cyclic
	// in a way which is impossible to actually use them.

	group := make(map[*Type]bool)
	for _, ty := range all_types {
		group[ty] = true
	}

	last_count := 999999999

	for len(group) > 0 {
		// note that we modify the group as we iterate over it.
		// in Go this is legal, may not be legal in other languages.

		for ty, _ := range group {
			if ty.DetermineSize() {
				delete(group, ty)
			}
		}

		if have_errors {
			break
		}

		count := len(group)

		if count == last_count {
			ErrorsBeginFile("")
			PostError("unable to determine size of some types %s",
				UnsizableExample(group))
			return
		}

		last_count = count
	}
}

func UnsizableExample(group map[*Type]bool) string {
	for ty, _ := range group {
		if ty.def != nil {
			return "(such as " + ty.def.name + ")"
		}
	}
	return ""
}

func (ty *Type) DetermineSize() bool {
	if ty.size >= 0 {
		return true
	}

	if ty.def != nil {
		ErrorsSetPos(ty.def.body.pos)
	} else {
		ErrorsBeginFile("")
	}

	// we only need to handle three things here: arrays, structs and
	// unions.  everything else *should* have a size already.

	// WISH: check for overflow / excessively large types

	switch ty.kind {
	case TYP_Array:
		if ty.sub.size == 0 {
			PostError("array type has zero-size elements: %s", ty.String())
			return false
		}
		if ty.sub.size > 0 {
			ty.size = ty.elems * ty.sub.size
			return true
		}

	case TYP_Struct:
		size := 0
		for _, par := range ty.param {
			if par.ty.size < 0 {
				return false
			}
			size += par.ty.size
		}
		ty.size = size
		return true

	case TYP_Union:
		size := 0
		for _, par := range ty.param {
			if par.ty.size < 0 {
				return false
			}
			if size < par.ty.size {
				size = par.ty.size
			}
		}
		ty.size = size
		return true
	}

	return false
}

func DetermineTypeOffsets() {
	// Note: for unions we leave all offsets as zero.

	for _, ty := range all_types {
		if ty.kind == TYP_Struct {
			ty.DetermineOffsets()
		}
	}
}

func (ty *Type) DetermineOffsets() {
	offset := 0

	for i := range ty.param {
		ty.param[i].offset = offset
		offset += ty.param[i].ty.size
	}
}

func CreateSizeConstants() {
	for name, def := range all_defs {
		if def.kind == DEF_Type {
			ty := def.d_type.ty
			if ty.size >= 0 {
				CreateOneSizeConstant(name, ty.size)
			}
		}
	}
}

func CreateOneSizeConstant(type_name string, size int) {
	name := type_name + ".size"

	con := new(ConstDef)
	con.name  = name
	con.kind  = CONST_Int
	con.value = strconv.Itoa(size)

	def := new(Definition)
	def.kind = DEF_Const
	def.d_const = con

	all_defs[con.name] = def
}

//----------------------------------------------------------------------

func ParseParameters(param *Node) *Type {
	ty := NewType(TYP_Function, 0, false)
	ty.size = 0

	children := param.children

	for len(children) > 0 {
		if len(children) < 2 {
			PostError("malformed parameters")
			return nil
		}
		if ty.sub != nil {
			PostError("extra rubbish after return type")
			return nil
		}

		t_name := children[0]

		if t_name.Match("->") {
			ret, count := ParseTypeSpec2(children[1:], "a function return", true, false)
			if ret == nil {
				return nil
			}
			ty.sub = ret
			children = children[1+count:]
			continue
		}

		if t_name.kind != NL_Name {
			PostError("expected parameter name, got: %s", t_name.String())
			return nil
		}
		if ValidateName(t_name, "parameter", ALLOW_SHADOW) != OK {
			return nil
		}
		if ty.FindParam(t_name.str) >= 0 {
			PostError("duplicate parameter name: %s", t_name.str)
			return nil
		}

		ctx := "parameter '" + t_name.str + "'"
		par_type, count := ParseTypeSpec(children[1:], ctx)
		if par_type == nil {
			return nil
		}

		ty.AddParam(t_name.str, par_type)

		children = children[1+count:]
	}

	if ty.sub == nil {
		ty.sub = void_type
	}
	return ty
}

func ValidateType(ty *Type) error {
	if ty.kind == TYP_Function {
		switch ty.sub.kind {
		case TYP_Array, TYP_Struct, TYP_Union, TYP_Function:
			PostError("bad return type, cannot be %s", ty.sub.SimpleName())
				return FAILED
		}
		for _, par := range ty.param {
			switch par.ty.kind {
			case TYP_Array, TYP_Struct, TYP_Union, TYP_Function:
				PostError("bad type for parameter '%s', cannot be a %s", par.name, par.ty.SimpleName())
				return FAILED
			}
		}
	}

	return OK
}

func ParseTypeSpec(input []*Node, ctx string) (*Type, int) {
	return ParseTypeSpec2(input, ctx, false, false)
}

func ParseTypeSpec2(input []*Node, ctx string, allow_void bool, allow_extern bool) (*Type, int) {
	// NOTE WELL: this code cannot validate anything inside a Type received
	// from ParseTypeSpec or ParseKnownType, since that may be a definition
	// which has not been compiled yet.  But we *can* compare a pointer with
	// a singleton like `void_type` etc...

	if len(input) == 0 {
		panic("no input to ParseTypeSpec")
	}

	head := input[0]

	if head.Match("^") {
		if len(input) < 2 {
			PostError("missing type for pointer")
			return nil, -1
		}

		sub, count := ParseTypeSpec2(input[1:], "a pointer target", false, true)
		if sub == nil {
			return nil, -1
		}

		ty := NewPointerType(sub)
		return ty, 1 + count
	}

	if head.kind == NG_Access {
		if len(input) < 2 {
			PostError("missing type for array")
			return nil, -1
		}
		sub, count := ParseTypeSpec(input[1:], "array elements")
		if sub == nil {
			return nil, -1
		}

		if head.Len() != 1 {
			PostError("malformed size in [] for array")
			return nil, -1
		}

		t_len := head.children[0]
		arr_len := ParseArrayLength(t_len)
		if arr_len < 0 {
			return nil, -1
		}

		ty := NewType(TYP_Array, 0, false)
		ty.elems = arr_len
		ty.sub   = sub

		// compute size if possible
		if ty.sub.size == 0 {
			PostError("array type has zero-size elements: %s", ty.String())
			return nil, -1
		}
		if ty.sub.size > 0 {
			ty.size = ty.elems * ty.sub.size
		}

		return ty, 1 + count
	}

	if head.kind != NL_Name {
		PostError("expected type spec, got: %s", head.String())
		return nil, -1
	}

	ty := ParseKnownType(head.str)
	if ty == nil {
		PostError("no such type: %s", head.str)
		return nil, -1
	}

	if !allow_void {
		if ty == void_type {
			PostError("%s cannot be void type", ctx)
			return nil, -1
		}
		if ty == noret_type {
			PostError("%s cannot be no-return type", ctx)
			return nil, -1
		}
	}

	if !allow_extern {
		if ty == mem_type {
			PostError("%s cannot be raw-mem type", ctx)
			return nil, -1
		}

		// despite the scary warning above, we can check for TYP_Extern and
		// TYP_Function here due to the logic in CreateBareType().

		if ty.kind == TYP_Extern {
			PostError("%s cannot be an external type", ctx)
			return nil, -1
		}
		if ty.kind == TYP_Function {
			PostError("%s cannot be a function type", ctx)
			return nil, -1
		}
	}

	// okay
	return ty, 1
}

func ParseKnownType(name string) *Type {
	user := all_defs[name]
	if user != nil {
		if user.kind == DEF_Type {
			return user.d_type.ty
		}
	}

	switch name {
	case "u8":  return u8_type
	case "u16": return u16_type
	case "u32": return u32_type
	case "u64": return u64_type

	case "s8":  return s8_type
	case "s16": return s16_type
	case "s32": return s32_type
	case "s64": return s64_type

	case "f32": return f32_type
	case "f64": return f64_type

	case "usize": return usize_type
	case "ssize": return ssize_type

	case "bool":      return bool_type
	case "raw-mem":   return mem_type

	case "void":      return void_type
	case "no-return": return noret_type
	}

	return nil
}

func ParseArrayLength(t_len *Node) int {
	length_str := ""

	if t_len.kind == NG_Expr {
		var res EvalResult
		res, t_len = EvalExpression(t_len, nil)

		if res != EVAL_OK {
			return -1
		}
	}

	if t_len.kind == NL_Integer {
		length_str = t_len.str

	} else if t_len.kind == NL_Name {
		// handle user constants
		def := all_defs[t_len.str]
		if def == nil || def.kind != DEF_Const {
			PostError("bad length for array, no such constant: %s", t_len.str)
			return -1
		}
		if def.d_const.kind != CONST_Int {
			PostError("bad length for array, %s is not an integer", t_len.str)
			return -1
		}
		length_str = def.d_const.value

	} else {
		PostError("bad length for array, wanted int, got: %s", t_len.String())
		return -1
	}

	arr_len, err := strconv.ParseInt(length_str, 0, 64)
	if err != nil || arr_len < 0 {
		PostError("bad length for array: %s", length_str)
		return -1
	}
	return int(arr_len)
}

//----------------------------------------------------------------------

func (ty *Type) AddParam(name string, par_type *Type) {
	ty.param = append(ty.param, ParamInfo{name, par_type, 0})
}

func (ty *Type) FindParam(name string) int {
	for i, par := range ty.param {
		if par.name == name {
			return i
		}
	}
	return -1  // not found
}

func (ty *Type) AddMethod(fu *FuncDef) {
	ty.methods = append(ty.methods, fu)
}

func (ty *Type) FindMethod(name string) *FuncDef {
	for _, fu := range ty.methods {
		if fu.name == name {
			return fu
		}
	}
	return nil  // not found
}

//----------------------------------------------------------------------

func TypeCompare(src, dest *Type) bool {
	// this works in a structural way.
	// to prevent infinite looping with cyclical types, we keep track
	// of which pairs of types have been compared so far.

	seen_pairs := make(map[uint64]bool)

	return TypeCompare2(src, dest, seen_pairs)
}

func TypeCompare2(src, dest *Type,
		seen_pairs map[uint64]bool) bool {

	if src  == nil { panic("TypeCompare with nil src")  }
	if dest == nil { panic("TypeCompare with nil dest") }

	if src == dest {
		return true
	}

	if src.kind != dest.kind {
		return false
	}


	switch src.kind {
	case TYP_Void, TYP_NoReturn, TYP_Extern, TYP_Bool:
		return true

	case TYP_Int:
		// arch-dependent types are not compatible with fixed size types.
		// this is disallowed because otherwise it means there can be code
		// that compiles fine on 32-bit but fails to compile on 64-bit, or
		// vice versa.
		S_archy := (src  == usize_type) || (src  == ssize_type);
		D_archy := (dest == usize_type) || (dest == ssize_type);
		if S_archy != D_archy {
			return false
		}

		return src.bits == dest.bits

	case TYP_Float:
		return src.bits == dest.bits

	case TYP_Union:
		// perhaps this is too lax...
		return src.size == dest.size
	}

	// produce a value representing this pair of types, and check
	// whether we have processed this pair before.
	pair := (uint64(src._sequence) << 32) | uint64(dest._sequence)

	if seen_pairs[pair] {
		return true
	}

	// need to mark it as seen before recursing.
	seen_pairs[pair] = true

	// pointers have a special rule.
	// we consider ^X and ^[]X and ^[][]X (etc) to be compatible.
	// also allow ^raw-mem to match any other pointer.

	if src.kind == TYP_Pointer {
		if src.sub == mem_type || dest.sub == mem_type {
			return true
		}

		src  =  src.sub.GetNonArray()
		dest = dest.sub.GetNonArray()

		return TypeCompare2(src, dest, seen_pairs)
	}

	// from here we handle TYP_Struct, TYP_Union, TYP_Array and TYP_Function.

	if len(src.param) != len(dest.param) {
		return false
	}

	if src.sub != nil {
		if !TypeCompare2(src.sub, dest.sub, seen_pairs) {
			return false
		}
	}

	for i := range src.param {
		src_par  := src.param[i]
		dest_par := dest.param[i]

		if !TypeCompare2(src_par.ty, dest_par.ty, seen_pairs) {
			return false
		}
	}

	return true
}

func (ty *Type) GetNonArray() *Type {
	// infinitely cyclic array types should not be possible, but
	// for safety we don't loop forever here...
	for d := 0; d < 100; d++ {
		if ty.kind != TYP_Array {
			return ty
		}
		ty = ty.sub
	}
	return ty
}

//----------------------------------------------------------------------

func (ty *Type) SimpleName() string {
	return ty.kind.String()
}

func (kind BaseType) String() string {
	switch kind {
	case TYP_Void:     return "void"
	case TYP_NoReturn: return "no-return"
	case TYP_Extern:   return "extern"
	case TYP_Int:      return "integer"
	case TYP_Float:    return "float"
	case TYP_Bool:     return "boolean"
	case TYP_Pointer:  return "pointer"
	case TYP_Array:    return "array"
	case TYP_Struct:   return "struct"
	case TYP_Union:    return "union"
	case TYP_Function: return "function"
	default:
		panic("unknown type")
	}
}

func (ty *Type) String() string {
	// check whether this is a custom type.
	// this is obviously slow, but this code is only used for error
	// messages and debugging, so speed is not an issue.
	// it is also needed to prevent an infinite loop with types
	// which form cyclic references (to itself or other types).
	if ty.def != nil {
		return ty.def.name
	}

	if ty == usize_type {
		return "usize"
	}
	if ty == ssize_type {
		return "ssize"
	}

	switch ty.kind {
	case TYP_Void:
		return "void"

	case TYP_NoReturn:
		return "no-return"

	case TYP_Extern:
		return "extern"

	case TYP_Int:
		s := "s"
		if ty.unsigned {
			s = "u"
		}
		return s + strconv.Itoa(ty.bits)

	case TYP_Float:
		return "f" + strconv.Itoa(ty.bits)

	case TYP_Bool:
		return "bool"

	case TYP_Pointer:
		return "^" + ty.sub.String()

	case TYP_Array:
		return "[]" + ty.sub.String()

	case TYP_Struct:
		return "struct"

	case TYP_Union:
		return "union"

	case TYP_Function:
		s := "fun("
		for _, par := range ty.param {
			if s != "" { s = s + " " }
			s = s + par.ty.String()
		}
		if ty.sub.kind != TYP_Void {
			if s != "" { s = s + " " }
			s = s + "-> " + ty.sub.String()
		}
		return s + ")"

	default:
		panic("unknown type")
	}
}
