// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "fmt"
import "sort"
import "path/filepath"

var OK     error = nil
var FAILED error = fmt.Errorf("FAILED")

type LocError struct {
	Str  string // error message
	File string // filename of error, or ""
	Func string // function which was being compiled, or ""
	Glob string // global var/const being compiled, or ""
	Line int    // line # where error occurred, or 0
}

// this is set if any errors have been posted.
var have_errors bool

// parpos contains the current location of parsing (module, file, line, etc).
// it is private to this file!
var parpos LocError

// error_list contains all the errors posted via PostError().
var error_list []*LocError

func ClearErrors() {
	error_list = make([]*LocError, 0)

	parpos.File = ""
	parpos.Func = ""
	parpos.Glob = ""
	parpos.Line = 0

	have_errors = false
}

func SortErrors() {
	sort.Slice(error_list, func(i, k int) bool {
		F1 := error_list[i].File
		F2 := error_list[k].File

		if F1 == "" { F1 = "\U000FFFFF" }
		if F2 == "" { F2 = "\U000FFFFF" }

		if F1 != F2 {
			return F1 < F2
		}

		L1 := error_list[i].Line
		L2 := error_list[k].Line

		if L1 == 0 { L1 = 99999999 }
		if L2 == 0 { L2 = 99999999 }

		return L1 < L2
	});
}

func ShowErrors(dest io.Writer) {
	SortErrors()

	for i, err := range error_list {
		if i > 0 {
			fmt.Fprintf(dest, "\n")
		}

		fmt.Fprintf(dest, "ERROR: %s\n", err.Str)

		loc_str := err.Location()
		if loc_str != "" {
			fmt.Fprintf(dest, "Occurred%s\n", loc_str)
		}
	}

//	if len(error_list) > 0 {
//		fmt.Fprintf(dest, "\n")
//	}
}

func PostError(format string, a ...interface{}) {
	// copy the current location info
	L := parpos

	// set message
	L.Str = fmt.Sprintf(format, a...)

	error_list = append(error_list, &L)

	have_errors = true
}

func ErrorsBeginFile(name string) {
	parpos.File = name
	parpos.Line = 0
	parpos.Func = ""
	parpos.Glob = ""
}

func ErrorsSetGlobal(name string) {
	parpos.Func = ""
	parpos.Glob = name
}

func ErrorsSetFunction(name string) {
	parpos.Func = name
	parpos.Glob = ""
}

func ErrorsSetLine(Line int) {
	parpos.Line = Line
}

func ErrorsSetPos(pos Position) {
	if pos.file > 0 {
		parpos.File = Options.in_files[pos.file - 1]
	}
	if pos.line > 0 {
		parpos.Line = pos.line
	}
}

//----------------------------------------------------------------------

func (err *LocError) Error() string {
	return err.Str
}

func (err *LocError) Location() string {
	l_part := ""
	if err.Line > 0 {
		l_part = fmt.Sprintf(" near line %d", err.Line)
	}

	f_part := ""
	if err.File != "" {
		f_part = " of " + filepath.Base(err.File)
	}

	g_part := ""
	if err.Glob != "" {
		g_part = " in global '" + err.Glob + "'"
	} else if err.Func != "" {
		g_part = " in function '" + err.Func + "'"
	}

	return l_part + f_part + g_part
}
