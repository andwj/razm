" Vim syntax file
" Language:	RAZM
" Maintainer:	Andrew Apted  <ajapted@users.sf.net>
" Last Change:	2021 Jan 16
" Home Page:	http://gitlab.com/andwj/razm

" error format -- this must be done manually:
"   :set efm=%EERROR:\ %m,%ZOccurred\ near\ line\ %l\ of\ %f%.%#
"   :cfile! ERRS

" quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

" syntax is case sensitive
syn case match

" identifier characters
setlocal iskeyword=33,36-39,42-43,45-57,60-90,95,97-122,126,_

" comments
syn region  razmComment		start=/;/ keepend end=/$/ contains=razmCommentTodo
syn keyword razmCommentTodo 	contained TODO FIXME XXX

" special directives
syn region razmDirective	start=/^#/ keepend end=/$/
syn match  razmDirective	/#\k\+#\?/

" labels
syn match razmLabel		/^\s*\k*:/

" identifiers
syn match razmIdentifier	/\k\+/

" strings and characters
syn region razmString		start=/"/ skip=/\\\\\|\\"/ end=/"/
syn region razmString		start=/'/ skip=/\\\\\|\\'/ end=/'/

" integers
syn match razmNumber		/[+-]\?\d\+/
syn match razmNumber		/[+-]\?0b[01]\+/
syn match razmNumber		/[+-]\?0x\x\+/

" floating point
syn match razmNumber		/[+-]\?\d\+[.]\d\+/
syn match razmNumber		/[+-]\?\d\+[.]\d*[eE][+-]\?\d\+/
syn match razmNumber		/[+-]\?\d\+[eE][+-]\?\d\+/
syn match razmNumber		/[+-]\?0x\x\+[.]\x*[pP][+-]\?\d\+/
syn match razmNumber		/[+-]\?0x\x\+[pP][+-]\?\d\+/

" types
syn keyword razmType		void no-return raw-mem
syn keyword razmType		s8 s16 s32 s64 ssize
syn keyword razmType		u8 u16 u32 u64 usize bool
syn keyword razmType		f32 f64
syn keyword razmType		array endarray
syn keyword razmType		struct endstruct
syn keyword razmType		union endunion

syn match razmType		/\^/

" language keywords
syn keyword razmKeyword		const type extern-type end
syn keyword razmKeyword		var extern-var zero-var rom-var
syn keyword razmKeyword		fun extern-fun inline-fun
syn keyword razmKeyword		if elif else endif
syn keyword razmKeyword		loop endloop while until
syn keyword razmKeyword		return tail-call jump break unless
syn keyword razmKeyword		swap mem-read mem-write stack-var
syn keyword razmKeyword		call conv raw-cast addr-of sel-if

" constants
syn keyword razmConstant	NULL FALSE TRUE
syn keyword razmConstant	+INF -INF QNAN SNAN
syn keyword razmConstant	CPU_BITS CPU_ENDIAN PTR_SIZE

" intrinsics
syn keyword razmIntrinsic	eq? ne? lt? le? gt? ge?
syn keyword razmIntrinsic	zero? some? pos? neg?
syn keyword razmIntrinsic	null? ref? inf? nan?
syn keyword razmIntrinsic	not and or
syn keyword razmIntrinsic	padd psub
syn keyword razmIntrinsic	iadd isub imul idivt iremt ishl ishr
syn keyword razmIntrinsic	ineg iabs iflip iand ior ixor imin imax
syn keyword razmIntrinsic	fadd fsub fmul fdiv fremt
syn keyword razmIntrinsic	fneg fabs finv fsqrt fmax fmin
syn keyword razmIntrinsic	fround ftrunc ffloor fceil
syn keyword razmIntrinsic	little-endian big-endian

" Define the default highlighting.
" Only when an item doesn't have highlighting yet

hi def link razmComment		Comment
hi def link razmCommentTodo	Todo
hi def link razmConstant	Constant
hi def link razmDirective	Special
hi def link razmIntrinsic	Function
hi def link razmIdentifier	Normal
hi def link razmKeyword		Keyword
hi def link razmLabel		Normal
hi def link razmNumber		Number
hi def link razmNumberError	Error
hi def link razmNumberFloat	Float
hi def link razmString		String
hi def link razmStringError	Error
hi def link razmStringEscape	SpecialChar
hi def link razmType		Type


let b:current_syntax = "razm"

" vim:ts=8:sw=8:noexpandtab:nowrap
