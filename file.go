// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "bufio"

type InputFile struct {
	name    string
	pos     Position

	// current visibility mode (default is private)
	public bool

	reader  *bufio.Reader
	hit_eof bool
}

//----------------------------------------------------------------------

func NewInputFile(name string, file int, r io.Reader) *InputFile {
	f := new(InputFile)

	f.name = name
	f.pos = Position{line:0, file:file}
	f.reader = bufio.NewReader(r)
	f.hit_eof = false

	return f
}

// Load reads the input file, decodes each line into low-level tokens
// and does some awesome shit.  Returns OK if everything was fine, or
// FAILED if an error occurred.
func (f *InputFile) Load() error {
	var saved *Node

	for {
		var t *Node

		if saved == nil {
			t = f.NextLine()

			if t == nil {
				if have_errors {
					return FAILED
				}
				return OK
			}
		} else {
			t = saved
			saved = nil
		}

		ErrorsSetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			saved = f.SkipAhead()
			continue
		}

		// NOTE: on parse failures here where generally skip ahead to
		//       the next known directive.  this allows multiple syntax
		//       errors to be found and reported.

		head := t.children[0]

		if head.kind == NL_Indent {
			PostError("indented line found outside a function")
			saved = f.SkipAhead()
			continue
		}
		if head.kind != NL_Name {
			PostError("expected a directive, got: %s", t.String())
			saved = f.SkipAhead()
			continue
		}

		switch {
		case head.Match("const"):
			if f.LoadConst(t) != OK {
				saved = f.SkipAhead()
			}

		case head.Match("type"):
			if f.LoadType(t, 0) != OK {
				saved = f.SkipAhead()
			}

		case head.Match("extern-type"):
			if f.LoadType(t, 'e') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("var"):
			if f.LoadVar(t, 0) != OK {
				saved = f.SkipAhead()
			}

		case head.Match("extern-var"):
			if f.LoadVar(t, 'e') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("rom-var"):
			if f.LoadVar(t, 'r') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("zero-var"):
			if f.LoadVar(t, 'z') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("fun"):
			if f.LoadFunction(t, 0) != OK {
				saved = f.SkipAhead()
			}

		case head.Match("extern-fun"):
			if f.LoadFunction(t, 'e') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("inline-fun"):
			if f.LoadFunction(t, 'i') != OK {
				saved = f.SkipAhead()
			}

		case head.Match("#public"):
			f.public = true

		case head.Match("#private"):
			f.public = false

		case head.Match("end"):
			PostError("end directive outside a function")
			saved = f.SkipAhead()

		default:
			PostError("unknown directive: %s", head.str)
			saved = f.SkipAhead()
		}
	}
}

func (f *InputFile) SkipAhead() *Node {
	for {
		t := f.NextLine()
		if t == nil || t.kind == NL_ERROR {
			return t
		}
		head := t.children[0]
		switch {
		case head.Match("fun"), head.Match("var"),
			head.Match("const"), head.Match("type"),
			head.Match("extern-fun"), head.Match("extern-var"),
			head.Match("zero-var"), head.Match("rom-var"):

			return t
		}
	}
}

// NextLine reads the next logical line from the input file.
// If the raw line is semantically unterminated (e.g. ends with a
// backslash), the next line or lines are read to finish it.
//
// Returns an NG_Line node containing the line broken into words,
// or NIL to indicate EOF, or an NL_ERROR if an error occurred.
func (f *InputFile) NextLine() *Node {
	var code *Node

	for {
		line, err := f.FetchRawLine()

		if err == io.EOF {
			if code != nil {
				return NewNode(NL_ERROR, "missing line after \\", f.pos)
			}
			return nil
		}
		if err != nil {
			return NewNode(NL_ERROR, err.Error(), f.pos)
		}

		// break the line into tokens, remove comments
		t := LexLine(line, f.pos)
		if t.kind == NL_ERROR {
			return t
		}

		if code == nil {
			code = t
		} else {
			code.Join(t)
		}

		// handle a trailing backslash to extend the line
		size := code.Len()
		if size > 0 && code.children[size-1].Match("\\") {
			code.children = code.children[0:size-1]
			continue
		}

		code = LexFinishLine(code)
		if code.kind == NL_ERROR {
			return code
		}

		// skip empty lines
		if code.Len() == 0 {
			code = nil
			continue
		}
		return code
	}
}

func (f *InputFile) FetchRawLine() (string, error) {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if f.hit_eof {
		return "", io.EOF
	}

	f.pos.line += 1

	// NOTE: this can return some data + io.EOF
	s, err := f.reader.ReadString('\n')

	if err == io.EOF {
		f.hit_eof = true
	} else if err != nil {
		f.hit_eof = true
		return "", err
	}

	// strip CR and LF from the end
	if len(s) > 0 && s[len(s)-1] == '\n' {
		s = s[0:len(s)-1]
	}
	if len(s) > 0 && s[len(s)-1] == '\r' {
		s = s[0:len(s)-1]
	}

	return s, OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadConst(line *Node) error {
	children := line.children

	if len(children) < 4 {
		PostError("const def is missing name or value")
		return FAILED
	}
	if !children[2].Match("=") {
		PostError("const def is missing '=' after name")
		return FAILED
	}
	if len(children) > 4 {
		PostError("const def has extra rubbish at end")
		return FAILED
	}

	t_name  := children[1]
	t_value := children[3]

	if ValidateName(t_name, "const", 0) != OK {
		return FAILED
	}

	con := new(ConstDef)
	con.name  = t_name.str
	con.value = t_value.str

	def := new(Definition)
	def.kind = DEF_Const
	def.d_const = con

	all_defs[con.name] = def

	// NL_Null and NL_FltSpec are deliberately excluded here

	switch t_value.kind {
	case NL_Integer:
		con.kind = CONST_Int

	case NL_Float:
		con.kind = CONST_Float

	case NL_Char:
		con.kind  = CONST_Char

	case NL_Bool:
		con.kind  = CONST_Bool

	case NL_String:
		con.kind = CONST_String

	case NG_Expr, NL_Name:
		// this will be evaluated later (in CompileAllConstants)
		con.expr  = t_value
		con.value = "0"

	default:
		PostError("bad const def: expected literal value, got: %s", t_value.String())
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadType(line *Node, mode rune) error {
	children := line.children

	if len(children) < 2 || (mode != 'e' && len(children) < 3) {
		PostError("type def is missing name or definition")
		return FAILED
	}

	t_name := children[1]

	if ValidateName(t_name, "type", 0) != OK {
		return FAILED
	}

	tdef := new(TypeDef)
	tdef.name = t_name.str
	tdef.extern = (mode == 'e')

	def := new(Definition)
	def.kind = DEF_Type
	def.d_type = tdef

	all_defs[tdef.name] = def

	// the actual Type is created later on...

	// something with a body?
	if mode != 'e' {
		t_part := children[2]

		if t_part.Match("struct") || t_part.Match("union") {
			tdef.body = NewNode(NG_Body, t_part.str, line.pos)
			return f.LoadBody(tdef.body, "type")

		} else {
			tdef.body = NewNode(NG_Line, "", line.pos)
			for _, child := range children[2:] {
				tdef.body.Add(child)
			}
		}
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadVar(line *Node, mode rune) error {
	children := line.children

	if len(children) < 3 {
		PostError("variable is missing name or type")
		return FAILED
	}

	t_name := children[1]

	if ValidateName(t_name, "variable", 0) != OK {
		return FAILED
	}

	v := new(VarDef)
	v.name = t_name.str
	v.extern = (mode == 'e')
	v.zeroed = (mode == 'z')
	v.rom    = (mode == 'r')
	v.public = f.public

	def := new(Definition)
	def.kind = DEF_Var
	def.d_var = v

	all_defs[v.name] = def

	// check if we have an inline value
	equal_pos := -1
	for i := 2; i < len(children); i++ {
		if children[i].Match("=") {
			equal_pos = i
			break
		}
	}

	if equal_pos == 2 {
		PostError("missing type for variable")
		return FAILED
	}

	// collect type tokens, they are parsed later
	raw_type := NewNode(NG_Line, "", line.pos)
	ty_end   := equal_pos
	if equal_pos < 0 { ty_end = len(children) }

	for k := 2; k < ty_end; k++ {
		raw_type.Add(children[k])
	}

	v.raw_type = raw_type

	// collect the value expression, if present
	if equal_pos > 0 {
		if v.extern || v.zeroed {
			PostError("unexpected value for variable")
			return FAILED
		}

		children = children[1+equal_pos:]

		// handle a value which occupies multiple lines.
		// we don't know the exact type yet, so this form is allowed
		// for all types, even if it is a bit silly for one integer.
		if len(children) == 0 {
			v.expr = NewNode(NG_Body, "var", line.pos)
			return f.LoadBody(v.expr, "variable")
		}

		if len(children) == 1 {
			v.expr = children[0]
		} else {
			if children[0].kind == NG_Data {
				PostError("extra rubbish after data in {}")
				return FAILED
			}

			v.expr = NewNode(NG_Line, "", line.pos)
			for _, child := range children {
				v.expr.Add(child)
			}
		}

		return OK
	}

	if !(v.extern || v.zeroed) {
		PostError("missing value for variable, expected '='")
		return FAILED
	}

	return OK
}

//----------------------------------------------------------------------

func (f *InputFile) LoadFunction(line *Node, mode rune) error {
	children := line.children

	if len(children) < 3 {
		PostError("function is missing name or parameters")
		return FAILED
	}
	if len(children) > 3 {
		PostError("extra rubbish after function parameters")
		return FAILED
	}

	t_name  := children[1]
	t_param := children[2]

	is_method := false

	if t_name.IsField() {
		is_method = true
	} else if ValidateName(t_name, "function", 0) != OK {
		return FAILED
	}

	fu := new(FuncDef)
	fu.name = t_name.str
	fu.extern = (mode == 'e')
	fu.inline = (mode == 'i')
	fu.method = is_method
	fu.public = f.public
	fu.raw_pars = t_param

	if is_method {
		all_methods = append(all_methods, fu)
	} else {
		def := new(Definition)
		def.kind = DEF_Func
		def.d_func = fu

		all_defs[fu.name] = def
	}

	if fu.extern {
		// I... ain't got no body
		return OK
	} else {
		// handle body of function
		fu.body = NewNode(NG_Body, "code", line.pos)
		return f.LoadBody(fu.body, "function")
	}
}

func (f *InputFile) LoadBody(body *Node, what string) error {
	for {
		t := f.NextLine()
		if t == nil {
			PostError("unfinished %s (reached end of file)", what)
			return FAILED
		}
		ErrorsSetPos(t.pos)

		if t.kind == NL_ERROR {
			PostError("%s", t.str)
			return FAILED
		}

		head := t.children[0]

		// check a few unusable forms here...
		for i, child := range t.children {
			if child.kind == NH_Label && what != "function" {
				PostError("labels can only be used in functions")
				return FAILED
			}
			if i > 0 && child.Match("end") {
				PostError("the 'end' directive must appear at start of line")
				return FAILED
			}
		}

		if head.Match("end") {
			if t.Len() > 1 {
				PostError("extra rubbish after 'end' directive")
				return FAILED
			}
			break

		} else if head.kind == NL_Indent {
			// remove the indent so later code won't have to handle it.
			// the lexer ensures there is something after it.
			t.children = t.children[1:]

		} else if head.kind == NH_Label {
			// ok

		} else {
			PostError("unfinished %s (reached another directive)", what)
			return FAILED
		}

		// we don't do any analysis of the code/data here.
		body.Add(t)
	}

	return OK
}
