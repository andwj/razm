// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strings"
import "sort"

var all_defs     map[string]*Definition
var ordered_defs []*Definition

var all_methods  []*FuncDef

type Definition struct {
	kind    DefKind

	d_const *ConstDef
	d_type  *TypeDef
	d_var   *VarDef
	d_func  *FuncDef
}

type DefKind int
const (
	DEF_Const DefKind = iota
	DEF_Type
	DEF_Func
	DEF_Var
)

type ConstDef struct {
	name   string
	kind   ConstKind
	value  string
	expr   *Node
}

type ConstKind int
const (
	CONST_Int   = iota
	CONST_Float
	CONST_Char
	CONST_Bool
	CONST_String
)

type TypeDef struct {
	name   string
	extern bool
	alias  bool
	body   *Node
	ty     *Type
}

type VarDef struct {
	name     string
	extern   bool
	zeroed   bool
	rom      bool
	public   bool

	expr     *Node
	raw_type *Node
	ty       *Type
	ptr      *Type
}

type FuncDef struct {
	name     string
	extern   bool
	inline   bool
	method   bool
	public   bool

	raw_pars *Node
	body     *Node
	ty       *Type
	ptr      *Type

	// parsing info....
	labels      map[string]bool
	used_labels map[string]bool
	if_labels   int
	loop_labels int
	error_num   int

	locals      map[string]*LocalInfo
	seen_locals map[string]bool
	params      []*LocalInfo
	blocks      ActiveBlocks
	num_temps   int
	num_inlines int

	// optimisation info....
	paths  []*CodePath
	missing_ret bool
}

type LocalInfo struct {
	name string
	ty   *Type

	// this is >= 0 for parameters, -1 for locals
	param_idx int

	// true if we have detected an error with this local
	errored bool

	// these keep track of usages of a local var.
	// the `writes` field includes the creation of the var.
	reads  int
	writes int

	// estimated cost for being on the stack
	cost int64

	// live range of local (indices into the function body).
	// to account for loops, both ends may be extended.
	live_start int
	live_end   int

	// register of a parameter (as per the ABI)
	abi_reg Register

	// register assignment, for REG_ON_STACK the offset is valid
	reg    Register
	offset int
}

func InitDefs() {
	all_defs = make(map[string]*Definition)
	all_methods = make([]*FuncDef, 0)
}

func CreateOrderedDefs() {
	ordered_defs = make([]*Definition, 0)

	for _, def := range all_defs {
		ordered_defs = append(ordered_defs, def)
	}

	sort.Slice(ordered_defs, func(i, k int) bool {
		d1 := ordered_defs[i]
		d2 := ordered_defs[k]

		return d1.Name() < d2.Name()
	})
}

func (def *Definition) Name() string {
	switch def.kind {
	case DEF_Const: return def.d_const.name
	case DEF_Type:  return def.d_type.name
	case DEF_Func:  return def.d_func.name
	case DEF_Var:   return def.d_var.name
	default:        return ""
	}
}

func (A *LocalInfo) OrdLess(B *LocalInfo) bool {
	// firstly, handle parameters
	if A.param_idx >= 0 {
		if B.param_idx < 0 {
			return true
		}
		return A.param_idx < B.param_idx
	}
	if B.param_idx >= 0 {
		return false
	}

	// secondly, look at live range
	if A.live_start != B.live_start {
		return A.live_start < B.live_start
	}
	if A.live_end != B.live_end {
		return A.live_end < B.live_end
	}

	// tie breaker, look at the name
	return A.name < B.name
}

//----------------------------------------------------------------------

// Register represents a general purpose CPU register (when > 0).
// Exactly which CPU register it is depends on the architecture
// back-end.  Not all registers are interchangeable, e.g. some will
// be floating point (like XMM0), some are integer/pointer (like RAX).
//
// Zero and negative values of Register have special meanings.
// REG_NONE is used when a register has not been decided yet.
// REG_ON_STACK is used in certain places for values that will be
// stored on (or moved to) the stack.
type Register int

const REG_NONE     Register = 0
const REG_ON_STACK Register = -1

type RegisterSet struct {
	group map[Register]bool
}

func NewRegisterSet() *RegisterSet {
	rs := new(RegisterSet)
	rs.group = make(map[Register]bool)
	return rs
}

func (rs *RegisterSet) Add(r Register) {
	rs.group[r] = true
}

func (rs *RegisterSet) Has(r Register) bool {
	return rs.group[r]
}

//----------------------------------------------------------------------

const (
	// allow the name to shadow a global definition
	ALLOW_SHADOW = (1 << iota)
	ALLOW_UNDERSCORE
)

func ValidateName(t_name *Node, what string, flags int) error {
	if t_name.kind != NL_Name {
		PostError("bad %s def: name is not an identifier", what)
		return FAILED
	}

	name := t_name.str

	if name == "_" && (flags & ALLOW_UNDERSCORE) != 0 {
		return OK
	}

	// disallow field names
	if len(name) >= 1 && name[0] == '.' {
		PostError("bad %s name: cannot begin with a dot", what)
		return FAILED
	}

	// disallow attributes
	if len(name) >= 1 && name[0] == '@' {
		PostError("bad %s name: cannot begin with a '@'", what)
		return FAILED
	}

	// disallow special keywords
	if len(name) >= 1 && name[0] == '#' {
		PostError("bad %s name: cannot begin with a '#'", what)
		return FAILED
	}

	// already defined?
	if (flags & ALLOW_SHADOW) == 0 {
		if all_defs[name] != nil {
			PostError("bad %s name: '%s' already defined", what, name)
			return FAILED
		}
	}

	// disallow language keywords
	if IsLanguageKeyword(name) {
		PostError("bad %s name: '%s' is a reserved keyword", what, name)
		return FAILED
	}

	// disallow names of intrinsics
	if builtins[name] != nil {
		PostError("bad %s name: '%s' is an intrinsic function", what, name)
		return FAILED
	}

	// disallow operators
	if IsOperatorName(name) {
		PostError("bad %s name: '%s' is a math operator", what, name)
		return FAILED
	}

	// disallow names with certain suffixes
	if len(name) >= 6 && strings.HasSuffix(name, ".size") {
		PostError("bad %s name: '.size' is a reserved suffix", what)
		return FAILED
	}

	return OK
}

func IsLanguageKeyword(name string) bool {
	switch name {
	case
		// directives
		"#public", "#private",
		"const", "type", "extern-type", "end",
		"var", "extern-var", "zero-var", "rom-var", "stack-var",
		"fun", "extern-fun", "inline-fun",

		// statements
		"if", "else", "elif", "endif", "unless",
		"loop", "endloop", "while", "until",
		"call", "tail-call", "jump", "return", "break",
		"conv", "raw-cast", "addr-of", "mem-read", "mem-write",

		// types
		"u8", "u16", "u32", "u64",
		"s8", "s16", "s32", "s64",
		"^", "bool", "void", "no-return", "raw-mem",
		"array", "struct", "union",
		"endarray", "endstruct", "endunion",

		// constants
		"NULL", "FALSE", "TRUE",
		"+INF", "-INF", "SNAN", "QNAN",
		"CPU_BITS", "CPU_ENDIAN", "PTR_SIZE",

		// miscellaneous
		":", "::", "_", "\\", "->", "=>", "...":

		return true
	}

	return false
}

//----------------------------------------------------------------------

func CompileAllConstants() {
	// collect all constant definitions which need to be evaluated.
	// we process the list repeatedly until it shrinks to zero.
	// if it fails to shrink, it means there is a cyclic dependency.

	group := make(map[*ConstDef]bool)

	for _, def := range all_defs {
		if def.kind == DEF_Const {
			con := def.d_const
			if con.expr != nil {
				group[con] = true
			}
		}
	}

	last_count := 999999999
	got_error  := false

	for len(group) > 0 {
		// note that we modify the group as we iterate over it.
		// in Go this is legal, may not be legal in other languages.

		example := ""

		for con, _ := range group {
			ErrorsSetPos(con.expr.pos)
			example = con.name

			switch con.TryEvaluate() {
			case EVAL_OK:
				delete(group, con)

			case EVAL_Error:
				got_error = true

			case EVAL_Unknown:
				// try again next round
			}
		}

		if got_error {
			return
		}

		count := len(group)

		if count == last_count {
			PostError("cyclic dependencies with constants (such as %s)", example)
			return
		}

		last_count = count
	}
}

func (con *ConstDef) TryEvaluate() EvalResult {
	res, t_value := EvalNode(con.expr, nil)
	if res != EVAL_OK {
		return res
	}

	switch t_value.kind {
	case NL_Char:   con.kind = CONST_Char
	case NL_Float:  con.kind = CONST_Float
	case NL_String: con.kind = CONST_String
	case NL_Bool:   con.kind = CONST_Bool
	default:        con.kind = CONST_Int
	}

	con.value = t_value.str
	con.expr  = nil

	return EVAL_OK
}

func (con *ConstDef) MakeLiteral(pos Position) *Node {
	var kind NodeKind

	switch con.kind {
	case CONST_Int:    kind = NL_Integer
	case CONST_Float:  kind = NL_Float
	case CONST_Char:   kind = NL_Char
	case CONST_Bool:   kind = NL_Bool
	case CONST_String: kind = NL_String
	default: panic("bad constant")
	}

	t_lit := NewNode(kind, con.value, pos)
	return t_lit
}
