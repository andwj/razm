// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

/*
   this code contains optimisations which do not depend on the
   target architecture, plus some helper stuff for the back-end
   (architecture-specific) code.
*/

package main

// import "fmt"
import "strconv"

// loop multipliers for LocalInfo.reads and .writes
var outer_factor = 10
var inner_factor = 40

func (fu *FuncDef) MissingLabels() error {
	found := false

	for name, _ := range fu.used_labels {
		if ! fu.labels[name] {
			PostError("missing label: %s", name)
			found = true
		}
	}

	if found {
		return FAILED
	}

	// remove any labels which are unused
	old_body := fu.body
	fu.body = NewNode(NG_Body, "code", old_body.pos)

	for _, t := range old_body.children {
		if t.kind == NH_Label && ! fu.used_labels[t.str] {
			fu.labels[t.str] = false
			continue
		}
		fu.body.Add(t)
	}

	return OK
}

//----------------------------------------------------------------------

type InlineAnalysis struct {
	input   []*FuncDef
	output  []*FuncDef
	seen    map[*FuncDef]bool
	example *FuncDef
}

type InlineResult int
const (
	INRES_OK       InlineResult = 0  // finished, all okay
	INRES_Progress InlineResult = 1  // progress was made
	INRES_Failed   InlineResult = 2  // cycle detected
)

func InlineAllFunctions() {
	lan := new(InlineAnalysis)

	lan.input  = make([]*FuncDef, 0)
	lan.output = make([]*FuncDef, 0)
	lan.seen   = make(map[*FuncDef]bool)

	// collect all function defs
	for _, def := range ordered_defs {
		if def.kind == DEF_Func {
			lan.input = append(lan.input, def.d_func)
		}
	}
	for _, fu := range all_methods {
		lan.input = append(lan.input, fu)
	}

	// determine an ordering so that any inlined function is handled
	// before the function which inlines it.  also detect situations
	// which are impossible to satisfy, for example: A inlines B and
	// B inlines A.

	for {
		res := lan.DoPass()
		if res == INRES_OK {
			break
		} else if res == INRES_Failed {
			ErrorsSetPos(lan.example.body.pos)
			PostError("cyclic inlining of functions (such as %s)", lan.example.name)
			return
		}
	}

	/* we now have a usable ordering */

	for _, fu := range lan.output {
		fu.PerformInlining()
	}
	for _, fu := range lan.input {
		if !fu.inline && !fu.extern {
			fu.PerformInlining()
		}
	}
}

func (lan *InlineAnalysis) DoPass() InlineResult {
	count    := 0
	progress := false

	for _, fu := range lan.input {
		if fu.inline && !lan.seen[fu] {
			count += 1
			lan.example = fu

			if lan.Visit(fu) {
				lan.output = append(lan.output, fu)
				lan.seen[fu] = true
				progress = true
			}
		}
	}

	if count == 0 {
		return INRES_OK
	} else if progress {
		return INRES_Progress
	} else {
		return INRES_Failed
	}
}

func (lan *InlineAnalysis) Visit(fu *FuncDef) bool {
	for _, t := range fu.body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_Call {
			called_func := fu.GetCalledFunc(comp)

			if called_func == nil {
				continue
			}
			if !called_func.inline {
				continue
			}
			if lan.seen[called_func] {
				continue
			}

			// this inline func is not yet "leafy"
			return false
		}
	}

	// this inline func is "leafy"
	return true
}

func (fu *FuncDef) PerformInlining() {
	old_body := fu.body
	fu.body = NewNode(NG_Body, "code", old_body.pos)

	for _, t := range old_body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_Call {
			called_func := fu.GetCalledFunc(comp)

			if called_func != nil && called_func.inline {
				t_new := fu.PerformInlineCall(t, called_func)
				if t_new != nil {
					fu.body.Add(t_new)
				}
				fu.num_inlines += 1
				continue
			}
		}

		fu.body.Add(t)
	}
}

func (fu *FuncDef) PerformInlineCall(t_call *Node, other *FuncDef) *Node {
	comp := t_call.GetComputation()

	suffix := ":" + strconv.Itoa(fu.num_inlines)

	// copy the locals, including parameters
	for _, local := range other.locals {
		fu.CopyInlineLocal(local, suffix)
	}

	// NOTE: no need to update fu.labels or fu.used_labels

	// create assignments for the parameter locals
	for i, par_info := range other.params {
		call_par := comp.children[1 + i]

		var_name := par_info.name + suffix
		var_dest := NewNode(NP_Local, var_name, t_call.pos)
		var_dest.ty = par_info.ty

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(var_dest)
		t_move.Add(call_par)

		fu.body.Add(t_move)
	}

	// return values may need a local to store it
	var ret_local *LocalInfo
	var ret_local_node *Node
	var ret_local_exist bool = false

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// no local needed
	default:
		if t_call.kind == NH_Drop {
			// no local needed
		} else if t_call.kind == NH_Move && t_call.children[0].kind == NP_Local {
			// use the existing local
			ret_local_node = t_call.children[0]
			ret_local = fu.locals[ret_local_node.str]
			ret_local_exist = true
		} else {
			ret_local = fu.NewLocal("__inline" + suffix, other.ty.sub)
			ret_local_node = NewNode(NP_Local, ret_local.name, t_call.pos)
			ret_local_node.ty = ret_local.ty
		}
	}

	need_end := false

	// copy the statements
	for _, t2 := range other.body.children {
		if t2.kind == NH_Return {
			need_end = true
		}

		fu.CopyInlineStatement(t2, suffix, ret_local, t_call.pos)
	}

	if need_end {
		lab_end := NewNode(NH_Label, "__end" + suffix, t_call.pos)
		fu.body.Add(lab_end)
	}

	// copy the original node (NH_Move, NH_Drop, etc...)

	switch other.ty.sub.kind {
	case TYP_Void, TYP_NoReturn:
		// nothing needed
		return nil
	}

	switch t_call.kind {
	case NH_Drop:
		// nothing needed
		return nil

	case NH_Move:
		if ret_local_exist {
			// nothing needed, inlined code writes result to an existing local
			return nil
		}

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(t_call.children[0])
		t_move.Add(ret_local_node)
		return t_move

	case NH_Jump, NH_JumpNot:
		t_jump := NewNode(t_call.kind, t_call.str, t_call.pos)
		t_jump.Add(ret_local_node)
		return t_jump

	case NH_Return:
		t_ret := NewNode(NH_Return, "", t_call.pos)
		t_ret.Add(ret_local_node)
		return t_ret

	default:
		panic("strange node kind in PerformInlineCall")
	}
}

func (fu *FuncDef) CopyInlineLocal(local *LocalInfo, suffix string) {
	new_loc := fu.NewLocal(local.name + suffix, local.ty)
	_ = new_loc
}

func (fu *FuncDef) CopyInlineStatement(t *Node, suffix string, ret_local *LocalInfo, pos Position) {
	if t.kind == NH_Return {
		// special logic for return statements: transfer a needed value
		// to the return local, then jump to the end label.

		if ret_local != nil {
			comp := fu.CopyInlineNode(t.children[0], suffix, pos)

			dest := NewNode(NP_Local, ret_local.name, pos)
			dest.ty = ret_local.ty

			t_move := NewNode(NH_Move, "", pos)
			t_move.Add(dest)
			t_move.Add(comp)

			fu.body.Add(t_move)
		}

		t_jump := NewNode(NH_Jump, "__end" + suffix, pos)
		fu.body.Add(t_jump)
		return
	}

	new_node := fu.CopyInlineNode(t, suffix, pos)
	fu.body.Add(new_node)
}

func (fu *FuncDef) CopyInlineNode(t1 *Node, suffix string, pos Position) *Node {
	t2 := NewNode(t1.kind, t1.str, pos)

	t2.ty     = t1.ty
	t2.scale  = t1.scale
	t2.offset = t1.offset

	// adjust labels and locals with a suffix
	switch t1.kind {
	case NH_Label, NH_Jump, NH_JumpNot, NP_Local:
		t2.str += suffix
	}

	for _, child := range t1.children {
		t2.Add(fu.CopyInlineNode(child, suffix, pos))
	}
	return t2
}

//----------------------------------------------------------------------

// CodePath represents a path of code statements in a function.
// The path may be empty, it represents a function which has not yet
// started execution.
type CodePath struct {
	// the next node to be visited, -1 if finished
	next int

	// nodes in the path (indexes into body)
	visited map[int]bool

	// the names of locals which have been created along this path.
	locals map[string]bool

	// this path produced an error
	failed bool
}

func NewCodePath() *CodePath {
	pt := new(CodePath)
	pt.next    = 0
	pt.visited = make(map[int]bool, 0)
	pt.locals  = make(map[string]bool)
	return pt
}

func (pt *CodePath) Copy() *CodePath {
	pt2 := new(CodePath)
	pt2.next = pt.next

	pt2.visited = make(map[int]bool)
	for idx,_ := range pt.visited {
		pt2.visited[idx] = true
	}
	pt2.locals = make(map[string]bool)
	for name,_ := range pt.locals {
		pt2.locals[name] = true
	}
	return pt2
}

func (pt1 *CodePath) CanMerge(pt2 *CodePath) bool {
	return pt1.next == pt2.next
}

func (pt *CodePath) Merge(old *CodePath) {
	for idx,_ := range old.visited {
		pt.visited[idx] = true
	}

	// only keep locals that are common to both paths
	new_locals := make(map[string]bool)
	for name, _ := range old.locals {
		if pt.locals[name] {
			new_locals[name] = true
		}
	}

	pt.locals = new_locals
	pt.failed = pt.failed || old.failed

	// mark old path as dead
	old.next = -1
}

func (fu *FuncDef) FlowAnalysis() error {
	for _, t := range fu.body.children {
		t.dead = true
	}

	// create initial path
	pt := NewCodePath()

	fu.paths = make([]*CodePath, 0)
	fu.paths = append(fu.paths, pt)

	error_count := 0

	/* process active paths until done */

	for len(fu.paths) > 0 {
		// remove dead paths from the end
		last := len(fu.paths) - 1
		pt   := fu.paths[last]

		if pt.next < 0 {
			if pt.failed {
				error_count += 1
			}
			fu.paths = fu.paths[0:last]
			continue
		}

		// choose the path at earliest node
		for i := 0; i < last; i++ {
			other := fu.paths[i]
			if other.next >= 0 && other.next < pt.next {
				pt = other
			}
		}

		// attempt to merge other paths into this one
		for i := 0; i <= last; i++ {
			other := fu.paths[i]
			if other != pt {
				if pt.CanMerge(other) {
					pt.Merge(other)
				}
			}
		}

		fu.FlowStep(pt)
	}

	if error_count > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) FlowStep(pt *CodePath) {
	// reached end?
	if pt.next >= fu.body.Len() {
		ret_type := fu.ty.sub

		if ret_type.kind != TYP_Void && !fu.missing_ret {
			fu.missing_ret = true
			last := fu.body.Last()
			ErrorsSetPos(last.pos)
			PostError("control reaches end of non-void function")
		}
		pt.next = -1
		return
	}

	// already visited? (i.e. looping back)
	if pt.visited[pt.next] {
		pt.next = -1
		return
	}

	pt.visited[pt.next] = true

	t := fu.body.children[pt.next]
	t.dead = false

	// check for use of uninitialized locals
	err := fu.FlowCheckLocals(t, pt)
	if err != nil {
		pt.failed = true
	}

	// check for creation of locals
	if t.kind == NH_Move {
		if t.children[0].kind == NP_Local {
			name := t.children[0].str
			pt.locals[name] = true
		}
	}

	// handle return statements, or equivalent
	switch t.kind {
	case NH_Return, NH_TailCall:
		pt.next = -1
		return

	case NH_Drop:
		// calling a `no-return` function?
		comp := t.children[0]

		if comp.kind == NC_Call {
			t_func   := comp.children[0]
			ret_type := t_func.ty.sub

			if ret_type.kind == TYP_NoReturn {
				pt.next = -1
				return
			}
		}
	}

	// handle jumps.
	// conditional jumps split the path into two.

	if t.kind == NH_Jump || t.kind == NH_JumpNot {
		dest_idx := fu.FindLabel(t.str)

		// WISH: detect when a jump is not really conditional, especially in
		//       loops where a local begins at zero and stops at a constant.

		if t.Len() > 0 {
			branch := pt.Copy()
			branch.next = pt.next + 1
			fu.paths = append(fu.paths, branch)
		}

		pt.next = dest_idx
		return
	}

	pt.next += 1
}

func (fu *FuncDef) FlowCheckLocals(t *Node, pt *CodePath) error {
	if t.kind == NP_Local {
		// has been created in this path?
		if pt.locals[t.str] {
			return OK
		}
		// parameters don't need a previous write
		local := fu.locals[t.str]
		if local.param_idx >= 0 {
			return OK
		}
		// prevent multiple errors
		if !local.errored {
			local.errored = true

			ErrorsSetPos(t.pos)
			PostError("local '%s' may be used uninitialized", t.str)
		}

		pt.failed = true
		return FAILED
	}

	start := 0
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		start = 1
	}
	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		if fu.FlowCheckLocals(child, pt) != OK {
			return FAILED
		}
	}

	return OK
}

func (fu *FuncDef) DeadCodeRemoval() {
	if Options.no_optim {
		return
	}

	// WISH: detect a jump statement followed by its label
	//       (with nothing but comments in between), remove it.

	for _, t := range fu.body.children {
		if t.dead {
			if t.kind != NH_Comment && t.kind != NH_Label {
				t.kind = NH_Comment
				t.str  = "[ DEAD CODE ]"
			}
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) StrengthReduce() {
	// this optimizes certain nodes to ones which are more
	// efficient but produce the same result.
	// for example: `imul x 8` --> `ishl x 3`

	if Options.no_optim {
		return
	}

	// WISH: evaluate builtins when all args are a literal

	for _, t := range fu.body.children {
		comp := t.GetComputation()
		if comp != nil && comp.kind == NC_Builtin {
			fu.StrengthReduce_force(comp, t)
			fu.StrengthReduce_nofx (comp, t)

			switch comp.str {
			case "imul":
				fu.StrengthReduce_imul(comp,  t)
			case "idivt":
				fu.StrengthReduce_idivt(comp, t)
			case "iremt":
				fu.StrengthReduce_iremt(comp, t)
			case "little-endian", "big-endian":
				fu.StrengthReduce_endian(comp, t)
			case "not":
				fu.StrengthReduce_not(comp, t)
			default:
				fu.StrengthReduce_compare(comp, t)
			}
		}
	}
}

func (fu *FuncDef) StrengthReduce_imul(t, parent *Node) {
	if t.Len() != 2 {
		return
	}

	lhs := t.children[0]
	rhs := t.children[1]

	if rhs.kind != NL_Integer {
		return
	}

	mul, err := strconv.ParseUint(rhs.str, 0, lhs.ty.bits)
	if err != nil {
		return
	}

	// see whether the multiplier is a power of two
	for p := 0; p < lhs.ty.bits; p++ {
		if mul == (uint64(1) << uint64(p)) {
			t.str = "ishl"
			rhs.str = strconv.FormatUint(uint64(p), 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_idivt(t, parent *Node) {
	if t.Len() != 2 {
		return
	}

	lhs := t.children[0]
	rhs := t.children[1]

	// only unsigned types can be optimized to a shift, since `idivt` is a
	// truncating division but a right shift is a floored division.
	if !lhs.ty.unsigned {
		return
	}
	if rhs.kind != NL_Integer {
		return
	}

	div, err := strconv.ParseUint(rhs.str, 0, lhs.ty.bits)
	if err != nil {
		return
	}

	// see whether the divisor is a power of two
	for p := 0; p < lhs.ty.bits; p++ {
		if div == (uint64(1) << uint64(p)) {
			t.str = "ishr"
			rhs.str = strconv.FormatUint(uint64(p), 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_iremt(t, parent *Node) {
	// can only replace a `iremt` with a masking `iand` for unsigned
	// types, since signed types always return a value with same sign
	// as the dividend.

	if t.Len() != 2 {
		return
	}

	lhs := t.children[0]
	rhs := t.children[1]

	if !lhs.ty.unsigned {
		return
	}
	if rhs.kind != NL_Integer {
		return
	}

	div, err := strconv.ParseUint(rhs.str, 0, lhs.ty.bits)
	if err != nil {
		return
	}

	// see whether the divisor is a power of two
	for p := 0; p < lhs.ty.bits; p++ {
		if div == (uint64(1) << uint64(p)) {
			t.str = "iand"
			rhs.str = strconv.FormatUint(div - 1, 10)
			return
		}
	}
}

func (fu *FuncDef) StrengthReduce_compare(t, parent *Node) {
	if t.Len() != 2 {
		return
	}

	lhs := t.children[0]
	rhs := t.children[1]

	bu_name := t.str

	switch bu_name {
	case "gt?":
		if IsLiteralZero(rhs) {
			t.str = "pos?"
			t.children = t.children[0:1]
		}

	case "lt?":
		if IsLiteralZero(rhs) {
			t.str = "neg?"
			t.children = t.children[0:1]
		}

	case "eq?":
		if IsLiteralZero(rhs) {
			t.str = "zero?"
			t.children = t.children[0:1]

		} else if IsLiteralZero(lhs) {
			t.str = "zero?"
			t.children    = t.children[0:1]
			t.children[0] = rhs

		} else if IsLiteralTrue(rhs) {
			fu.ReplaceBuiltin(parent, lhs)

		} else if IsLiteralTrue(lhs) {
			fu.ReplaceBuiltin(parent, rhs)

		} else if IsLiteralFalse(rhs) {
			t.str = "not"
			t.children = t.children[0:1]

		} else if IsLiteralFalse(lhs) {
			t.str = "not"
			t.children = t.children[0:1]
			t.children[0] = rhs
		}

	case "ne?":
		if IsLiteralZero(rhs) {
			t.str = "some?"
			t.children = t.children[0:2]

		} else if IsLiteralZero(lhs) {
			t.str = "some?"
			t.children    = t.children[0:1]
			t.children[0] = rhs

		} else if IsLiteralFalse(rhs) {
			fu.ReplaceBuiltin(parent, lhs)

		} else if IsLiteralFalse(lhs) {
			fu.ReplaceBuiltin(parent, rhs)

		} else if IsLiteralTrue(rhs) {
			t.str = "not"
			t.children = t.children[0:1]

		} else if IsLiteralTrue(lhs) {
			t.str = "not"
			t.children = t.children[0:1]
			t.children[0] = rhs
		}
	}
}

func (fu *FuncDef) StrengthReduce_endian(t, parent *Node) {
	arch_big := arch.BigEndian()
	node_big := (t.str == "big-endian")

	if arch_big == node_big {
		fu.ReplaceBuiltin(parent, t.children[0])
	}
}

func (fu *FuncDef) StrengthReduce_not(t, parent *Node) {
	child := t.children[0]

	if IsLiteralFalse(child) {
		// not FALSE --> TRUE
		child.str = "1"
		fu.ReplaceBuiltin(parent, child)

	} else if IsLiteralTrue(child) {
		// not TRUE --> FALSE
		child.str = "0"
		fu.ReplaceBuiltin(parent, child)
	}
}

func (fu *FuncDef) StrengthReduce_force(t, parent *Node) {
	// this handle cases where the result is forced to a known value.
	// examples: `imul` with a literal zero, `or` with literal TRUE.

	bu_name := t.str

	count := t.Len()
	switch bu_name {
	case "idivt", "ishl", "ishr":
		count = 1
	}

	for i := 0; i < count; i++ {
		child := t.children[i]

		if fu.TermForcesResult(bu_name, child) {
			fu.ReplaceBuiltin(parent, child)
			return
		}
	}
}

func (fu *FuncDef) TermForcesResult(bu_name string, term *Node) bool {
	switch bu_name {
	case "imul", "idivt", "iand", "ishl", "ishr":
		return IsLiteralZero(term)

	case "and":
		return IsLiteralFalse(term)

	case "or":
		return IsLiteralTrue(term)
	}

	return false
}

func (fu *FuncDef) StrengthReduce_nofx(t, parent *Node) {
	bu_name := t.str

	start := 0
	switch bu_name {
	case "isub", "idivt", "ishl", "ishr", "pdiff":
		start = 1
	}

	// remove any term which would cause no effect on the result.
	// examples: a literal zero with `iadd`, a literal one with `imul`.

	// if there would only be a single term left, the whole NC_Builtin
	// will be replaced by the remaining term.

	for idx := t.Len()-1; idx >= start; idx-- {
		child := t.children[idx]

		if fu.TermWithNoEffect(bu_name, child) {
			t.Remove(idx)

			if t.Len() < 2 {
				fu.ReplaceBuiltin(parent, t.children[0])
				return
			}
		}
	}
}

func (fu *FuncDef) TermWithNoEffect(bu_name string, term *Node) bool {
	switch bu_name {
	case "iadd", "isub", "padd", "psub", "ior", "ixor", "ishl", "ishr":
		return IsLiteralZero(term)

	case "imul", "idivt":
		return IsLiteralOne(term)

	case "or":
		return IsLiteralFalse(term)

	case "and":
		return IsLiteralTrue(term)

	case "pdiff":
		return term.kind == NL_Null
	}

	return false
}

func (fu *FuncDef) ReplaceBuiltin(parent *Node, val *Node) {
	if parent.kind == NH_Move {
		parent.children[1] = val
	} else {
		// NH_Drop, NH_Jump or NH_Return
		parent.children[0] = val
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) UnusedLocals() {
	if Options.no_optim {
		return
	}

	// keep trying until no more changes
	for fu.UnusedLocalPass() {
	}
}

func (fu *FuncDef) UnusedLocalPass() bool {
	changed := false

	// determine # of reads and writes of each local
	fu.LiveRanges(true)

	// we remove nodes writing to an unused local
	old_body := fu.body
	fu.body = NewNode(NG_Body, "code", old_body.pos)

	removals := make(map[string]bool)

	for node_idx, t := range old_body.children {
		if t.kind != NH_Move {
			fu.body.Add(t)
			continue
		}

		dest := t.children[0]
		comp := t.children[1]

		kill_node := false

		if dest.kind == NP_Local {
			local := fu.locals[dest.str]

			if local.param_idx >= 0 {
				// ignore parameters here

			} else if local.reads == 0 {
				kill_node = true

			} else if local.writes == 1 && t.children[1].IsLiteral() {
				// replace this never-modified local with the constant
				lit := t.children[1]
				for k := local.live_start; k <= local.live_end; k++ {
					if k != node_idx {
						fu.PropagateConstantInNode(local, old_body.children[k], lit)
					}
				}
				local.reads = 0
				kill_node = true
			}

			// leave a tombstone
			if kill_node {
				if !removals[dest.str] {
					removals[dest.str] = true

					msg   := "[ " + dest.str + " OPTIMISED AWAY ]"
					cmt   := NewNode(NH_Comment, msg, t.pos)
					blank := NewNode(NH_Comment, "",  t.pos)

					fu.body.Add(cmt)
					fu.body.Add(blank)
				}
			}
		}

		// certain nodes MUST be kept, these become NH_Drop
		if kill_node {
			changed = true

			switch comp.kind {
			case NC_MemRead, NC_Call:
				t2 := NewNode(NH_Drop, "", t.pos)
				t2.Add(comp)
				fu.body.Add(t2)

			case NC_Builtin:
				bu := builtins[comp.str]
				if (bu.flags & BF_SIDE_FX) != 0 {
					t2 := NewNode(NH_Drop, "", t.pos)
					t2.Add(comp)
					fu.body.Add(t2)
				}

			default:
				// remove the node (by doing nothing)
			}
		} else {
			fu.body.Add(t)
		}
	}

	// actually remove the LocalInfo data
	for name, local := range fu.locals {
		if local.reads == 0 && local.param_idx < 0 {
			delete(fu.locals, name)
			changed = true
		}
	}

	return changed
}

func (fu *FuncDef) LiveRanges(ignore_updates bool) {
	for _, local := range fu.locals {
		local.reads  = 0
		local.writes = 0
		local.live_start = -1
		local.live_end   = -1
	}

	for use_pos, t := range fu.body.children {
		loop_factor := 1
		if t.loop == 1 {
			loop_factor = outer_factor
		} else if t.loop >= 2 {
			loop_factor = inner_factor
		}

		start := 0
		skip_local := ""

		if t.kind == NH_Move {
			dest := t.children[0]

			if dest.kind == NP_Local {
				local := fu.locals[dest.str]
				local.writes += loop_factor

				fu.UpdateLiveRange(local, use_pos)

				// skip this destination when marking reads
				start = 1

				// when determining unused locals, a statement like `x = iadd x 1`
				// should not be considered as reading from ("using") the local x.
				// however function calls (etc) must not be removed.
				if ignore_updates {
					comp := t.children[1]
					if !(comp.kind == NC_Call || comp.kind == NC_MemRead) {
						skip_local = local.name
					}
				}
			}
		}

		if t.kind == NH_Swap {
			for _, child := range t.children {
				if child.kind == NP_Local {
					local := fu.locals[child.str]
					local.writes += loop_factor

					fu.UpdateLiveRange(local, use_pos)
				}
			}
		}

		fu.LiveRangesInNode(t, start, use_pos, loop_factor, skip_local)
	}

	// mark all parameters to be live at first node
	for _, par := range fu.params {
		par.live_start = 0
		if par.live_end < 0 {
			par.live_end = 0
		}
	}
}

func (fu *FuncDef) LiveRangesInNode(t *Node, start, use_pos, loop_factor int, skip_local string) {
	if t.kind == NP_Local {
		local := fu.locals[t.str]
		if local.name != skip_local {
			local.reads += loop_factor
		}
		fu.UpdateLiveRange(local, use_pos)
	}

	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		fu.LiveRangesInNode(child, 0, use_pos, loop_factor, skip_local)
	}
}

func (fu *FuncDef) UpdateLiveRange(local *LocalInfo, use_pos int) {
	if local.live_start < 0 {
		local.live_start = use_pos
		local.live_end   = use_pos

	} else if use_pos < local.live_start {
		local.live_start = use_pos

	} else if use_pos > local.live_end {
		local.live_end = use_pos
	}
}

func (fu *FuncDef) ExtendLiveRanges() {
	// this detects whether a local's last usage is inside a loop,
	// which means the live range may need to extend to the end of
	// that loop unless the local only exists solely in that loop.
	//
	// similarly, if a local is created inside a loop but used by code
	// beyond the end of the loop, its live range needs to be extended
	// to the start of that loop.

	for _, local := range fu.locals {
		if local.live_end >= 0 {
			new_end   := fu.TryExtendForward(local)
			new_start := fu.TryExtendBackward(local)

			local.live_start = new_start
			local.live_end   = new_end
		}
	}
}

func (fu *FuncDef) TryExtendForward(local *LocalInfo) int {
	loop := fu.body.children[local.live_end].loop

	// when `loop` is two or more, the local is technically in multiple
	// loops at the same time, and each loop may need to extend it.

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_end, loop)
		end   := fu.FindLoopEnd  (local.live_end, loop)

		// is the local wholly contained in this loop?
		if local.param_idx < 0 && local.live_start >= start {
			break
		}

		// WISH: if we are 100% sure that this local is written to near start
		//       of loop (without reading it), with no jumps/labels in between,
		//       then no need to extend the local for this loop.

		// extend the local
		local.live_end = end
	}

	return local.live_end
}

func (fu *FuncDef) TryExtendBackward(local *LocalInfo) int {
	if local.param_idx >= 0 {
		return 0
	}

	loop := fu.body.children[local.live_start].loop

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_start, loop)
		end   := fu.FindLoopEnd  (local.live_start, loop)

		// is the local wholly contained in this loop?
		if local.live_end <= end {
			break
		}

		// extend the local
		local.live_start = start
	}

	return local.live_start
}

func (fu *FuncDef) FindLoopStart(pos int, level int) int {
	for pos > 0 {
		t := fu.body.children[pos-1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos -= 1
	}
	return pos
}

func (fu *FuncDef) FindLoopEnd(pos int, level int) int {
	for pos+1 < fu.body.Len() {
		t := fu.body.children[pos+1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos += 1
	}
	return pos
}

func (fu *FuncDef) NodeReadsLocal(t *Node, local *LocalInfo) bool {
	if t.kind == NP_Local {
		return t.str == local.name
	}

	start := 0
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		start = 1
	}
	for pos := start; pos < t.Len(); pos++ {
		child := t.children[pos]
		if fu.NodeReadsLocal(child, local) {
			return true
		}
	}

	return false
}

func (fu *FuncDef) NodeWritesLocal(t *Node, local *LocalInfo) bool {
	if t.kind == NH_Move {
		dest := t.children[0]
		if dest.kind == NP_Local && dest.str == local.name {
			return true
		}
	}
	return false
}

func (fu *FuncDef) PropagateConstantInNode(local *LocalInfo, t, lit *Node) {
	for i, child := range t.children {
		fu.PropagateConstantInNode(local, child, lit)

		if child.kind == NP_Local && child.str == local.name {
			// copy the literal
			lit2 := NewNode(lit.kind, lit.str, lit.pos)
			lit2.ty = lit.ty

			t.children[i] = lit2
		}
	}
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectLoops() {
	level := 0

	for {
		found := false

		for idx, line := range fu.body.children {
			if line.kind == NH_Jump || line.kind == NH_JumpNot {
				if fu.MarkLoop(idx, line.str, level) {
					found = true
				}
			}
		}

		if !found {
			return
		}

		level += 1
	}
}

func (fu *FuncDef) MarkLoop(idx int, label string, level int) bool {
	start := fu.FindLabel(label)

	if start >= idx {
		return false
	}

	children := fu.body.children

	// check that the loop is truly "inside" another one.
	next := idx
	if level > 0 {
		next += 1
	}

	if next < len(children) &&
		children[start+1].loop >= level &&
		children[next].loop >= level {

		// do not mark the label itself, to prevent two loops in
		// a row looking like one big loop.
		for k := start+1; k <= idx; k++ {
			fu.body.children[k].loop = level+1
		}
		return true
	}

	return false
}

func (fu *FuncDef) FindLabel(label string) int {
	for idx, line := range fu.body.children {
		if line.kind == NH_Label && line.str == label {
			return idx
		}
	}

	panic("no such label: " + label)
}

//----------------------------------------------------------------------

func (fu *FuncDef) CostOfLocal(local *LocalInfo, c_create, c_write, c_read int) int64 {
	// compute the cost of keeping this local on the stack (as compared
	// to keeping it in a register).  the result is an estimation based
	// on the number of times the local is read and written, taking loops
	// into account.
	//
	// the `c_xxx` parameters are cost values, which should be >= 0.
	// -  c_create is the cost of creation
	// -  c_write  is the cost of all writes (including the first)
	// -  c_read   is the cost of all reads.

	var cost int64

	if local.param_idx >= 0 {
		if local.abi_reg != REG_ON_STACK {
			// this parameter begins in a reg, but gets written to the stack.
			cost += int64(c_create)
		}
	} else {
		if local.writes > 0 {
			cost += int64(c_create)
		}
	}

	cost += int64(c_write) * int64(local.writes)
	cost += int64(c_read)  * int64(local.reads)

	// slight bonus to temporary vars
	if len(local.name) > 2 && local.name[0] == '_' && local.name[1] == '_' {
		cost += 1
	}

	return cost
}
