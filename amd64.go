// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "fmt"
import "strconv"
import "strings"
import "sort"

// AMD64_Arch is an implementation of the Architecture interface
// which generate output assembly for the AMD64 cpu architecture.
type AMD64_Arch struct {
	cur_section string

	// the ABI is either "win" or "sysv"
	abi string

	// floating point constants get stored in the .text section,
	// and these remember the offset into an array.
	f32_consts map[string]int
	f64_consts map[string]int

	// keep track of all string constants.
	utf8_strings  map[string]int
	utf16_strings map[string]int
	utf32_strings map[string]int

	// some optimisation constants
	c_create int
	c_write  int
	c_read   int

	// floating point work registers
	workf1 Register
	workf2 Register

	// compiling info for a single function....
	cur_func *FuncDef

	extra_regs *RegisterSet

	ordered_locals []*LocalInfo  // sorted by appearance
	costed_locals  []*LocalInfo  // sorted by cost

	stack_slots int
	gc_slots    int

	need_end    bool
	new_labels  int
}

const (
	RAX Register = 1 + iota
	RBX
	RCX
	RDX

	RBP
	RSI
	RDI
	RSP

	R8
	R9
	R10
	R11
	R12
	R13
	R14
	R15

	XMM0
	XMM1
	XMM2
	XMM3
	XMM4
	XMM5
	XMM6
	XMM7

	XMM8
	XMM9
	XMM10
	XMM11
	XMM12
	XMM13
	XMM14
	XMM15
)

//----------------------------------------------------------------------

func NewAMD64Arch() *AMD64_Arch {
	arch := new(AMD64_Arch)
	arch.f32_consts = make(map[string]int)
	arch.f64_consts = make(map[string]int)

	arch.utf8_strings  = make(map[string]int)
	arch.utf16_strings = make(map[string]int)
	arch.utf32_strings = make(map[string]int)

	if Options.os == "windows" {
		arch.abi = "win"
	} else {
		arch.abi = "sysv"
	}

	if arch.abi == "win" {
		// Win64 ABI cannot use XMM14/15 since they are in the set of
		// callee-saved registers (non-clobbered by function calls).
		arch.workf1 = XMM4
		arch.workf2 = XMM5
	} else {
		// SysV ABI cannot use XMM4/5 since they may be used to pass
		// parameters in a function call.
		arch.workf1 = XMM14
		arch.workf2 = XMM15
	}

	arch.c_create = 200
	arch.c_write  = 24
	arch.c_read   = 10

	return arch
}

func (arch *AMD64_Arch) OutputExt() string {
	return ".s"
}

func (arch *AMD64_Arch) Begin() {
	// nothing needed
}

func (arch *AMD64_Arch) Finish() {
	arch.Section("text")

	arch.WriteFloats()
	arch.WriteStrings()

	// this makes viewing with vim easier
	OutLine("")
	OutLine("; vi:ts=10:sw=10")
}

// Section outputs assembly to set the current section.
// The parameter must be one of "text", "data" and "bss" for now.
func (arch *AMD64_Arch) Section(sec string) {
	if arch.cur_section != sec {
		arch.cur_section = sec

		OutLine("section ." + sec)
		OutLine("")
	}
}

func (arch *AMD64_Arch) BigEndian() bool {
	return false
}

func (arch *AMD64_Arch) RegisterBits() int {
	return 64
}

func (arch *AMD64_Arch) RegName(reg Register) string {
	switch reg {
	case RAX: return "rax"
	case RBX: return "rbx"
	case RCX: return "rcx"
	case RDX: return "rdx"
	case RBP: return "rbp"
	case RSI: return "rsi"
	case RDI: return "rdi"
	case RSP: return "rsp"

	case R8:  return "r8"
	case R9:  return "r9"
	case R10: return "r10"
	case R11: return "r11"
	case R12: return "r12"
	case R13: return "r13"
	case R14: return "r14"
	case R15: return "r15"

	case XMM0: return "xmm0"
	case XMM1: return "xmm1"
	case XMM2: return "xmm2"
	case XMM3: return "xmm3"
	case XMM4: return "xmm4"
	case XMM5: return "xmm5"
	case XMM6: return "xmm6"
	case XMM7: return "xmm7"

	case XMM8:  return "xmm8"
	case XMM9:  return "xmm9"
	case XMM10: return "xmm10"
	case XMM11: return "xmm11"
	case XMM12: return "xmm12"
	case XMM13: return "xmm13"
	case XMM14: return "xmm14"
	case XMM15: return "xmm15"

	default:
		panic("RegName with bad register")
	}
}

func (arch *AMD64_Arch) RegName8(reg Register) string {
	switch reg {
	case RAX: return "al"
	case RBX: return "bl"
	case RCX: return "cl"
	case RDX: return "dl"
	case RBP: return "bpl"
	case RSI: return "sil"
	case RDI: return "dil"
	case RSP: return "spl"

	case R8:  return "r8b"
	case R9:  return "r9b"
	case R10: return "r10b"
	case R11: return "r11b"
	case R12: return "r12b"
	case R13: return "r13b"
	case R14: return "r14b"
	case R15: return "r15b"

	default:
		panic("RegName8 with bad register")
	}
}

func (arch *AMD64_Arch) RegName16(reg Register) string {
	switch reg {
	case RAX: return "ax"
	case RBX: return "bx"
	case RCX: return "cx"
	case RDX: return "dx"
	case RBP: return "bp"
	case RSI: return "si"
	case RDI: return "di"
	case RSP: return "sp"

	case R8:  return "r8w"
	case R9:  return "r9w"
	case R10: return "r10w"
	case R11: return "r11w"
	case R12: return "r12w"
	case R13: return "r13w"
	case R14: return "r14w"
	case R15: return "r15w"

	default:
		panic("RegName16 with bad register")
	}
}

func (arch *AMD64_Arch) RegName32(reg Register) string {
	switch reg {
	case RAX: return "eax"
	case RBX: return "ebx"
	case RCX: return "ecx"
	case RDX: return "edx"
	case RBP: return "ebp"
	case RSI: return "esi"
	case RDI: return "edi"
	case RSP: return "esp"

	case R8:  return "r8d"
	case R9:  return "r9d"
	case R10: return "r10d"
	case R11: return "r11d"
	case R12: return "r12d"
	case R13: return "r13d"
	case R14: return "r14d"
	case R15: return "r15d"

	default:
		// the floating point registers stay the same
		return arch.RegName(reg)
	}
}

func (arch *AMD64_Arch) RegNameSize(reg Register, bits int) string {
	switch bits {
	case 8:  return arch.RegName8(reg)
	case 16: return arch.RegName16(reg)
	case 32: return arch.RegName32(reg)
	default: return arch.RegName(reg)
	}
}

func (arch *AMD64_Arch) SizePrefix(bits int) string {
	switch bits {
	case 8:  return "byte "
	case 16: return "word "
	case 32: return "dword "
	default: return "qword "
	}
}

func (arch *AMD64_Arch) ParameterRegs(fun_type *Type) []Register {
	list := make([]Register, 0)

	i_num := 0
	f_num := 0

	for n, par := range fun_type.param {
		is_float := (par.ty.kind == TYP_Float)
		is_int   := !is_float

		reg := REG_ON_STACK

		if arch.abi == "win" {
			// Win64 ABI

			if n < 4 && is_float {
				reg = XMM0 + Register(n)

			} else if n < 4 && is_int {
				switch n {
				case 0: reg = RCX
				case 1: reg = RDX
				case 2: reg = R8
				case 3: reg = R9
				}
			}

		} else {
			// System V AMD64 ABI -- for BSDs, Linux, MacOS

			if is_float && f_num < 8 {
				reg = XMM0 + Register(f_num)
				f_num += 1

			} else if is_int && i_num < 6 {
				switch i_num {
				case 0: reg = RDI
				case 1: reg = RSI
				case 2: reg = RDX
				case 3: reg = RCX
				case 4: reg = R8
				case 5: reg = R9
				}
				i_num += 1
			}
		}

		list = append(list, reg)
	}

	return list
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) EncodeName(name string) string {
	// encode identifiers to be compatible with NASM:
	//   1. ASCII letters, digits and the underscore stay the same
	//   2. escape '-' as "?_"
	//   3. escape ASCII symbols/punctuation as "?XX" (two hex digits)
	//   4. escape unicode characters as "?uXXXX" or "?vXXXXXX"
	//   5. if solely ASCII, prefix with a '$' (which NASM removes)

	var sb strings.Builder
	has_escape := false

	for _, ch := range []rune(name) {
		if ch == '_' || ('A' <= ch && ch <= 'Z') || ('a' <= ch && ch <= 'z') || ('0' <= ch && ch <= '9') {
			sb.WriteByte(byte(ch))
		} else if ch > 0xFFFF {
			fmt.Fprintf(&sb, "?v%06X", ch)
			has_escape = true
		} else if ch >= 0x7F {
			fmt.Fprintf(&sb, "?u%04X", ch)
			has_escape = true
		} else if ch == '-' {
			sb.WriteString("?_")
			has_escape = true
		} else {
			fmt.Fprintf(&sb, "?%02X", ch)
			has_escape = true
		}
	}

	// prevent words clashing with assembly instructions/registers/etc
	if !has_escape {
		return "$" + sb.String()
	}

	return sb.String()
}

func (arch *AMD64_Arch) EncodeLabel(name string) string {
	name = arch.EncodeName(name)

	if name[0] == '$' {
		name = name[1:]
	}
	return "." + name
}

func (arch *AMD64_Arch) EncodeStack(r_name string, offset int) string {
	if offset == 0 {
		return fmt.Sprintf("[%s]", r_name)
	} else if offset < 0 {
		return fmt.Sprintf("[%s-%d]", r_name, 0 - offset)
	} else {
		return fmt.Sprintf("[%s+%d]", r_name, offset)
	}
}

func (arch *AMD64_Arch) EncodeMethodName(name string, ty *Type) string {
	def := ty.def
	if def == nil {
		panic("no TypeDef for method")
	}

	name = name[1:] // omit the dot

	return arch.EncodeName(def.name + ":" + name)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CompileVar(vdef *VarDef) {
	out_name := arch.EncodeName(vdef.name)

	if vdef.extern {
		OutLine("extern %s", out_name)
		OutLine("")
		return
	}

	if vdef.zeroed {
		arch.Section("bss")
	} else if vdef.rom {
		arch.Section("text")
	} else {
		arch.Section("data")
	}

	if vdef.public {
		OutLine("global %s", out_name)
	}

	OutLine("%s:", out_name)

	if vdef.zeroed {
		size := vdef.ty.size
		if size < 1 { size = 1 }

		OutIns("resb", "%d", size)
		OutIns("alignb", "16")
		OutLine("")
		return
	}

	ErrorsSetPos(vdef.raw_type.pos)
	ErrorsSetGlobal(vdef.name)

	arch.GenerateData(vdef.expr, vdef.ty)

	OutIns("align", "16")
	OutLine("")
}

func (arch *AMD64_Arch) GenerateData(data *Node, ty *Type) {
	// this handles elided arrays, structs and unions
	if data.kind == NL_Zeroes {
		arch.GenerateZeroes(ty.size)
		return
	}

	switch ty.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		arch.GenerateSimple(data, ty)

	case TYP_Array:
		arch.GenerateArray(data, ty)

	case TYP_Struct:
		arch.GenerateStruct(data, ty)

	case TYP_Union:
		arch.GenerateUnion(data, ty)

	default:
		panic("weird data: " + ty.SimpleName())
	}
}

func (arch *AMD64_Arch) GenerateSimple(data *Node, ty *Type) {
	keyword := "dq"
	switch ty.bits {
	case  8: keyword = "db"
	case 16: keyword = "dw"
	case 32: keyword = "dd"
	}

	str_val := arch.SimpleDataValue(data)

	OutIns(keyword, "%s", str_val)
}

func (arch *AMD64_Arch) GenerateArray(data *Node, arr_type *Type) {
	elem_type := arr_type.sub

	OutLine("\t; array [%d]", arr_type.elems)

	switch elem_type.kind {
	case TYP_Int, TYP_Float, TYP_Pointer, TYP_Bool:
		arch.GenerateSimpleArray(data, arr_type)
		return
	}

	for _, elem := range data.children {
		// OutLine("\t; index %d", i)
		arch.GenerateData(elem, elem_type)
	}
}

func (arch *AMD64_Arch) GenerateSimpleArray(data *Node, arr_type *Type) {
	elem_type := arr_type.sub
	_ = elem_type

	keyword := "dq"
	switch elem_type.bits {
	case  8: keyword = "db"
	case 16: keyword = "dw"
	case 32: keyword = "dd"
	}

	line := ""

	for _, elem := range data.children {
		if len(line) > 60 {
			OutIns(keyword, "%s", line)
			line = ""
		}
		if len(line) > 0 {
			line += ","
		}
		line += arch.SimpleDataValue(elem)
	}

	if len(line) > 0 {
		OutIns(keyword, "%s", line)
	}
}

func (arch *AMD64_Arch) GenerateStruct(data *Node, struct_type *Type) {
	OutLine("\t; struct")

	for i, elem := range data.children {
		elem_type := struct_type.param[i].ty
		arch.GenerateData(elem, elem_type)
	}
}

func (arch *AMD64_Arch) GenerateUnion(data *Node, union_type *Type) {
	// node kind is a ND_Union with a single data element
	field_name := data.str
	field_idx  := union_type.FindParam(field_name)

	if field_idx < 0 {
		panic("union field not found")
	}
	field_type := union_type.param[field_idx].ty

	OutLine("\t; union %s", field_name)

	arch.GenerateData(data.children[0], field_type)
}

func (arch *AMD64_Arch) GenerateZeroes(count int) {
	if count > 0 {
		num_str := strconv.Itoa(count)
		OutIns("times " + num_str, "db 0")
	}
}

func (arch *AMD64_Arch) SimpleDataValue(elem *Node) string {
	switch elem.kind {
	case NL_Null:
		return "0"

	case NL_Integer, NL_Float, NL_Char, NL_Bool:
		return elem.str

	case NL_FltSpec:
		switch elem.str {
		case "+INF": return "__Infinity__"
		case "-INF": return "-__Infinity__"
		case "QNAN": return "__QNaN__"
		case "SNAN": return "__SNaN__"
		default:
			panic("unknown NL_FltSpec literal")
		}

	case NL_String:
		if elem.ty == nil {
			panic("string with no type")
		}

		switch elem.ty.sub.bits {
		case 8:  return arch.AddStringUTF8(elem.str)
		case 16: return arch.AddStringUTF16(elem.str)
		case 32: return arch.AddStringUTF32(elem.str)
		default:
			panic("string with odd type (expected ^u8 or so)")
		}

	case NP_GlobPtr:
		return arch.EncodeName(elem.str)

	default:
		panic("odd node to SimpleDataValue: " + elem.String())
	}
}

func (arch *AMD64_Arch) WriteFloats() {
	if len(arch.f32_consts) > 0 {
		values := make([]string, len(arch.f32_consts))
		for v, idx := range arch.f32_consts {
			values[idx] = v
		}

		OutLine("__float32_values:")

		for _, v := range values {
			OutIns("dd", "%s", v)
		}
		OutLine("")
	}

	if len(arch.f64_consts) > 0 {
		values := make([]string, len(arch.f64_consts))
		for v, idx := range arch.f64_consts {
			values[idx] = v
		}

		OutLine("__float64_values:")

		for _, v := range values {
			OutIns("dq", "%s", v)
		}
		OutLine("")
	}
}

func (arch *AMD64_Arch) WriteStrings() {
	// collect the strings to ensure the right order
	values8  := make([]string, len(arch.utf8_strings))
	values16 := make([]string, len(arch.utf16_strings))
	values32 := make([]string, len(arch.utf32_strings))

	total := len(values8) + len(values16) + len(values32)

	for v, idx := range arch.utf8_strings {
		values8[idx] = v
	}
	for v, idx := range arch.utf16_strings {
		values16[idx] = v
	}
	for v, idx := range arch.utf32_strings {
		values32[idx] = v
	}

	if total > 0 {
		OutIns("align", "4")
		OutLine("")
	}

	for idx, s := range values32 {
		OutLine("__utf32_str%d:", idx)
		arch.WriteStringUTF32(s)
		OutLine("")
	}
	for idx, s := range values16 {
		OutLine("__utf16_str%d:", idx)
		arch.WriteStringUTF16(s)
		OutLine("")
	}
	for idx, s := range values8 {
		OutLine("__utf8_str%d:", idx)
		arch.WriteStringUTF8(s)
		OutLine("")
	}
}

func (arch *AMD64_Arch) WriteStringUTF8(s string) {
	line := ""

	for pos := 0; pos < len(s); pos++ {
		if len(line) > 60 {
			OutIns("db", "%s", line)
			line = ""
		}

		var ch byte = s[pos]

		line = arch.AppendChar(line, rune(ch), false)
	}

	// ensure string is NUL terminated
	if len(line) > 0 {
		line += ","
	}
	line += "0"

	OutIns("db", "%s", line)
}

func (arch *AMD64_Arch) WriteStringUTF16(s string) {
	line := ""

	for _, ch := range s {
		if len(line) > 60 {
			OutIns("dw", "%s", line)
			line = ""
		}

		line = arch.AppendChar(line, ch, true)
	}

	// ensure string is NUL terminated
	if len(line) > 0 {
		line += ","
	}
	line += "0"

	OutIns("dw", "%s", line)
}

func (arch *AMD64_Arch) WriteStringUTF32(s string) {
	line := ""

	for _, ch := range s {
		if len(line) > 60 {
			OutIns("dd", "%s", line)
			line = ""
		}

		line = arch.AppendChar(line, ch, false)
	}

	// ensure string is NUL terminated
	if len(line) > 0 {
		line += ","
	}
	line += "0"

	OutIns("dd", "%s", line)
}

func (arch *AMD64_Arch) AppendChar(line string, ch rune, utf16 bool) string {
	if len(line) > 0 {
		line += ","
	}

	// for UTF-16 we ignore any surrogate values (U+D800 to U+DFFF) in
	// the input string -- not much else we can do here.

	if 32 <= ch && ch <= 126 && ch != '\'' && ch != '\\' {
		line += fmt.Sprintf("'%c'", ch)

	} else if utf16 && (ch > 0xFFFF) {
		ch -= 0x10000
		high := 0xD800 + ((ch >> 10) & 0x03FF)
		low  := 0xDC00 + ( ch        & 0x03FF)
		line += fmt.Sprintf("0x%x,0x%x", high, low)

	} else if ch >= 128 {
		line += fmt.Sprintf("0x%x", ch)

	} else {
		line += fmt.Sprintf("%d", ch)
	}

	return line
}

func (arch *AMD64_Arch) AddFloat32(s string) string {
	idx, exist := arch.f32_consts[s]
	if !exist {
		idx = len(arch.f32_consts)
		arch.f32_consts[s] = idx
	}
	return "[rel __float32_values+" + strconv.Itoa(4 * idx) + "]"
}

func (arch *AMD64_Arch) AddFloat64(s string) string {
	idx, exist := arch.f64_consts[s]
	if !exist {
		idx = len(arch.f64_consts)
		arch.f64_consts[s] = idx
	}
	return "[rel __float64_values+" + strconv.Itoa(8 * idx) + "]"
}

func (arch *AMD64_Arch) AddStringUTF8(s string) string {
	idx, exist := arch.utf8_strings[s]
	if !exist {
		idx = len(arch.utf8_strings)
		arch.utf8_strings[s] = idx
	}
	return "__utf8_str" + strconv.Itoa(idx)
}

func (arch *AMD64_Arch) AddStringUTF16(s string) string {
	idx, exist := arch.utf16_strings[s]
	if !exist {
		idx = len(arch.utf16_strings)
		arch.utf16_strings[s] = idx
	}
	return "__utf16_str" + strconv.Itoa(idx)
}

func (arch *AMD64_Arch) AddStringUTF32(s string) string {
	idx, exist := arch.utf32_strings[s]
	if !exist {
		idx = len(arch.utf32_strings)
		arch.utf32_strings[s] = idx
	}
	return "__utf32_str" + strconv.Itoa(idx)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CompileFunction(fu *FuncDef) {
	// inline functions never exist in the output assembly
	if fu.inline {
		return
	}

	arch.cur_func    = fu
	arch.extra_regs  = NewRegisterSet()
	arch.ordered_locals = nil
	arch.costed_locals = nil
	arch.stack_slots = 0
	arch.gc_slots    = 0
	arch.need_end    = false
	arch.new_labels  = 0

	arch.Section("text")

	ret_type := arch.cur_func.ty.sub

	out_name := "???"
	if fu.method {
		belong_type := fu.ty.param[0].ty.sub
		out_name = arch.EncodeMethodName(fu.name, belong_type)
	} else {
		out_name = arch.EncodeName(fu.name)
	}

	if fu.extern {
		OutLine("extern %s", out_name)
		OutLine("")
		return
	}

	if fu.public {
		OutLine("global %s", out_name)
	}

	OutLine("%s:", out_name)

	//  Dumpty(arch.cur_func.name, arch.cur_func.body, 0)

	arch.cur_func.DeadCodeRemoval()
	arch.cur_func.UnusedLocals()
	arch.cur_func.StrengthReduce()
	arch.cur_func.UnusedLocals()

	arch.cur_func.DetectLoops()
	arch.cur_func.LiveRanges(false)
	arch.cur_func.ExtendLiveRanges()

	// collect and sort locals, to give a reproducible order
	arch.SortLocals()

	if arch.RegisterAllocate() != OK {
		return
	}
	if arch.StackAllocate() != OK {
		return
	}

	OutIns("push", "rbp")
	OutIns("mov", "rbp,rsp")

	arch.SetupStack()
	arch.SaveExtraRegs()
	arch.TransferParameters()

	OutLine("")

	for _, t := range fu.body.children {
		arch.CompileNode(t)
	}

	if ret_type.kind != TYP_Void {
		// since `no-return` cannot return, we will raise an error.
		// do the same for any non-void return type, just in case our
		// detection of missing returns fails to catch it.
		OutIns("hlt", "")
	}

	if arch.need_end {
		OutLine(".__end:")
	}

	arch.RestoreExtraRegs()

	if ret_type.kind != TYP_NoReturn {
		OutIns("leave", "")
		OutIns("ret", "")
	}
	OutLine("")
}

func (arch *AMD64_Arch) SetupStack() {
	if arch.stack_slots > 0 {
		// size needs to be a multiple of 16
		size := arch.stack_slots * 8
		size = ((size + 15) / 16) * 16

		OutIns("sub", "rsp,%d", size)
	}
}

func (arch *AMD64_Arch) SaveExtraRegs() {
	slot := 1

	for reg := RAX; reg <= R15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("mov", "[rbp-%d],%s", slot*8, arch.RegName(reg))
			slot += 1
		}
	}
	for reg := XMM0; reg <= XMM15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("movq", "[rbp-%d],%s", slot*8, arch.RegName(reg))
			slot += 1
		}
	}
}

func (arch *AMD64_Arch) RestoreExtraRegs() {
	slot := 1

	for reg := RAX; reg <= R15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("mov", "%s,[rbp-%d]", arch.RegName(reg), slot*8)
			slot += 1
		}
	}
	for reg := XMM0; reg <= XMM15; reg++ {
		if arch.extra_regs.group[reg] {
			OutIns("movq", "%s,[rbp-%d]", arch.RegName(reg), slot*8)
			slot += 1
		}
	}
}

func (arch *AMD64_Arch) TransferParameters() {
	for _, par := range arch.cur_func.params {
		is_float := (par.ty.kind == TYP_Float)

		// ignore unused parameters
		if par.reads == 0 {
			continue
		}

		// it is where it should be?
		if par.abi_reg == par.reg {
			continue
		}

		ins := "mov"
		if is_float {
			ins = arch.FloatIns("movd", "movq", par.ty)
		}

		if par.reg == REG_ON_STACK {
			// register --> stack
			r_name   := arch.RegNameSize(par.abi_reg, par.ty.bits)
			slot_str := arch.EncodeStack("rbp", par.offset)
			OutIns(ins, "%s,%s", slot_str, r_name)

		} else if par.abi_reg == REG_ON_STACK {
			// stack --> register
			slot_str := arch.EncodeStack("rbp", par.offset)
			r_name   := arch.RegNameSize(par.reg, par.ty.bits)
			OutIns(ins, "%s,%s", r_name, slot_str)

		} else {
			// register --> register
			r_dest := arch.RegNameSize(par.reg,     par.ty.bits)
			r_src  := arch.RegNameSize(par.abi_reg, par.ty.bits)
			OutIns(ins, "%s,%s", r_dest, r_src)
		}
	}
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CompileNode(t *Node) {
	switch t.kind {
	case NH_Label:
		arch.DoLabel(t)
		return

	case NH_Comment:
		arch.DoComment(t)
		return

	case NH_Drop:
		arch.DoDrop(t)

	case NH_Move:
		arch.DoMove(t, false)

	case NH_MemWrite:
		arch.DoMove(t, true)

	case NH_Swap:
		arch.DoSwap(t)

	case NH_Jump:
		arch.DoJump(t, false)

	case NH_JumpNot:
		arch.DoJump(t, true)

	case NH_Return:
		arch.DoReturn(t)

	case NH_TailCall:
		arch.DoTailCall(t)

	default:
		panic("unknown node kind in CompileNode")
	}

	OutLine("")
}

func (arch *AMD64_Arch) DoLabel(t *Node) {
	out_name := arch.EncodeLabel(t.str)
	OutLine("%s:", out_name)
}

func (arch *AMD64_Arch) DoComment(t *Node) {
	if t.str == "" {
		OutLine("")
	} else {
		OutLine("\t; %s", t.str)
	}
}

func (arch *AMD64_Arch) DoDrop(t *Node) {
	t_comp := t.children[0]

	arch.CalcComputation(t_comp, REG_NONE)
}

func (arch *AMD64_Arch) DoMove(t *Node, mem_write bool) {
	t_dest := t.children[0]
	t_src  := t.children[1]

	// convert `x = iadd x 1` to an INC instruction
	if t_src.kind == NC_Builtin &&
		t_src.Len() == 2 &&
		IsLiteralOne(t_src.children[1]) &&
		arch.SameLocalOrMem(t_src.children[0], t_dest) {

		switch t_src.str {
		case "iadd", "padd":
			dest := arch.ArgumentString(t_dest, 8|4)
			OutIns("inc", "%s", dest)
			return

		case "isub", "psub":
			dest := arch.ArgumentString(t_dest, 8|4)
			OutIns("dec", "%s", dest)
			return
		}
	}

	// when source or dest is a register, we don't need an intermediate.
	if arch.IsRegister(t_src) {
		reg := arch.GetRegister(t_src)
		arch.StoreReg(reg, t_dest)
		return
	}
	if arch.IsRegister(t_dest) {
		reg := arch.GetRegister(t_dest)
		arch.CalcComputation(t_src, reg)
		return
	}

	// general case needs two instructions (a load and a store),
	// so determine the intermediate register....

	reg := RAX
	if t_dest.ty.kind == TYP_Float {
		reg = arch.workf1
	}

	arch.CalcComputation(t_src, reg)
	arch.StoreReg(reg, t_dest)
}

func (arch *AMD64_Arch) DoSwap(t *Node) {
	t_left  := t.children[0]
	t_right := t.children[1]

	is_float := (t_left.ty.kind == TYP_Float)

	L_memory := (t_left .kind == NP_Memory)
	R_memory := (t_right.kind == NP_Memory)

	L_isreg  := arch.IsRegister(t_left)
	R_isreg  := arch.IsRegister(t_right)

	// handle floating point
	if is_float {
		if L_isreg {
			arch.LoadReg(arch.workf1, t_right)
			arch.StoreReg(arch.GetRegister(t_left), t_right)
			arch.StoreReg(arch.workf1, t_left)

		} else if R_isreg {
			arch.LoadReg(arch.workf1, t_left)
			arch.StoreReg(arch.GetRegister(t_right), t_left)
			arch.StoreReg(arch.workf1, t_right)

		} else {
			arch.LoadReg(arch.workf1, t_left)
			arch.LoadReg(arch.workf2, t_right)

			arch.StoreReg(arch.workf1, t_right)
			arch.StoreReg(arch.workf2, t_left)
		}
		return
	}

	// handle case where neither operand is a register
	if !L_isreg && !R_isreg {
		if L_memory || R_memory {
			reg1 := RAX
			reg2 := R10  // cannot use R11

			arch.LoadReg(reg1, t_left)
			arch.LoadReg(reg2, t_right)

			arch.StoreReg(reg1, t_right)
			arch.StoreReg(reg2, t_left)

		} else {  // both locals
			op_left  := arch.Operand(t_left)
			op_right := arch.Operand(t_right)

			arch.LoadReg(RAX, t_left)
			arch.LoadReg(R11, t_right)

			bits := t_left.ty.bits

			OutIns("mov", "%s,%s", op_right, arch.RegNameSize(RAX, bits))
			OutIns("mov", "%s,%s", op_left,  arch.RegNameSize(R11, bits))
		}
		return
	}

	op_left  := arch.Operand(t_left)
	op_right := arch.Operand(t_right)

	OutIns("xchg", "%s,%s", op_left, op_right)
}

func (arch *AMD64_Arch) DoJump(t *Node, negated bool) {
	out_label := arch.EncodeLabel(t.str)

	if t.Len() == 0 {
		OutIns("jmp", "%s", out_label)
		return
	}

	jump_ins := "jnz"
	if negated {
		jump_ins = "jz"
	}

	t_cond := t.children[0]

	reg    := RAX
	r_name := "al"

	if arch.IsRegister(t_cond) {
		reg    = arch.GetRegister(t_cond)
		r_name = arch.RegNameSize(reg, 8)

	} else {
		// certain builtins can set the CPU condition codes.
		// for these we can produce nicer assembly code.

		if t_cond.kind == NC_Builtin {
			cc := arch.GetCCforBuiltin(t_cond)
			if cc != "" {
				if negated {
					cc = arch.NegateCC(cc)
				}
				arch.CalcBuiltin_CC(t_cond)
				OutIns("j" + cc, out_label)
				return
			}
		}

		arch.CalcComputation(t_cond, reg)
	}

	OutIns("test", "%s,1", r_name)
	OutIns(jump_ins, "%s", out_label)
}

func (arch *AMD64_Arch) DoReturn(t *Node) {
	arch.need_end = true

	ret_type := arch.cur_func.ty.sub

	if t.Len() > 0 {
		// get register for result according to the ABI
		reg := RAX
		if ret_type.kind == TYP_Float {
			reg = XMM0
		}

		arch.CalcComputation(t.children[0], reg)
	}

	OutIns("jmp", ".__end")
}

func (arch *AMD64_Arch) DoTailCall(t *Node) {
	arch.CalcCall(t, true, REG_NONE)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CalcComputation(t *Node, reg Register) {
	// reg can be REG_NONE when the result is not used.
	// reg may be RAX or XMM0, but never R11 or workf1/workf2.

	switch t.kind {
	case NC_Call:
		arch.CalcCall(t, false, reg)

	case NC_Builtin:
		arch.CalcBuiltin(t, reg)

	case NC_Convert:
		arch.CalcConvert(t, reg)

	case NC_RawCast:
		arch.CalcRawCast(t, reg)

	case NC_MemRead:
		arch.CalcMemRead(t, reg)

	case NC_Select:
		arch.CalcSelect(t, reg)

	case NC_StackVar:
		arch.CalcStackVar(t, reg)

	default:
		// node must be a normal operand
		if reg != REG_NONE {
			arch.LoadReg(reg, t)
		}
	}
}

func (arch *AMD64_Arch) CalcConvert(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	t_src := t.children[0]

	src_type  := t_src.ty
	conv_type := t.ty

	if src_type.kind == TYP_Float && conv_type.kind == TYP_Float {
		arch.CalcConvert_float_float(t, reg)

	} else if conv_type.kind == TYP_Float {
		arch.CalcConvert_int_float(t, reg)

	} else if src_type.kind == TYP_Float {
		arch.CalcConvert_float_int(t, reg)

	} else if conv_type.kind == TYP_Bool {
		arch.CalcConvert_int_bool(t, reg)

	} else {
		arch.CalcConvert_int_int(t, reg)
	}
}

func (arch *AMD64_Arch) CalcConvert_int_int(t *Node, reg Register) {
	// this handles int <--> int conversions.
	// main thing it does is widening (target type has more bits).

	t_src  := t.children[0]
	src_type  := t_src.ty
	conv_type := t.ty

	arch.LoadReg(reg, t_src)

	if conv_type.bits > src_type.bits {
		arch.CalcConvert_int_extend(reg, src_type)
	}
}

func (arch *AMD64_Arch) CalcConvert_int_extend(reg Register, src_type *Type) {
	// Note that the AMD64 architecture automatically zero extends
	// to 64 bits when moving something to a 32-bit register.
	// [ hence no `movzx` for the 32-bit case below ]

	if src_type.unsigned {
		switch src_type.bits {
		case 8:  OutIns("movzx", "%s,%s", arch.RegName(reg),   arch.RegName8(reg))
		case 16: OutIns("movzx", "%s,%s", arch.RegName(reg),   arch.RegName16(reg))
		case 32: OutIns("mov",   "%s,%s", arch.RegName32(reg), arch.RegName32(reg))
		}

	} else {
		switch src_type.bits {
		case 8:  OutIns("movsx", "%s,%s", arch.RegName(reg), arch.RegName8(reg))
		case 16: OutIns("movsx", "%s,%s", arch.RegName(reg), arch.RegName16(reg))
		case 32: OutIns("movsxd","%s,%s", arch.RegName(reg), arch.RegName32(reg))
		}
	}
}

func (arch *AMD64_Arch) CalcConvert_int_bool(t *Node, reg Register) {
	t_src := t.children[0]

	// negative or zero becomes FALSE, anything else becomes TRUE.
	ins := "setg"

	if t_src.ty.unsigned {
		ins = "setnz"
	}

	arch.LoadReg(reg, t_src)

	arg := arch.RegNameSize(reg, t_src.ty.bits)

	OutIns("test", "%s,%s", arg, arg)
	OutIns(ins, "%s", arch.RegName8(reg))
}

func (arch *AMD64_Arch) CalcConvert_float_float(t *Node, reg Register) {
	t_src  := t.children[0]
	src_type  := t_src.ty
	conv_type := t.ty

	if src_type.bits == conv_type.bits {
		arch.LoadReg(reg, t_src)
		return
	}

	arch.LoadReg(arch.workf2, t_src)
	src_name  := arch.RegName(arch.workf2)

	dest_name := arch.RegName(reg)

	ins := arch.FloatIns("cvtsd2ss", "cvtss2sd", conv_type)

	OutIns(ins, "%s,%s", dest_name, src_name)
}

func (arch *AMD64_Arch) CalcConvert_int_float(t *Node, reg Register) {
	t_src  := t.children[0]
	src_type  := t_src.ty
	conv_type := t.ty

	arch.LoadReg(RAX, t_src)

	// need an integer at least 32-bits wide, extend if needed
	if src_type.unsigned {
		switch src_type.bits {
		case 8:  OutIns("movzx", "eax,al")
		case 16: OutIns("movzx", "eax,ax")
		}
	} else {
		switch src_type.bits {
		case 8:  OutIns("movsx", "eax,al")
		case 16: OutIns("movsx", "eax,ax")
		}
	}

	src_name := "rax"
	if src_type.bits < 64 {
		src_name = "eax"
	}

	dest_name := arch.RegName(reg)

	ins := "cvtsi2sd"
	if conv_type.bits < 64 {
		ins = "cvtsi2ss"
	}

	OutIns(ins, "%s,%s", dest_name, src_name)
}

func (arch *AMD64_Arch) CalcConvert_float_int(t *Node, reg Register) {
	t_src  := t.children[0]
	src_type  := t_src.ty
	conv_type := t.ty

	ins := "cvtsd2si"
	if src_type.bits < 64 {
		ins = "cvtss2si"
	}

	arch.LoadReg(arch.workf2, t_src)
	src_name := arch.RegName(arch.workf2)

	dest_name := arch.RegName(reg)
	if conv_type.bits < 64 {
		dest_name = arch.RegName32(reg)
	}

	OutIns(ins, "%s,%s", dest_name, src_name)

	// WISH: check if value is too large for 8/16 bit destination,
	//       and if so produce 0x80/0x8000.
}

func (arch *AMD64_Arch) CalcRawCast(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	t_src := t.children[0]

	src_type  := t_src.ty
	dest_type := t.ty

	src_float  := (src_type.kind  == TYP_Float)
	dest_float := (dest_type.kind == TYP_Float)

	// nothing special is needed unless we are casting between a float
	// and a non-float....

	if src_float == dest_float {
		arch.LoadReg(reg, t_src)

	} else if src_float {
		// float --> integer

		arch.LoadReg(arch.workf1, t_src)

		ins    := arch.FloatIns("movd", "movq", src_type)
		i_name := arch.RegNameSize(reg, dest_type.bits)
		f_name := arch.RegName(arch.workf1)

		OutIns(ins, "%s,%s", i_name, f_name)

	} else {
		// integer --> float

		arch.LoadReg(RAX, t_src)

		ins := arch.FloatIns("movd", "movq", dest_type)
		i_name := arch.RegNameSize(RAX, src_type.bits)
		f_name := arch.RegName(reg)

		OutIns(ins, "%s,%s", f_name, i_name)
	}
}

func (arch *AMD64_Arch) CalcMemRead(t *Node, reg Register) {
	// source is *always* a NP_Memory operand
	t_src := t.children[0]

	if reg == REG_NONE {
		reg = RAX
	}

	arch.LoadReg(reg, t_src)
}

func (arch *AMD64_Arch) CalcCall(t *Node, is_tail bool, reg Register) {
	t_func := t.children[0]
	params := t.children[1:]

	fun_type := t_func.ty
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type.kind != TYP_Function {
		panic("weird fun type")
	}

	/* process the parameters */

	abi_regs := arch.ParameterRegs(fun_type)

	arch.HandleStackParameters(params, abi_regs)
	arch.HandleRegisterParameters(params, abi_regs)

	/* generate code to call the function */

	call_name := "r11"

	switch t_func.kind {
	case NP_FuncRef:
		call_name = arch.EncodeName(t_func.str)
	case NP_Method:
		belong_type := t_func.ty.param[0].ty.sub
		call_name = arch.EncodeMethodName(t_func.str, belong_type)
	default:
		arch.LoadReg(R11, t_func)
	}

	if is_tail {
		// if we used extra registers, make sure to restore them
		arch.RestoreExtraRegs()

		OutIns("leave", "")

		// a tail-call is really a non-local jump
		OutIns("jmp", "%s", call_name)
		return
	}

	OutIns("call", "%s", call_name)

	// store the result
	if reg != REG_NONE {
		ret_type := fun_type.sub

		if fun_type.sub.kind == TYP_Float {
			arch.MoveReg(reg, XMM0, ret_type)
		} else {
			arch.MoveReg(reg, RAX, ret_type)
		}
	}
}

func (arch *AMD64_Arch) HandleStackParameters(params []*Node, abi_regs []Register) {
	slot := 0
	if arch.abi == "win" {
		slot = 4
	}

	for i, par := range params {
		if abi_regs[i] == REG_ON_STACK {
			reg := RAX

			if arch.IsRegister(par) {
				reg = arch.GetRegister(par)

			} else {
				if par.ty.kind == TYP_Float {
					reg = arch.workf1
				}
				arch.LoadReg(reg, par)
			}

			ins := "mov"
			if par.ty.kind == TYP_Float {
				ins = arch.FloatIns("movss", "movsd", par.ty)
			}

			slot_str := arch.EncodeStack("rsp", slot*8)
			slot += 1

			OutIns(ins, "%s,%s", slot_str, arch.RegNameSize(reg, par.ty.bits))
		}
	}
}

func (arch *AMD64_Arch) HandleRegisterParameters(params []*Node, abi_regs []Register) {
	// detect when loading an ABI register would overwrite the value
	// in a local variable (since they use the same register) AND that
	// local variable is needed for a later in-reg parameter.

	// WISH: if we can simply re-order the visit (esp. backwards) to avoid
	//       any overwrites, then do that instead.

	use_regs := make([]Register, len(abi_regs))
	for i, reg := range abi_regs {
		use_regs[i] = reg
	}

	swaps := make([]Register, 0)

	// need multiple passes (due to the register swapping)
	for {
		did_swap := false

		for i := range params {
			r1 := use_regs[i]
			if r1 > 0 {
				for k := i+1; k < len(params); k++ {
					r2  := use_regs[k]
					par := params[k]

					if r2 != REG_ON_STACK && arch.UsesRegister(par, r1) {
						use_regs[i] = r2
						use_regs[k] = r1

						swaps = append(swaps, r1)
						swaps = append(swaps, r2)

						/* DEBUG
						println("OVERWRITE FOUND IN", arch.cur_func.name)
						println("  param", i, "overwrites local in param", k)
						println("  register", arch.RegName(r1), "<->", arch.RegName(r2))
						*/

						// need to break out of two loops (i and k)
						did_swap = true
						break
					}
				}
			}
			if did_swap {
				break
			}
		}

		if !did_swap {
			break
		}
	}

	// load the registers now
	for i, par := range params {
		reg := use_regs[i]

		if reg != REG_ON_STACK {
			arch.LoadReg(reg, par)
		}
	}

	// perform any needed swaps
	for i := len(swaps)-2; i >= 0; i -= 2 {
		r1 := swaps[i]
		r2 := swaps[i+1]

		if r1 >= XMM0 {
			ins := "movq"
			r3  := arch.workf2
			OutIns(ins, "%s,%s", arch.RegName(r3), arch.RegName(r1))
			OutIns(ins, "%s,%s", arch.RegName(r1), arch.RegName(r2))
			OutIns(ins, "%s,%s", arch.RegName(r2), arch.RegName(r3))
		} else {
			OutIns("xchg", "%s,%s", arch.RegName(r1), arch.RegName(r2))
		}
	}
}

func (arch *AMD64_Arch) CalcSelect(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	t_cond := t.children[0]
	t_src1 := t.children[1]
	t_src2 := t.children[2]

	// try to use the CMOV instruction...
	if arch.CalcSelect_cmov(t_cond, t_src1, t_src2, reg) {
		return
	}

	// version using jumps and labels

	arch.new_labels += 1
	lab_false := ".__selfalse" + strconv.Itoa(arch.new_labels)
	lab_done  := ".__seldone"  + strconv.Itoa(arch.new_labels)

	cond_arg := arch.ArgumentString(t_cond, 8|4)
	OutIns("test", "%s,1", cond_arg)

	// generate simpler code if the output register is the same as one of
	// the input values.
	if arch.GetRegister(t_src1) == reg {
		OutIns("jnz", "%s", lab_done)
		arch.LoadReg(reg, t_src2)

	} else if arch.GetRegister(t_src2) == reg {
		OutIns("jz", "%s", lab_done)
		arch.LoadReg(reg, t_src1)

	} else {
		OutIns("jz", "%s", lab_false)

		arch.LoadReg(reg, t_src1)
		OutIns("jmp", "%s", lab_done )

		OutLine("%s:", lab_false)
		arch.LoadReg(reg, t_src2)
	}

	OutLine("%s:", lab_done )
}

func (arch *AMD64_Arch) CalcSelect_cmov(t_cond, t_src1, t_src2 *Node, reg Register) bool {
	// not possible for floats
	if t_src1.ty.kind == TYP_Float {
		return false
	}

	// avoid unnecessary reads from memory (except for on-stack locals)
	if t_src1.kind == NP_Memory || t_src2.kind == NP_Memory {
		return false
	}

	// when condition is currently in same register as output, that
	// complicates things.  just abort for that (rare?) case.
	if arch.GetRegister(t_cond) == reg {
		return false
	}

	reg2 := arch.GetRegister(t_src2)

	if reg2 == reg {
		reg2 = RAX

	} else if reg2 == REG_NONE {
		reg2 = R11

		// check that condition does not need RAX or R11
		if arch.GetRegister(t_cond) == REG_NONE {
			if t_cond.kind == NP_Local || t_cond.kind == NP_Memory {
				// ok
			} else {
				return false
			}
		}
	}

	arch.LoadReg(reg,  t_src1)
	arch.LoadReg(reg2, t_src2)

	cond_arg := arch.ArgumentString(t_cond, 8|4)
	OutIns("test", "%s,1", cond_arg)

	// use full register names, as CMOV does not support 8-bit regs
	OutIns("cmovz", "%s,%s", arch.RegName(reg), arch.RegName(reg2))

	return true
}

func (arch *AMD64_Arch) CalcStackVar(t *Node, reg Register) {
	if reg == REG_NONE {
		return
	}

	ofs_str := arch.EncodeStack("rbp", t.offset)

	OutIns("lea", "%s,%s", arch.RegName(reg), ofs_str)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) CalcBuiltin(t *Node, reg Register) {
	if reg == REG_NONE {
		// none of the current intrinsics have side effects, hence
		// we can ignore this statement.
		return
	}

	name  := t.str
	flags := builtins[name].flags

	switch name {
	case "eq?", "ne?", "lt?", "le?", "gt?", "ge?",
		"not", "zero?", "some?", "null?", "ref?",
		"pos?", "neg?", "inf?", "nan?":

		arch.CalcBuiltin_CC(t)

		cc := arch.GetCCforBuiltin(t)
		if cc == "" {
			panic("GetCCforBuiltin is empty")
		}

		out_name := arch.RegNameSize(reg, 8)
		OutIns("set" + cc, "%s", out_name)
		return
	}

	// a floating point operation?
	if (flags & BF_FLOAT) != 0 {
		arch.CalcBuiltin_Float(t, reg)
		return
	}

	// everything else is an integer operation
	// (though that includes pointer and boolean types).
	arch.CalcBuiltin_Int(t, reg)
}

func (arch *AMD64_Arch) CalcBuiltin_Int(t *Node, reg Register) {
	name := t.str

	if t.Len() == 1 {
		t_arg := t.children[0]
		arch.LoadReg(reg, t_arg)

		r_name := arch.RegNameSize(reg, t_arg.ty.bits)

		switch name {
		case "ineg":
			OutIns("neg", "%s", r_name)

		case "iflip":
			OutIns("not", "%s", r_name)

		case "iabs":
			arch.BU_int_abs(t, reg)

		case "little-endian":
			// nothing needed

		case "big-endian":
			arch.BU_endian_swap(t, reg)

		default:
			panic("intrinsic " + name + " NYI")
		}
		return
	}

	switch name {
	case "ishl":
		arch.BU_int_shift(t, reg)
		return

	case "ishr":
		arch.BU_int_shift(t, reg)
		return

	case "pdiff":
		arch.BU_pointer_diff(t, reg)
		return
	}

	// if the second (third etc) argument uses the same register as the
	// output register, we will use RAX as the accumulator instead.
	// WISH: avoid using RAX if can rearrange the arguments

	main_reg := reg

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		if arch.UsesRegister(t_arg, reg) {
			main_reg = RAX
			break
		}
	}

	// integer division/remainder MUST have dividend in RAX
	if name == "idivt" || name == "iremt" {
		main_reg = RAX
	}
	// 8-bit integer multiply also requires RAX
	if name == "imul" && t.children[0].ty.bits == 8 {
		main_reg = RAX
	}

	// load the first argument into the main register.
	// subsequent arguments will modify that.
	arch.LoadReg(main_reg, t.children[0])

	ty   := t.children[0].ty
	arg1 := arch.RegNameSize(main_reg, ty.bits)

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		// these two need a register (since they use CMOV)
		if name == "imax" || name == "imin" {
			reg2 := arch.GetRegister(t_arg)
			if reg2 == REG_NONE {
				reg2 = R11
			}
			arch.LoadReg(reg2, t_arg)
			arch.BU_int_minmax(t, main_reg, reg2)
			continue
		}

		flags2 := 4|2|1

		// these do not allow an immediate, but do need a memory size prefix
		if name == "idivt" || name == "iremt" {
			flags2 = 8|4|1
		}
		// 8-bit integer multiply also does not allow an immediate
		if name == "imul" && t.children[0].ty.bits == 8 {
			flags2 = 4|1
		}

		arg2 := arch.ArgumentString(t_arg, flags2)

		switch name {
		case "iadd", "padd":
			OutIns("add", "%s,%s", arg1, arg2)

		case "isub", "psub":
			OutIns("sub", "%s,%s", arg1, arg2)

		case "imul":
			// NOTE: we use the IMUL instruction for both signed and unsigned
			// integers, possible due to two's complement notation and that we
			// don't need to detect overflow.  it is also what GCC does.

			if name == "imul" && t.children[0].ty.bits == 8 {
				OutIns("imul", "%s", arg2)
			} else {
				// Note too that the second arg may be an immediate value, which
				// strictly speaking should use the 3-arg form, but NASM allows it.
				OutIns("imul", "%s,%s", arg1, arg2)
			}

		case "idivt":
			arch.BU_int_divrem(t, arg2)

		case "iremt":
			arch.BU_int_divrem(t, arg2)

			if t.str == "iremt" {
				if ty.bits == 8 {
					OutIns("mov", "al,ah")
				} else if i == t.Len()-1 {
					// last one in chain can omit a redundant move
					main_reg = RDX
				} else {
					OutIns("mov", "rax,rdx")
				}
			}

		case "iand", "and":
			OutIns("and", "%s,%s", arg1, arg2)

		case "ior", "or":
			OutIns("or", "%s,%s", arg1, arg2)

		case "ixor":
			OutIns("xor", "%s,%s", arg1, arg2)

		default:
			panic("intrinsic " + name + " NYI")
		}
	}

	arch.MoveReg(reg, main_reg, ty)
}

func (arch *AMD64_Arch) CalcBuiltin_Float(t *Node, reg Register) {
	name := t.str
	ty   := t.ty

	if t.Len() == 1 {
		t_arg   := t.children[0]
		arg_reg := arch.GetRegister(t_arg)

		switch name {
		case "finv":
			// computing the inverse needs value in a different register
			// than the output register.
			if arg_reg == reg {
				arg_reg = arch.workf2
				arch.LoadReg(arg_reg, t_arg)
			}

			arch.BU_float_invert(t, reg, arg_reg)
			return

		case "ffloor", "fceil", "fround", "ftrunc":
			// these _can_ perform the operation from one register to a
			// different one -- so save a move instruction if we can.
			if arg_reg == REG_NONE {
				arg_reg = reg
				arch.LoadReg(arg_reg, t_arg)
			}

			arch.BU_float_round(t, reg, arg_reg)
			return
		}

		if arg_reg != reg {
			arch.LoadReg(reg, t_arg)
		}

		switch name {
		case "fneg":
			arch.BU_float_neg(t, reg)

		case "fabs":
			arch.BU_float_abs(t, reg)

		case "fsqrt":
			arch.BU_float_sqrt(t, reg)

		default:
			panic("intrinsic " + name + " NYI")
		}
		return
	}

	// if the second (third etc) argument uses the same register as the
	// output register, we need to move it to the second work-horse.
	// this is never needed when output is the first work-horse.
	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		if arch.GetRegister(t_arg) == reg {
			arch.MoveReg(arch.workf2, reg, ty)
			break
		}
	}

	// load the first argument into the output register.
	// subsequent arguments will modify that.
	arch.LoadReg(reg, t.children[0])

	arg1 := arch.RegName(reg)

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		reg2 := arch.GetRegister(t_arg)
		if reg2 == reg {
			reg2 = arch.workf2
		}
		if reg2 == REG_NONE {
			if reg == arch.workf1 {
				reg2 = arch.workf2
			} else {
				reg2 = arch.workf1
			}
			arch.LoadReg(reg2, t_arg)
		}

		arg2 := arch.RegName(reg2)

		switch name {
		case "fadd":
			ins := arch.FloatIns("addss", "addsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fsub":
			ins := arch.FloatIns("subss", "subsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fmul":
			ins := arch.FloatIns("mulss", "mulsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fdiv":
			ins := arch.FloatIns("divss", "divsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fmax":
			ins := arch.FloatIns("maxss", "maxsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fmin":
			ins := arch.FloatIns("minss", "minsd", ty)
			OutIns(ins, "%s,%s", arg1, arg2)

		case "fremt":
			arch.BU_float_remainder(t, arg1, arg2)

		default:
			panic("intrinsic " + name + " NYI")
		}
	}
}

func (arch *AMD64_Arch) CalcBuiltin_CC(t *Node) {
	// this ONLY handles intrinsics which set the CPU condition codes.

	ty := t.children[0].ty

	// handle the extended `eq?` and `ne?` forms
	if t.Len() > 2 {
		arch.BU_chained_equal(t)
		return
	}

	switch t.str {
	case "not":
		arg := arch.ArgumentString(t.children[0], 8|4)

		OutIns("test", "%s,1", arg)

	case "eq?", "ne?", "lt?", "le?", "gt?", "ge?":
		arch.BU_comparison(t)

	case "zero?", "some?", "pos?", "neg?", "null?", "ref?", "inf?", "nan?":
		arg := arch.ArgumentString(t.children[0], 0)

		if ty.kind != TYP_Float {
			arch.BU_int_classify(t, arg)
		} else if ty.bits == 32 {
			arch.BU_float32_classify(t, arg)
		} else {
			arch.BU_float64_classify(t, arg)
		}

	default:
		panic("CalcBuiltin_CC with non-cc builtin")
	}
}

func (arch *AMD64_Arch) GetCCforBuiltin(t *Node) string {
	if t.kind != NC_Builtin {
		return ""
	}
	if t.Len() == 0 {
		return ""
	}

	arg_ty := t.children[0].ty
	if arg_ty.kind == TYP_Float {
		switch t.str {
		case "neg?": return "a"
		case "pos?": return "a"
		case "inf?": return "z"
		case "nan?": return "a"
		}
	}

	// note that floating point compares are like unsigned integer compares,
	// they distinguish less/greater via the carry flag (CF).
	signed := false

	if arg_ty.kind == TYP_Int {
		signed = !arg_ty.unsigned
	}

	switch t.str {
	case "eq?": return "e"
	case "ne?": return "ne"
	case "not": return "z"

	case "zero?", "null?": return "z"
	case "some?", "ref?":  return "nz"

	case "lt?":  if signed { return "l"  } else { return "b"  }
	case "le?":  if signed { return "le" } else { return "be" }
	case "gt?":  if signed { return "g"  } else { return "a"  }
	case "ge?":  if signed { return "ge" } else { return "ae" }
	case "pos?": if signed { return "g"  } else { return "nz" }
	case "neg?": if signed { return "s"  } else { return "c"  }
	}

	return ""
}

func (arch *AMD64_Arch) NegateCC(cc string) string {
	if cc[0] == 'n' {
		return cc[1:]
	}
	return "n" + cc
}

func (arch *AMD64_Arch) BU_comparison(t *Node) {
	ty := t.children[0].ty

	// use R11 for first argument and RAX for second, which is opposite
	// to normal usage.  we do that to avoid a case where first argument
	// becomes `[R11]` and second argument becomes plain `R11`.
	arg2 := arch.ArgumentString(t.children[1], 2)
	arg1 := arch.ArgumentString(t.children[0], 8|4|1)

	if ty.kind == TYP_Bool {
		// we do it this way to ignore the upper bits
		if arg1 != "al" {
			OutIns("mov",  "al,%s", arg1)
		}
		OutIns("xor",  "al,%s", arg2)
		OutIns("test", "al,1")
		return
	}

	cmp_ins := "cmp"
	if ty.kind == TYP_Float {
		cmp_ins = arch.FloatIns("comiss", "comisd", ty)
	}

	OutIns(cmp_ins, "%s,%s", arg1, arg2)
}

func (arch *AMD64_Arch) BU_chained_equal(t *Node) {
	ty := t.children[0].ty

	reg     := RAX
	cmp_ins := "cmp"

	if ty.kind == TYP_Float {
		reg     = arch.workf1
		cmp_ins = arch.FloatIns("comiss", "comisd", ty)
	}
	if ty.kind == TYP_Bool {
		panic("BU_chained_equal with bools")
	}

	r_name := arch.RegNameSize(reg, ty.bits)

	arch.LoadReg(reg, t.children[0])

	arch.new_labels += 1
	lab_done := ".__eqdone" + strconv.Itoa(arch.new_labels)

	// for `eq?` we want any to match.
	// for `ne?` we want none to match.
	// both cases require the "JE" jump instruction.

	for i := 1; i < t.Len(); i++ {
		t_arg := t.children[i]

		arg2 := arch.ArgumentString(t_arg, 4|2|1)
		OutIns(cmp_ins, "%s,%s", r_name, arg2)

		if i < t.Len()-1 {
			OutIns("je", "%s", lab_done)
		}
	}

	if t.Len() > 2 {
		OutLine("%s:", lab_done)
	}
}

func (arch *AMD64_Arch) BU_int_classify(t *Node, arg1 string) {
	ty := t.children[0].ty

	// using `neg?` on unsigned integers will always produce FALSE.
	// to achieve that, we clear carry flag (CF) here and test for it later.
	if t.str == "neg?" && ty.unsigned {
		OutIns("clc", "")
		return
	}

	OutIns("test", "%s,%s", arg1, arg1)
}

func (arch *AMD64_Arch) BU_float32_classify(t *Node, arg1 string) {
	OutIns("movd", "eax,%s", arg1)

	switch t.str {
	case "zero?":
		// all bits except sign must be zero
		OutIns("shl", "eax,1")

	case "some?":
		OutIns("shl", "eax,1")

	case "pos?":
		OutIns("shl", "eax,1")

		// this tests that the sign bit (now in CF) was zero and the
		// remaining bits were not all zero.
		//   ins = "seta"

	case "neg?":
		OutIns("shl", "eax,1")
		OutIns("cmc", "")
		// ins = "seta"

	case "inf?":
		// require exponent is all 1s, fraction is all 0s
		OutIns("xor", "eax,0x7F800000")
		OutIns("shl", "eax,1")
		// ins = "setz"

	case "nan?":
		// require exponent is all 1s, fraction is non-zero
		OutIns("shl", "eax,1")
		OutIns("cmp", "eax,0xFF000000")
		// ins = "seta"
	}
}

func (arch *AMD64_Arch) BU_float64_classify(t *Node, arg1 string) {
	OutIns("movq", "rax,%s", arg1)

	switch t.str {
	case "zero?":
		// all bits except sign must be zero
		OutIns("shl", "rax,1")

	case "some?":
		OutIns("shl", "rax,1")

	case "pos?":
		OutIns("shl", "rax,1")

		// this tests that the sign bit (now in CF) was zero and the
		// remaining bits were not all zero.
		// ins = "seta"

	case "neg?":
		OutIns("shl", "rax,1")
		OutIns("cmc", "")
		// ins = "seta"

	case "inf?":
		// require exponent is all 1s, fraction is all 0s
		OutIns("bswap", "rax")
		OutIns("xor",   "ax,0xF07F")
		OutIns("shl",   "al,1")
		OutIns("test",  "rax,rax")
		// ins = "setz"

	case "nan?":
		// require exponent is all 1s, fraction is non-zero
		OutIns("mov", "r11d,0xFFE00000")
		OutIns("shl", "r11,32")
		OutIns("shl", "rax,1")
		OutIns("cmp", "rax,r11")
		// ins = "seta"
	}
}

func (arch *AMD64_Arch) BU_int_abs(t *Node, reg Register) {
	ty := t.children[0].ty

	arg1 := arch.RegNameSize(reg, ty.bits)
	arg2 := arch.RegNameSize(R11, ty.bits)

	OutIns("mov",  "%s,%s", arg2, arg1)
	OutIns("neg",  "%s",    arg2)
	OutIns("test", "%s,%s", arg1, arg1)

	// CMOV does not support 8-bit registers
	arg1 = arch.RegName(reg)
	arg2 = arch.RegName(R11)

	OutIns("cmovs", "%s,%s", arg1, arg2)
}

func (arch *AMD64_Arch) BU_endian_swap(t *Node, reg Register) {
	ty     := t.children[0].ty
	r_name := arch.RegNameSize(reg, ty.bits)

	switch ty.bits {
	case 8:
		// nothing needed

	case 16:
		if reg != RAX {
			OutIns("mov", "ax,%s", r_name)
		}
		OutIns("xchg", "al,ah")
		if reg != RAX {
			OutIns("mov", "%s,ax", r_name)
		}

	case 32, 64:
		OutIns("bswap", "%s", r_name)
	}
}

func (arch *AMD64_Arch) BU_pointer_diff(t *Node, reg Register) {
	t_ptr1 := t.children[0]
	t_ptr2 := t.children[1]

	r_name := arch.RegName(reg)
	arg2   := "xxx"

	if arch.GetRegister(t_ptr2) == reg {
		arch.LoadReg(R11, t_ptr2)
		arg2 = "r11"
	} else {
		arg2 = arch.ArgumentString(t_ptr2, 4|1)
	}

	arch.LoadReg(reg, t_ptr1)

	OutIns("sub", "%s,%s", r_name, arg2)
}

func (arch *AMD64_Arch) BU_int_shift(t *Node, reg Register) {
	t_arg   := t.children[0]
	t_shift := t.children[1]

	// shift operations must use the CL register or a literal value.
	// if the output register is RCX, things get a bit tricky....

	arg_reg := reg
	if reg == RCX && t_shift.kind != NL_Integer {
		arg_reg = RAX
	}
	arg_str := arch.RegNameSize(arg_reg, t_arg.ty.bits)

	arch.LoadReg(arg_reg, t_arg)

	shift_str := "cl"

	if t_shift.kind == NL_Integer {
		// parsing code has checked for a valid shift count
		shift_str = t_shift.str
	} else {
		arch.LoadReg(RCX, t_shift)
	}

	shift_ins := "shl"

	if t.str == "ishr" {
		if t_arg.ty.unsigned {
			shift_ins = "shr"
		} else {
			shift_ins = "sar"
		}
	}

	OutIns(shift_ins, "%s,%s", arg_str, shift_str)

	if reg == RCX && t_shift.kind != NL_Integer {
		OutIns("mov", "rcx,rax")
	}
}

func (arch *AMD64_Arch) BU_int_divrem(t *Node, arg2 string) {
	ty := t.children[0].ty

	// the DIV and IDIV instructions are tricky.
	// the dividend must be in AX, DX:AX, EDX:EAX or RDX:RAX combos.
	// result stores the quotient in RAX, remainder in RDX.

	if ty.unsigned {
		// clear the upper portion
		if ty.bits == 8 {
			OutIns("xor", "ah,ah")
		} else {
			OutIns("xor", "rdx,rdx")
		}
		OutIns("div", "%s", arg2)

	} else {
		// set upper to all 0s or all 1s, depending on sign of divisor
		switch ty.bits {
		case 8:  OutIns("cbw", "")
		case 16: OutIns("cwd", "")
		case 32: OutIns("cdq", "")
		default: OutIns("cqo", "")
		}
		OutIns("idiv", "%s", arg2)
	}
}

func (arch *AMD64_Arch) BU_int_minmax(t *Node, reg1, reg2 Register) {
	ty := t.children[0].ty

	ins := "???"

	if ty.unsigned {
		switch t.str {
		case "imax": ins = "cmovb"
		case "imin": ins = "cmova"
		}
	} else {
		switch t.str {
		case "imax": ins = "cmovl"
		case "imin": ins = "cmovg"
		}
	}

	arg1 := arch.RegNameSize(reg1, ty.bits)
	arg2 := arch.RegNameSize(reg2, ty.bits)

	OutIns("cmp", "%s,%s", arg1, arg2)

	// CMOV does not support 8-bit registers
	arg1 = arch.RegName(reg1)
	arg2 = arch.RegName(reg2)

	OutIns(ins, "%s,%s", arg1, arg2)
}

func (arch *AMD64_Arch) BU_float_neg(t *Node, reg Register) {
	ty := t.children[0].ty

	// produce a mask which flips the top bit
	mask := arch.RegNameSize(RAX, ty.bits)
	work := arch.RegName(arch.workf2)

	OutIns("xor", "%s,%s", mask, mask)
	OutIns("stc", "")
	OutIns("rcr", "%s,1", mask)

	ins := arch.FloatIns("movd", "movq", ty)
	OutIns(ins, "%s,%s", work, mask)

	ins = arch.FloatIns("xorps", "xorpd", ty)
	OutIns(ins, "%s,%s", arch.RegName(reg) , work)
}

func (arch *AMD64_Arch) BU_float_abs(t *Node, reg Register) {
	ty := t.children[0].ty

	// produce a mask which clears the top bit
	mask := arch.RegNameSize(RAX, ty.bits)
	work := arch.RegName(arch.workf2)

	OutIns("xor", "%s,%s", mask, mask)
	OutIns("dec", "%s", mask)
	OutIns("shr", "%s,1", mask)

	ins := arch.FloatIns("movd", "movq", ty)
	OutIns(ins, "%s,%s", work, mask)

	ins = arch.FloatIns("andps", "andpd", ty)
	OutIns(ins, "%s,%s", arch.RegName(reg), work)
}

func (arch *AMD64_Arch) BU_float_invert(t *Node, out, in Register) {
	// caller guarantees that `in` and `out` are different
	in_name  := arch.RegName(in)
	out_name := arch.RegName(out)

	if t.ty.bits == 32 {
		one := arch.AddFloat32("1.0")

		OutIns("movss", "%s,%s", out_name, one)
		OutIns("divss", "%s,%s", out_name, in_name)

	} else {
		one := arch.AddFloat64("1.0")

		OutIns("movsd", "%s,%s", out_name, one)
		OutIns("divsd", "%s,%s", out_name, in_name)
	}
}

func (arch *AMD64_Arch) BU_float_sqrt(t *Node, reg Register) {
	ty     := t.children[0].ty
	ins    := arch.FloatIns("sqrtss", "sqrtsd", ty)
	r_name := arch.RegName(reg)

	OutIns(ins, "%s,%s", r_name, r_name)
}

func (arch *AMD64_Arch) BU_float_round(t *Node, out, in Register) {
	ty := t.children[0].ty

	var mode int

	switch t.str {
	case "fround": mode = 0x0
	case "ffloor": mode = 0x1
	case "fceil":  mode = 0x2
	case "ftrunc": mode = 0x3
	}

	ins      := arch.FloatIns("roundss", "roundsd", ty)
	in_name  := arch.RegName(in)
	out_name := arch.RegName(out)

	OutIns(ins, "%s,%s,0x%x", out_name, in_name, mode)
}

func (arch *AMD64_Arch) BU_float_remainder(t *Node, arg1, arg2 string) {
	// if the args are N and D, we need to compute this:
	//    N - trunc(N / D) * D

	suffix := "sd"
	if t.ty.bits == 32 {
		suffix = "ss"
	}

	// use XMM3 register to hold the division
	div := arch.RegName(XMM3)

	mode := 0x3  // truncate

	OutIns("movq",           "%s,%s", div, arg1)
	OutIns("div"   + suffix, "%s,%s", div, arg2)
	OutIns("round" + suffix, "%s,%s,0x%d", div, div, mode)
	OutIns("mul"   + suffix, "%s,%s", div, arg2)
	OutIns("sub"   + suffix, "%s,%s", arg1, div)
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) ArgumentString(op *Node, flags int) string {
	var reg Register

	if arch.IsRegister(op) {
		reg = arch.GetRegister(op)
		return arch.RegNameSize(reg, op.ty.bits)
	}

	// float instructions usually require registers
	if op.ty.kind == TYP_Float {
		reg = arch.workf1
		if (flags & 1) != 0 {
			reg = arch.workf2
		}
		arch.LoadReg(reg, op)
		return arch.RegName(reg)
	}

	// allow an immediate?
	if (flags & 2) != 0 {
		switch op.kind {
		case NL_Integer, NL_Char, NL_Bool:
			// 64-bits is ok when value fits into 32
			if op.ty.bits > 32 && IntegerFitting(op, false) > 32 {
				// too large
			} else {
				prefix := ""
				if (flags & 8) != 0 {
					prefix = arch.SizePrefix(op.ty.bits)
				}
				return prefix + arch.Operand(op)
			}
		}
	}

	// allow a memory access?
	if (flags & 4) != 0 {
		if op.kind == NP_Local || op.kind == NP_Memory {
			prefix := ""
			if (flags & 8) != 0 {
				prefix = arch.SizePrefix(op.ty.bits)
			}
			return prefix + arch.Operand(op)
		}
	}

	reg = RAX
	if (flags & 1) != 0 {
		reg = R11
	}

	arch.LoadReg(reg, op)
	return arch.RegNameSize(reg, op.ty.bits)
}

func (arch *AMD64_Arch) Operand(op *Node) string {
	switch op.kind {
	case NL_Integer, NL_Char, NL_Bool, NL_Float, NL_FltSpec, NL_String, NL_Null:
		return arch.SimpleDataValue(op)

	case NP_Local:
		local := arch.cur_func.locals[op.str]
		if local.reg == REG_ON_STACK {
			return arch.EncodeStack("rbp", local.offset)
		} else if local.ty.kind == TYP_Float {
			return arch.RegName(local.reg)
		} else {
			return arch.RegNameSize(local.reg, local.ty.bits)
		}

	case NP_Memory:
		base := ""
		base_op := op.children[0]

		if base_op.kind == NP_GlobPtr {
			base = "rel " + arch.EncodeName(base_op.str)
		} else {
			base = arch.Operand(base_op)

			// handle a local on the stack, move ptr into R11
			if base_op.kind == NP_Local {
				local := arch.cur_func.locals[base_op.str]

				if local.reg == REG_ON_STACK {
					base = arch.RegName(R11)
					slot_str := arch.EncodeStack("rbp", local.offset)
					OutIns("mov", "%s,%s", base, slot_str)
				}
			}
		}

		if op.offset > 0 {
			return fmt.Sprintf("[%s+%d]", base, op.offset)
		} else {
			return fmt.Sprintf("[%s]", base)
		}

	default:
		panic("unknown operand node")
	}
}

func (arch *AMD64_Arch) LiteralToReg(reg Register, lit *Node) {
	if lit.ty == nil {
		panic("literal node did not get a type")
	}
	ty := lit.ty

	r_name := arch.RegName(reg)

	if lit.kind == NL_Float || lit.kind == NL_FltSpec {
		if lit.str == "0.0" || lit.str == "0.0e0" || lit.str == "0x0p0" {
			OutIns("pxor", "%s,%s", r_name, r_name)
			return
		}

		val_str := arch.SimpleDataValue(lit)

		glob := ""
		ins  := ""

		switch ty.bits {
		case 32:
			glob = arch.AddFloat32(val_str)
			ins  = "movss"
		default:
			glob = arch.AddFloat64(val_str)
			ins  = "movsd"
		}

		OutIns(ins, "%s,%s", r_name, glob)
		return
	}

	if lit.kind == NL_String {
		glob := arch.SimpleDataValue(lit)
		OutIns("lea", "%s,[rel %s]", r_name, glob)
		return
	}

	switch lit.kind {
	case NL_Integer, NL_Char, NL_Bool, NL_Null:
		// ok
	default:
		panic("unsupported literal kind")
	}

	if lit.str == "0" || lit.str == "0x0" || lit.kind == NL_Null {
		OutIns("xor", "%s,%s", r_name, r_name)
		return
	}

	if ty.bits < 64 {
		r_name := arch.RegNameSize(reg, ty.bits)
		OutIns("mov", "%s,%s", r_name, lit.str)

	} else {
		if IntegerFitting(lit, ty.unsigned) <= 32 {
			if ty.unsigned || lit.str[0] != '-' {
				// the CPU zero extends here
				OutIns("mov", "%s,%s", arch.RegName32(reg), lit.str)
			} else {
				// the CPU sign extends here
				OutIns("mov", "%s,dword %s", r_name, lit.str)
			}
		} else {
			// use a 64-bit immediate move (called `movabs` in gas)
			OutIns("mov", "%s,%s", r_name, lit.str)
		}
	}
}

func (arch *AMD64_Arch) MoveReg(dest, src Register, ty *Type) {
	if dest != src {
		ins := "mov"

		if ty.kind == TYP_Float {
			ins = arch.FloatIns("movss", "movsd", ty)
		}

		OutIns(ins, "%s,%s", arch.RegName(dest), arch.RegName(src))
	}
}

func (arch *AMD64_Arch) LoadReg(reg Register, op *Node) {
	if op.ty == nil {
		panic("LoadReg with no type")
	}

	// nothing needed if operand is the same register
	if arch.IsRegister(op) {
		local := arch.cur_func.locals[op.str]
		if local.reg == reg {
			return
		}
	}

	if op.IsLiteral() {
		arch.LiteralToReg(reg, op)
		return
	}

	ty := op.ty

	r_name := arch.RegName(reg)
	ins    := "mov"

	if op.kind == NP_GlobPtr || op.kind == NP_FuncRef {
		OutIns("lea", "%s,[rel %s]", r_name, arch.EncodeName(op.str))
		return
	}

	if ty.kind == TYP_Float {
		ins = arch.FloatIns("movss", "movsd", ty)
	} else {
		r_name = arch.RegNameSize(reg, ty.bits)
	}

	OutIns(ins, "%s,%s", r_name, arch.Operand(op))
}

func (arch *AMD64_Arch) StoreReg(reg Register, op *Node) {
	if op.ty == nil {
		panic("StoreReg with no type")
	}

	// this is not possible due to use of R11 in Operand()
	if reg == R11 {
		panic("StoreReg with R11")
	}

	// nothing needed if destination is the same register
	if arch.IsRegister(op) {
		local := arch.cur_func.locals[op.str]
		if local.reg == reg {
			return
		}
	}

	ty := op.ty

	r_name := arch.RegName(reg)
	ins    := "mov"

	if ty.kind == TYP_Float {
		ins = arch.FloatIns("movss", "movsd", ty)
	} else {
		r_name = arch.RegNameSize(reg, ty.bits)
	}

	OutIns(ins, "%s,%s", arch.Operand(op), r_name)
}

func (arch *AMD64_Arch) IsRegister(t *Node) bool {
	if t.kind == NP_Local {
		local := arch.cur_func.locals[t.str]
		return local.reg != REG_ON_STACK
	}
	return false
}

func (arch *AMD64_Arch) GetRegister(t *Node) Register {
	if t.kind == NP_Local {
		local := arch.cur_func.locals[t.str]
		if local.reg != REG_ON_STACK {
			return local.reg
		}
	}
	return REG_NONE
}

func (arch *AMD64_Arch) UsesRegister(t *Node, reg Register) bool {
	if t.kind == NP_Memory {
		t = t.children[0]
	}
	if t.kind == NP_Local {
		local := arch.cur_func.locals[t.str]
		return local.reg == reg
	}
	return false
}

func (arch *AMD64_Arch) StoresToDerefLocal(t *Node, local *LocalInfo) bool {
	if t.kind != NH_Move {
		return false
	}

	dest := t.children[0]
	if dest.kind != NP_Memory {
		return false
	}

	return arch.cur_func.NodeReadsLocal(dest, local)
}

func (arch *AMD64_Arch) SameLocalOrMem(t1, t2 *Node) bool {
	return arch.SameLocals(t1, t2) || arch.SameMemory(t1, t2)
}

func (arch *AMD64_Arch) SameLocals(t1, t2 *Node) bool {
	if t1.kind == NP_Local && t2.kind == NP_Local {
		return t1.str == t2.str
	}
	return false
}

func (arch *AMD64_Arch) SameMemory(t1, t2 *Node) bool {
	if t1.kind == NP_Memory && t2.kind == NP_Memory {
		if t1.offset != t2.offset {
			return false
		}
		t1 = t1.children[0]
		t2 = t2.children[0]

		if t1.kind == NP_GlobPtr && t2.kind == NP_GlobPtr {
			return t1.str == t2.str
		}
		return arch.SameLocals(t1, t2)
	}

	return false
}

func (arch *AMD64_Arch) FloatIns(ins32, ins64 string, ty *Type) string {
	if ty.bits == 32 {
		return ins32
	}
	return ins64
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) SortLocals() {
	arch.ordered_locals = make([]*LocalInfo, 0)

	for _, local := range arch.cur_func.locals {
		arch.ordered_locals = append(arch.ordered_locals, local)
	}

	sort.Slice(arch.ordered_locals, func(i, k int) bool {
		L1 := arch.ordered_locals[i]
		L2 := arch.ordered_locals[k]

		return L1.OrdLess(L2)
	})
}

//----------------------------------------------------------------------

func (arch *AMD64_Arch) StackAllocate() error {
	arch.SA_Locals()
	arch.SA_Blocks()

	// reserve space for on-stack parameters of functions we call
	call_slots := arch.SA_DeepestCall()
	arch.stack_slots += call_slots

	return OK
}

func (arch *AMD64_Arch) SA_DeepestCall() int {
	// finds the function call with the most on-stack parameters,
	// and returns that quantity (as a number of slots).
	// for Win64 ABI, the result takes the shadow area into account.
	// note that tail-calls are ignored here.

	deepest := 0

	for _, t := range arch.cur_func.body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_Call {
			depth := arch.SA_CallDepth(comp)
			if depth > deepest {
				deepest = depth
			}
		}
	}

	return deepest
}

func (arch *AMD64_Arch) SA_CallDepth(t *Node) int {
	// t must be a NC_Call node

	t_func := t.children[0]

	fun_type := t_func.ty
	if fun_type.kind == TYP_Pointer {
		fun_type = fun_type.sub
	}
	if fun_type.kind != TYP_Function {
		panic("weird func type in call")
	}

	depth := 0

	for n, par := range fun_type.param {
		is_float := (par.ty.kind == TYP_Float)
		is_int   := !is_float

		if arch.abi == "win" {
			depth += 1
		} else {
			if (n < 6 && is_int) || (n < 8 && is_float) {
				// not on stack
			} else {
				depth += 1
			}
		}
	}

	// for Win64 ABI, reserve space for shadow area (4 slots).
	// this area is not for THIS function, rather it is used by OTHER
	// functions we call as a place to store their first four params.

	if arch.abi == "win" && depth < 4 {
		depth = 4
	}
	return depth
}

func (arch *AMD64_Arch) SA_Locals() {
	// locals which did not get a register get a stack slot here.

	// WISH A: re-use slots for non-overlapping locals (except with -N)
	// WISH B: pack 1/2/4 byte values more densely

	// for a parameter passed on the stack, we use/modify it where it is
	// (the caller's stack frame) rather than copy it into our own frame.
	// while that seems a bit cheeky, it matches what GCC does.

	for _, local := range arch.ordered_locals {
		if local.reg == REG_ON_STACK && local.offset == 0 {
			arch.stack_slots += 1
			local.offset = 0 - arch.stack_slots*8
		}
	}
}

func (arch *AMD64_Arch) SA_Blocks() {
	// underneath the non-reg locals are the `stack-var` blocks.
	// blocks of 16 bytes or larger get a 16-byte alignment.

	// WISH: pack 1/2/4 byte blocks more densely

	for _, t := range arch.cur_func.body.children {
		comp := t.GetComputation()

		if comp != nil && comp.kind == NC_StackVar {
			if comp.ty.kind != TYP_Pointer {
				panic("weird NH_StackVar type")
			}
			real_type := comp.ty.sub
			slots     := (real_type.size + 7) / 8

			arch.stack_slots += slots
			if real_type.size >= 16 && (arch.stack_slots & 1) != 0 {
				arch.stack_slots += 1
			}

			comp.offset = 0 - arch.stack_slots*8
		}
	}
}

//----------------------------------------------------------------------

type AMD64_RegAssign struct {
	group map[*LocalInfo]Register
	cost  int64

	extra_int   int
	extra_float int
}

func (arch *AMD64_Arch) RegisterAllocate() error {
	arch.RA_Parameters()
	arch.RA_CostOfLocals()

	// try various combinations of "extra registers" (ones which will
	// need to be saved at function entry and restored at exit) and
	// find the most optimal combination.

	max_int   := 5  // RBX and R12..R15
	max_float := 0

	if arch.abi == "win" {
		max_int   = 7  // include RSI and RDI
		max_float = 8  // XMM8..XMM15
	}

	best     := arch.RA_AllocateWith(0, 0)
	best_int := 0

	for extra_int := 1; extra_int <= max_int; extra_int++ {
		asg := arch.RA_AllocateWith(extra_int, 0)
		if asg.cost < best.cost {
			best = asg
			best_int = extra_int
		}
	}

	for extra_float := 1; extra_float <= max_float; extra_float++ {
		asg := arch.RA_AllocateWith(best_int, extra_float)
		if asg.cost < best.cost {
			best = asg
		}
	}

	// update the locals with the best allocation
	for _, local := range arch.ordered_locals {
		if local.param_idx >= 0 && local.reads == 0 {
			// don't waste a stack slot on an unused parameter
			local.reg = 0
		} else {
			local.reg = REG_ON_STACK
		}
	}
	for local, reg := range best.group {
		local.reg = reg

		// callee-saved registers need to be stored on the stack
		if arch.RA_IsExtra(reg) {
			arch.extra_regs.Add(reg)
			arch.stack_slots += 1
		}
	}

	return OK
}

func (arch *AMD64_Arch) RA_Parameters() {
	abi_regs := arch.ParameterRegs(arch.cur_func.ty)

	// need to skip the saved RBP and the return address
	stack_offset := 16

	for n, par := range arch.cur_func.params {
		par.abi_reg = abi_regs[n]

		if par.abi_reg == REG_ON_STACK || (arch.abi == "win" && n < 4) {
			par.offset = stack_offset
			stack_offset += 8
		}
	}
}

func (arch *AMD64_Arch) RA_CostOfLocals() {
	// create list of locals sorted by cost, highest first
	arch.costed_locals = make([]*LocalInfo, 0)

	for _, local := range arch.cur_func.locals {
		if local.reads > 0 {
			local.cost = arch.cur_func.CostOfLocal(local,
				arch.c_create, arch.c_write, arch.c_read)
			arch.costed_locals = append(arch.costed_locals, local)
		}
//		println(arch.cur_func.name, local.name, "=", local.cost)
	}

	sort.Slice(arch.costed_locals, func(i, k int) bool {
		L1 := arch.costed_locals[i]
		L2 := arch.costed_locals[k]

		if L1.cost != L2.cost {
			return L1.cost > L2.cost
		}

		// secondary test is by order
		return L1.OrdLess(L2)
	})

//	println("FUNCTION:", arch.cur_func.name)
//	for _, local := range arch.costed_locals {
//		println("  ", local.name, local.cost)
//	}
}

func (arch *AMD64_Arch) RA_AllocateWith(extra_int, extra_float int) *AMD64_RegAssign {
	asg := new(AMD64_RegAssign)
	asg.group = make(map[*LocalInfo]Register)
	asg.extra_int = extra_int
	asg.extra_float = extra_float

	if Options.no_optim {
		return asg
	}

	// visit each local, try to find a register for it
	for _, local := range arch.costed_locals {
		arch.RA_VisitLocal(local, asg)
	}

	// total cost is cost of save/restore the extra registers plus the
	// costs of each local.
	save_cost := int64(arch.c_create + arch.c_write + arch.c_read)

	asg.cost += save_cost * int64(extra_int + extra_float)

	for _, local := range arch.cur_func.locals {
		if local.reads > 0 {
			_, has_reg := asg.group[local]
			if !has_reg {
				asg.cost += local.cost
			}
		}
	}

/* DEBUG
println("TOTAL COST OF", arch.cur_func.name, extra_int, extra_float, "=", asg.cost)
println("allocated regs:", len(asg.group))
*/
	return asg
}

func (arch *AMD64_Arch) RA_VisitLocal(local *LocalInfo, asg *AMD64_RegAssign) {
	prefer := arch.RA_PreferRegister(local)

	var reg_list []Register

	if local.ty.kind == TYP_Float {
		reg_list = arch.RA_FloatRegisters(asg.extra_float, prefer)
	} else {
		reg_list = arch.RA_IntRegisters(asg.extra_int, prefer)
	}

	clobber := arch.RA_ClobberSet(local, asg)

	for _, reg := range reg_list {
		if !clobber.Has(reg) {
			// use this register for the local
			asg.group[local] = reg
			break
		}
	}
}

func (arch *AMD64_Arch) RA_IntRegisters(extra int, prefer Register) []Register {
	list := make([]Register, 0)

	if prefer != 0 {
		list = append(list, prefer)
	}

	for reg := R12; reg <= R15; reg++ {
		if extra > 0 {
			list = append(list, reg)
			extra--
		}
	}
	if extra > 0 {
		list = append(list, RBX)
		extra--
	}
	if arch.abi == "win" {
		if extra > 0 {
			list = append(list, RSI)
			extra--
		}
		if extra > 0 {
			list = append(list, RDI)
			extra--
		}
	}

	// ordinary (clobbered by func call) registers

	list = append(list, R10)
	list = append(list, R9)
	list = append(list, R8)
	list = append(list, RCX)
	list = append(list, RDX)

	if arch.abi != "win" {
		list = append(list, RSI)
		list = append(list, RDI)
	}

	return list
}

func (arch *AMD64_Arch) RA_FloatRegisters(extra int, prefer Register) []Register {
	list := make([]Register, 0)

	if prefer != 0 {
		list = append(list, prefer)
	}

	if arch.abi == "win" {
		for i := 0; i < extra; i++ {
			list = append(list, XMM8 + Register(i))
		}
		for reg := XMM0; reg < XMM4; reg++ {
			list = append(list, reg)
		}
	} else {
		for reg := XMM0; reg < XMM14; reg++ {
			list = append(list, reg)
		}
	}

	return list
}

func (arch *AMD64_Arch) RA_IsExtra(reg Register) bool {
	switch reg {
	case RBX, R12, R13, R14, R15:
		return true
	}

	if arch.abi == "win" {
		switch reg {
		case RSI, RDI:
			return true
		}
		if reg >= XMM6 {
			return true
		}
	}

	return false
}

func (arch *AMD64_Arch) RA_PreferRegister(local *LocalInfo) Register {
	// determine what could be the ideal register for this local.
	// this is most important for temporaries (single use locals).

	if local.param_idx >= 0 {
		if local.abi_reg != REG_ON_STACK {
			return local.abi_reg
		}
	}

	fu    := arch.cur_func
	nodes := fu.body.children

	for i := local.live_start + 1; i <= local.live_end; i++ {
		t    := nodes[i]
		comp := t.GetComputation()

		if comp != nil {
			if comp.kind == NC_Builtin {
				switch comp.str {
				case "ishl", "ishr":
					shift := comp.children[1]
					if shift.kind == NP_Local && shift.str == local.name {
						return RCX
					}
				}
			}
		}

		var call *Node

		if comp != nil && comp.kind == NC_Call {
			call = comp
		} else if t.kind == NH_TailCall {
			call = t
		}

		if call != nil {
			fun_type := call.children[0].ty
			if fun_type.kind == TYP_Pointer {
				fun_type = fun_type.sub
			}
			par_regs := arch.ParameterRegs(fun_type)

			for i, par := range call.children[1:] {
				if par.kind == NP_Local && par.str == local.name {
					if par_regs[i] != REG_ON_STACK {
						return par_regs[i]
					}
				}
			}
		}
	}

	return 0  // none
}

func (arch *AMD64_Arch) RA_ClobberSet(local *LocalInfo, asg *AMD64_RegAssign) *RegisterSet {
	// determine registers which cannot be used for a particular local,
	// including cases where a particular register is forced by a particular
	// instruction (e.g. RDX is needed by the IDIV instruction).

	set := NewRegisterSet()

	if local.param_idx >= 0 {
		abi_regs := arch.ParameterRegs(arch.cur_func.ty)

		for reg := RAX; reg <= XMM15; reg++ {
			if arch.RA_ParamConflict(local, reg, abi_regs) {
				set.Add(reg)
			}
		}
	}

	for i := local.live_start; i <= local.live_end; i++ {
		arch.RA_ClobbersForNode(i, local, set)
	}

	for other, reg := range asg.group {
		if other.live_start >= local.live_end ||
			other.live_end <= local.live_start {
			// no overlap
		} else {
			set.Add(reg)
		}
	}

	return set
}

func (arch *AMD64_Arch) RA_ClobbersForNode(idx int, local *LocalInfo, clobber *RegisterSet) {
	// WISH: an assignment to a local can mean previous nodes which do not
	//       need the local's value (AND no labels in between) don't really
	//       clobber it...

	nodes := arch.cur_func.body.children
	t     := nodes[idx]

	// first use creates the local, it can never clobber it
	if idx <= local.live_start {
		return
	}

	// nodes which assign to the local can be ignored here
	if t.kind == NH_Move {
		dest := t.children[0]
		if dest.kind == NP_Local && dest.str == local.name {
			return
		}
	}

	// the last usage generally does not matter, local won't be used again.
	// [ except for loops, but ExtendLiveRanges takes care of that ]
	// main exception is when local is a pointer and node stores to that
	// pointer -- clearly the local must survive the computation then.

	is_last  := (idx >= local.live_end)
	is_deref := arch.StoresToDerefLocal(t, local)

	comp := t.GetComputation()

	// tail calls are ignored here, since they never return.
	// WISH: ignore a call to a `no-return` function too.

	if comp != nil && comp.kind == NC_Call {
		if is_last && !is_deref {
			return
		}

		clobber.Add(RCX)
		clobber.Add(RDX)
		clobber.Add(R8)
		clobber.Add(R9)
		clobber.Add(R10)
		clobber.Add(R11)

		for r := XMM0; r <= XMM5; r++ {
			clobber.Add(r)
		}

		if arch.abi != "win" {
			clobber.Add(RDI)
			clobber.Add(RSI)

			for r := XMM6; r <= XMM15; r++ {
				clobber.Add(r)
			}
		}
	}

	if comp != nil && comp.kind == NC_Builtin {
		var t_arg1 *Node
		var t_arg2 *Node

		if len(comp.children) >= 1 {
			t_arg1 = comp.children[0]
		}
		if len(comp.children) >= 2 {
			t_arg2 = comp.children[1]
		}
		_ = t_arg1

		switch comp.str {
		case "fremt":
			// remainder calc requires a third register
			clobber.Add(XMM3)

		case "idivt", "iremt":
			// DIV instructions require the RAX and RDX registers
			clobber.Add(RDX)

		case "ishl", "ishr":
			// shift instructions need the RCX register (unless shift is a constant)
			if t_arg2.kind == NL_Integer {
				// ok
			} else if t_arg2.kind == NP_Local && t_arg2.str == local.name {
				// ok
			} else {
				clobber.Add(RCX)
			}
		}
	}

	if t.kind == NH_Swap {
		t_left  := t.children[0]
		t_right := t.children[1]

		// may need a register if one operand is a memory access
		if t_left.kind == NP_Memory || t_right.kind == NP_Memory {
			if t_left.ty.kind != TYP_Float {
				clobber.Add(R10)
			}
		}
	}
}

func (arch *AMD64_Arch) RA_ParamConflict(par *LocalInfo, reg Register, abi_regs []Register) bool {
	// when a parameter is allocated a register, there is code generated
	// at the beginning of a function to transfer it, but that code could
	// overwrite another (in-reg) parameter, so we check for that here.
	//
	// for example: if our function takes parameters x in XMM0 and y in XMM1,
	// then x cannot be allocated to XMM1 since the transfer would overwrite
	// parameter y.

	// if the parameter is on the stack, it will always be transferred
	// after every in-reg parameter of the same kind (int or float) has
	// been transferred, so no chance of overwrite.
	if par.abi_reg == REG_ON_STACK {
		return false
	}

	// no problem when it stays where it is
	if par.abi_reg == reg {
		return false
	}

	// no problem if the register is not used by any other parameter
	used := false
	for _, other := range abi_regs {
		if other == reg {
			used = true
			break
		}
	}
	if !used {
		return false
	}

	// floating point register?
	if par.abi_reg >= XMM0 {
		return reg >= par.abi_reg
	}

	// for integer registers, categorise into a few groups
	// (to keep the logic simple and ABI-agnostic).
	var p int
	var r int

	switch par.abi_reg {
	case RDI, RSI: p = 0
	case RCX, RDX: p = 1
	case R8:       p = 2
	case R9:       p = 3
	}

	switch reg {
	case RDI, RSI: r = 0
	case RCX, RDX: r = 1
	case R8:       r = 2
	case R9:       r = 3
	default: return false
	}

	return r >= p
}
