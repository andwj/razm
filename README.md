
Razm Programming Language
=========================

by Andrew Apted, 2021.


About
-----

Razm is a low-level but portable assembly-like programming language.
The name is an abbreviation of the phrase "register allocating assembly",
and it aims to be more low-level than C while still providing some type
safety, as well as avoiding some of the drudgery and complexity that
comes when writing native assembly code.


Documentation
-------------

The [MANUAL.md](MANUAL.md) doc describes the language.


Binary Packages
---------------

https://gitlab.com/andwj/razm/tags/v1.0.0


Status
------

The language itself is now stable.  While there may be extra facilities
added in the future, it is expected that any such additions will not
require updating existing razm code.

The compiler front-end (i.e. the architecture-neutral part) is complete
and working well.

So far only a single architecture is supported, namely "AMD64", which
is sometimes called 64-bit "x86" code.  A register allocator has been
implemented and does a decent job.


Legalese
--------

Razm is under a permissive MIT license.

Razm comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) doc for the full terms.


Sample Code
-----------

```
;;
;; Mandelbrot example
;;
;; by Andrew Apted, 2021.
;;
;; this code is licensed as CC0 (i.e. public domain)
;;

extern-fun tinyrt-out-byte (c u8)

const W = 78
const H = 62

const XOffset = -1.33
const YOffset = -1.20
const XScale  = 45.0
const YScale  = 30.0

const MaxIterations = 64000

#public

fun main ()
    y s32 = 0
    loop while lt? y H
        DrawLine y
        y = iadd y 1
    endloop
end

fun DrawLine (y s32)
    x s32 = 0
    loop while lt? x W
        ch = ComputePoint x y
        tinyrt-out-byte ch
        x = iadd x 1
    endloop
    tinyrt-out-byte '\r'
    tinyrt-out-byte '\n'
end

fun ComputePoint (sx s32 sy s32 -> u8)
    x = conv f64 sx
    y = conv f64 sy

    x = fadd (fdiv x XScale) XOffset
    y = fadd (fdiv y YScale) YOffset

    re = x     ; real component of Z
    im = y     ; imaginary component of Z
    n s32 = 0  ; number of iterations

    loop
        n = iadd n 1
        jump in_set if ge? n MaxIterations

        re2 = fmul re re
        im2 = fmul im im

        sqsum = fadd re2 im2
        jump out_set if gt? sqsum 4.0

        im = fadd y (fmul re  im  2.0)
        re = fadd x (fsub re2 im2)
    endloop

in_set:
    return '#'

out_set:
    return '.'
end
```
