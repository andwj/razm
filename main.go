// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	in_files []string
	out_file string

	// the architecture, such as "amd64"
	arch string

	// the operating system, such as "linux" or "windows"
	os string

	no_optim   bool
	all_errors bool
	help       bool
}

var out_fp *os.File
var out_errors bool

// Architecture is an interface which encapsulates methods for
// generating code for a particular architecture and assembler.
type Architecture interface {
	// OutputExt provides the normal extension for the output
	// assembly file.  It should include the dot.
	OutputExt() string

	// Begin is called just after all the input files are parsed,
	// but before any actual compilation has been done.
	Begin()

	// Finish is called just before the output file is closed.
	Finish()

	// this returns true for big endian CPUs, false for little endian.
	BigEndian() bool

	// RegisterBits gives the number of bits in a general purpose register.
	RegisterBits() int

	// RegName queries the assembler name of a register.
	RegName(reg Register) string

	// CompileVar generates the output assembly for global variable.
	CompileVar(vdef *VarDef)

	// CompileFunction generates the output assembly for global function.
	CompileFunction(fu *FuncDef)
}

var arch Architecture

//----------------------------------------------------------------------

func main() {
	// this sets up the `arch` global var.
	// it will fatal error on invalid arguments.
	HandleArgs()

	DefaultOutputFile()
	ClearErrors()

	InitDefs()
	InitTypes()
	InitBuiltins()

	// create output file
	var err error
	out_fp, err = os.Create(Options.out_file)
	if err != nil {
		FatalError("cannot create output: %s", err.Error())
	}

	// read all the tokens from every file
	// [ the only errors will be file errors, lexing and bad/dup names ]
	LoadEverything()

	arch.Begin()

	if !have_errors {
		CompileEverything()
	}

	arch.Finish()

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	if out_errors {
		FatalError("writing to the output file failed")
	}

	out_fp.Close()

	os.Exit(0)
}

func HandleArgs() {
	Options.in_files = make([]string, 0)

	// default architecture : try to match host
	switch runtime.GOARCH {
	case "amd64":
		Options.arch = runtime.GOARCH
	default:
		Options.arch = "amd64"
	}

	// default OS : try to match host
	switch runtime.GOOS {
	case "windows", "linux":
		Options.os = runtime.GOOS
	default:
		Options.os = "linux"
	}

	argv.Generic("o", "output",     &Options.out_file, "file", "name of output file")
	argv.Generic("t", "target",     &Options.os, "OS", "target OS (linux|windows)")
	argv.Generic("a", "arch",       &Options.arch, "arch", "CPU architecture (amd64)")
	argv.Enabler("e", "all-errors", &Options.all_errors, "remove limit on errors per function")
	argv.Enabler("N", "no-optim",   &Options.no_optim, "disable all optimizations")
	argv.Enabler("h", "help",       &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	Options.in_files = argv.Unparsed()

	// WISH: support more architectures
	switch Options.arch {
	case "amd64":
		arch = NewAMD64Arch()
	default:
		FatalError("unknown arch: %s", Options.arch)
	}

	switch Options.os {
	case "linux", "windows":
		// ok
	default:
		FatalError("unknown OS: %s", Options.os)
	}

	if Options.help || len(Options.in_files) == 0 {
		ShowUsage()
		os.Exit(0)
	}
}

func DefaultOutputFile() {
	if Options.out_file == "" {
		first := Options.in_files[0]
		bare := FileRemoveExtension(first)
		ext := arch.OutputExt()
		Options.out_file = bare + ext
	}
}

func ShowUsage() {
	fmt.Printf("Usage: razm FILE.rz ... [OPTIONS]\n")

	fmt.Printf("\n")
	fmt.Printf("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	if out_fp != nil {
		out_fp.Close()

		// remove the useless file
		os.Remove(Options.out_file)
	}
	if format != "" {
		format = "razm: " + format + "\n"
		fmt.Fprintf(os.Stderr, format, a...)
	}
	os.Exit(1)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

//----------------------------------------------------------------------

func LoadEverything() {
	// visit all files, even if some produce an error
	for f_idx, in_file := range Options.in_files {
		LoadCodeFile(1 + f_idx, in_file)
	}
}

func LoadCodeFile(f_idx int, fullname string) {
	ErrorsBeginFile("")

	read_fp, err := os.Open(fullname)
	if err != nil {
		if os.IsNotExist(err) {
			PostError("no such file: %s", fullname)
		} else {
			PostError("could not open file: %s", fullname)
		}
		return
	}

	f := NewInputFile(fullname, f_idx, read_fp)

	f.Load()

	read_fp.Close()
}

//----------------------------------------------------------------------

func CompileEverything() {
	CreateOrderedDefs()

	CompileAllConstants()
	CompileAllTypes()

	if have_errors {
		return
	}

	DeduceAllFunctions()
	DeduceAllVars()

	if have_errors {
		return
	}

	FinalizeTypes()

	if have_errors {
		return
	}

	ParseAllFunctions()
	ParseAllVars()

	if have_errors {
		return
	}

	InlineAllFunctions()

	if have_errors {
		return
	}

	CompileAllFunctions()
	CompileAllVars()
}

//----------------------------------------------------------------------

func OutLine(format string, a ...interface{}) {
	if out_errors {
		return
	}

	format += "\n"

	_, err := fmt.Fprintf(out_fp, format, a...)
	if err != nil {
		out_errors = true
	}
}

func OutIns(ins, args string, a ...interface{}) {
	if out_errors {
		return
	}

	line := "\t" + ins

	if args != "" {
		line += "\t" + fmt.Sprintf(args, a...)
	}

	_, err := fmt.Fprintf(out_fp, "%s\n", line)
	if err != nil {
		out_errors = true
	}
}
